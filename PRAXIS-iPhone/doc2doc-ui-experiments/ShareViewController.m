//
//  ShareViewController.m
//  FrameWork
//
//  Created by Mahbub Morshed on 9/1/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "ShareViewController.h"

@interface ShareViewController ()

@end

@implementation ShareViewController
@synthesize name_of_framework, access_level, shareButton, cancelButton, activity;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [name_of_framework becomeFirstResponder];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction)closeYourSelf:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}

/*
 {
 "screen_width":3,
 "screen_height":4,
 "frames":[
 {
 "index":0,
 "start_x":0,
 "start_y":0,
 "width":3,
 "height":1,
 "content":"doc2doc/index.php/user/geo_location",
 "content_type":"map"
 },
 {
 "index":1,
 "start_x":0,
 "start_y":1,
 "width":1,
 "height":1,
 "content":"http://www.facebook.com",
 "content_type":"web"
 }
 ]
 }
 
 */

-(NSString *)createString{
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    MMAppEngine *engine= [MMAppEngine sharedManager];
    
    NSString *sharing= [NSString stringWithFormat:@"{\n\"screen_width\":%d,\n\"screen_height\":%d,\n\"frames\":[",[prefs integerForKey:@"xval"],[prefs integerForKey:@"yval"]];
    
    NSString *frameToShareString= @"";
    
    for (int index= 0; index <[engine.currentStates count];index++ ) {
        
        
        int minH= [prefs integerForKey:@"minH"];
        int minW= [prefs integerForKey:@"minW"];
        
        NSString *frameString= [engine.currentStates objectAtIndex:index] ;
        CGRect frameRect= CGRectFromString(frameString);
        int w= (int)frameRect.size.width/ minW;
        int h= (int)frameRect.size.height/ minH;
        
        frameToShareString= [NSString stringWithFormat: @"%@{\"index\":%d ,\"start_x\":%d,\"start_y\":%d,\"width\":%d,\"height\":%d,\"conent\":%@,\"content_type\":%@}",frameToShareString, index, (int)frameRect.origin.x/ minW ,(int)frameRect.origin.y/ minH, w, h,[engine.content objectAtIndex:index], [engine.contentType objectAtIndex:index] ];
    }
    
    sharing= [NSString stringWithFormat:@"%@%@\n]\n}",sharing,frameToShareString];
    
    NSLog(@"%@",sharing);
    
    return sharing;
}

-(IBAction)shareFrame:(id)sender{
    
    [activity startAnimating];

    if (self.name_of_framework.text.length >=1 ) {
        
        NSString *toShare= [self createString];
        
        NSString *accessLevel;
        
        if(self.access_level.selectedSegmentIndex == 0){
            accessLevel= @"public";
        }
        else
            accessLevel = @"self";
        
        __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                        self.name_of_framework.text, @"name",
                                        @"framework", @"type",
                                        toShare, @"contents",
                                        @"", @"notify_user_ids",
                                        accessLevel, @"access_level",
                                        nil];
        
        //request block
        [[RKClient sharedClient] post:@"/shareables/publish" usingBlock:^(RKRequest *request) {
            [request setParams:params];
            request.onDidLoadResponse = ^(RKResponse *response) {

                RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
                
                NSString *re= [response bodyAsString];
                NSDictionary *__response= [par objectFromString:re error:nil];
                
                NSLog(@"%@", __response);
            };//End of onDidLoadResponse
        }];//End of block
        [activity stopAnimating];
    }
    else{
        [activity stopAnimating];
        
        UIAlertView *alert= [[UIAlertView alloc]initWithTitle:@"Please enter a name" message:@"You must enter a name to share the frameWork" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
    }
    
    [self closeYourSelf:self];
}


@end
