//
//  ViewController.h
//  doc2doc-ui-experiments
//
//  Created by Mahbub Morhsed on 9/5/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPUserResizableView.h"
#import "QuartzCore/QuartzCore.h"
#import <MapKit/MapKit.h>
#import "EditMode.h"
#import "ViewMode.h"
#import "ShareViewController.h"
#import "MYRootViewController.h"


@interface ViewController : UIViewController
{
    int currentMode;//EditMode=0 ViewMode=1
    
    UIAlertView *login;
    UIAlertView *xInput;
    UIAlertView* yInput;
    UIActivityIndicatorView *__weak activity;
    
    EditMode *editMode;
    ViewMode *viewMode;

    UIButton *__weak undo;
    UILabel *__weak mode;
}

@property(weak, nonatomic) IBOutlet UIButton *undo;
@property(weak, nonatomic) IBOutlet UILabel *mode;
@property(weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property(strong, nonatomic) IBOutlet UIImageView *blackCover;
@property(strong) IBOutlet UIImageView *praxisLogo;

-(IBAction)undoButton:(id)sender;
-(IBAction)changeMode:(id)sender;
- (IBAction)curlAction:(id)sender;
- (NSString *) LoginWIthUserName:(NSString *)username password:(NSString *)password;
-(IBAction)showTables:(id)sender;
-(IBAction)sharingIsCaring:(id)sender;

@end

