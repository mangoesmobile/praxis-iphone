//
//  NSData+AES256.h
//  CryptoExercise
//
//  Created by Mahbub Morshed on 7/28/12.
//  Copyright (c) 2012 ManGoes Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSData+Base64.h"

@interface NSData (AES256)
- (NSData *)AES256EncryptWithKey:(NSString *)key;
- (NSData *)AES256DecryptWithKey:(NSString *)key;
@end
