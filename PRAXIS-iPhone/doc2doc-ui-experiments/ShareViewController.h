//
//  ShareViewController.h
//  FrameWork
//
//  Created by Mahbub Morshed on 9/1/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMAppEngine.h"

@interface ShareViewController : UIViewController

//All the outlets
@property(strong) IBOutlet UITextField *name_of_framework;
@property(strong) IBOutlet UISegmentedControl *access_level;
@property(strong) IBOutlet UIButton *shareButton;
@property(strong) IBOutlet UIButton *cancelButton;
@property(strong) IBOutlet UIActivityIndicatorView *activity;

//All the methods
-(IBAction)closeYourSelf:(id)sender;
-(NSString *)createString;
-(IBAction)shareFrame:(id)sender;

@end
