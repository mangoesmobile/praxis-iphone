//
//  Authorize.h
//  FrameWork
//
//  Created by Mahbub Morshed on 7/17/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import "SecKeyWrapper.h"
#import "NSData+AES256.h"
#import <RestKit/RestKit.h>
#import <RestKit/RKJSONParserJSONKit.h>

@interface PRAXIS : NSObject{

}

- (NSString *) sha1:(NSString*)input;

/*************************************************************************************************/
 //Authorize
- (NSString *) LoginWIthUserName:(NSString *)username password:(NSString *)password;
- (NSString *) logOut;
- (NSString *) changePasswordOld:(NSString *)oldPassword new:(NSString *)newPassword;
- (NSString *) forgotPassword:(NSString *)email;
- (NSString *) signupUsingEmail:(NSString *)email username:(NSString *)username password:(NSString *)password workgroup_id:(NSString *)org;

/*************************************************************************************************/
//Shareables
-(NSString *)fetch_followed_page:(NSString *)pageNo;
-(NSString *)fetch_owned:(NSString *)access_level page:(NSString *)pageNo;
-(NSString *)get_item:(NSString *)shareable_id;
-(NSString *)publish : (NSString *)name 
                type : (NSString *)type 
            contents : (NSString *)contents 
       geo_longitude : (NSString *)geo_longitude 
        geo_latitude : (NSString *)geo_latitude 
    tag_category_ids : (NSString *)tag_category_ids 
     notify_user_ids : (NSString *)notify_user_ids 
        access_level : (NSString *)access_level;

-(NSString *)update  : (NSString *)shareable_id
                name : (NSString *)name 
                type : (NSString *)type 
            contents : (NSString *)contents 
       geo_longitude : (NSString *)geo_longitude 
        geo_latitude : (NSString *)geo_latitude 
    tag_category_ids : (NSString *)tag_category_ids 
     notify_user_ids : (NSString *)notify_user_ids 
        access_level : (NSString *)access_level;
-(NSString *)start_following:(NSString *)shareable_id;
-(NSString *)stop_following:(NSString *)shareable_id;

/*************************************************************************************************/
//Messages
-(NSString *)fetch_inbox:(NSString *)page_no;
-(NSString *)fetch_sentbox:(NSString *)page_no;
-(NSString *)message_details_id: (NSString *)message_id latest_reply_id:(NSString *)latest_reply_id;
-(NSString *)post_new_subject:(NSString *) subject 
                     contents:(NSString *)contents 
                 geo_logitude:(NSString *)geo_longitude 
                 geo_latitude:(NSString *)geo_latitude  
              notify_user_ids:(NSString *)notify_user_ids 
             tag_category_ids:(NSString *)tag_category_ids;
-(NSString *)post_reply:(NSString *) message_id
               contents:(NSString *)contents 
           geo_logitude:(NSString *)geo_longitude 
           geo_latitude:(NSString *)geo_latitude;

/*************************************************************************************************/
//Cases
-(NSString *)change_review_date_case_id:(NSString *) case_id
                       next_review_date:(NSString *)next_review_date;
-(NSString *)close_case:(NSString *) case_id;
-(NSString *)fetch_comments_of_proposition:(NSString *) case_prop_id;
-(NSString *)fetch_complete_case:(NSString *) case_id;
-(NSString *)fetch_followed:(NSString *) user_id
                    page_no:(NSString *)page_no;
-(NSString *)fetch_propositions_of_review:(NSString *) case_rev_id
                    latest_proposition_id:(NSString *)latest_proposition_id;
-(NSString *)fetch_reviews_of_case:(NSString *) case_id
                  latest_review_id:(NSString *)latest_review_id;
-(NSString *)fetch_unfollowed:(NSString *) page_no;
-(NSString *)my_cases:(NSString *) page_no;
-(NSString *)post_comment: (NSString *) case_prop_id contents:(NSString *)contents geo_longitude:(NSString *)geo_longitude geo_latitude:(NSString *)geo_latitude consent:(NSString *)consent notify_user_ids:(NSString *)notify_user_ids;

@end
