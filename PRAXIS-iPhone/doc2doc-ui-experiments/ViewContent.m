//
//  ViewModeContent.m
//  FrameWork
//
//  Created by Mahbub Morshed on 7/8/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "ViewContent.h"

@implementation ViewContent
@synthesize fullScreen,contentChooser,originalFrame, web;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        self.viewIndex = [NSString stringWithFormat:@"nil"];
        
        engine= [MMAppEngine sharedManager];
        
        [ViewContent setAnimationsEnabled:NO];
        
        fullScreenView= NO;
        
        originalFrame= NSStringFromCGRect(frame);
        
        self.layer.backgroundColor = [UIColor whiteColor].CGColor;
        self.layer.borderColor = [UIColor blackColor].CGColor;
        self.layer.borderWidth = 2;
        
        //Adding the web view
        web=[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        web.delegate = self;
        
        //Adding the imageView
        imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        
        //Adding the map
        map = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)]; 
        
        //Adding the top corner buttons
        //Content Chooser
        contentChooser = [UIButton buttonWithType:UIButtonTypeCustom];
        [contentChooser setBackgroundImage:[UIImage imageNamed:@"corner_topleft"] forState:UIControlStateNormal];
        contentChooser.layer.opacity = 0.2;
        
        [contentChooser addTarget:self 
                           action:@selector(showActionSheet:)
                 forControlEvents:UIControlEventTouchDown];
        contentChooser.frame = CGRectMake(0, 0, 30, 30);
        
        //FullScreen
        fullScreen = [UIButton buttonWithType:UIButtonTypeCustom];
        [fullScreen setBackgroundImage:[UIImage imageNamed:@"corner_topright"] forState:UIControlStateNormal];
        fullScreen.layer.opacity = 0.2;
        [fullScreen addTarget:self 
                       action:@selector(toggleFullScreen:)
             forControlEvents:UIControlEventTouchDown];
        fullScreen.frame = CGRectMake(self.frame.size.width-30, 0, 30, 30);
        fullScreen.hidden =YES;
        
        
        //Spinner
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        spinner.frame = CGRectMake(0, 0, web.frame.size.width, web.frame.size.height);
        spinner.layer.backgroundColor = [UIColor grayColor].CGColor;
        spinner.layer.opacity = 0.5;
        spinner.hidesWhenStopped= YES;
        [web addSubview:spinner];
        
        //[self addSubview: web];
        [self addSubview: contentChooser];
        [self addSubview: fullScreen];
        fullScreen.hidden =NO;
        
        [self setAutoresizesSubviews:YES];
        [ViewContent setAnimationsEnabled:YES];
    }
    return self;
}

-(IBAction)showUrlChooser:(id)sender{

    NSString *title= @"Enter url";

    contentAlert= [[UIAlertView alloc]initWithTitle:@"Enter URL" message:title delegate:self cancelButtonTitle:@"Okay" otherButtonTitles: @"cancel", nil];

    contentAlert.alertViewStyle= UIAlertViewStylePlainTextInput;
    [[contentAlert textFieldAtIndex:0] setKeyboardType: UIKeyboardTypeURL];
    [[contentAlert textFieldAtIndex:0] setText:@"http://www."];
    [[contentAlert textFieldAtIndex:0] setAdjustsFontSizeToFitWidth:YES];
    [contentAlert show];
}

-(IBAction)toggleFullScreen:(id)sender{
    
    [self.superview bringSubviewToFront:self];
    
    if(!fullScreenView){
        originalFrame = NSStringFromCGRect(self.frame);
        
        NSLog(@"%@", originalFrame);

        NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
        [prefs setObject:self.originalFrame forKey:@"Originalframe"];
        
        //make full screen
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options: UIViewAnimationCurveEaseOut
                         animations:^{       
                             
                             self.frame = CGRectMake(0, 0, self.superview.frame.size.width, self.superview.frame.size.height);
                             
                             if ([contentType isEqualToString:@"web"]) {
                                 web.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);                                 
                             }
                             else if ([contentType isEqualToString:@"image"]) {
                                 imageView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);                             
                             }
                             else if ([contentType isEqualToString:@"map"]) {
                                 map.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);                             
                             }
                             
                             contentChooser.hidden = YES;
                             fullScreen.frame = CGRectMake(self.frame.size.width-30, 0, 30, 30);
                             
                             [fullScreen setBackgroundImage:[UIImage imageNamed:@"corner_topright_close"] forState:UIControlStateNormal];
                             fullScreenView = YES;
                         } 
                         completion:^(BOOL finished){
                             NSLog(@"Done!");
                         }];
    }
    
    //close full screen
    else {
        NSLog(@"Exit full screen method called");
        
        
        [fullScreen setBackgroundImage:[UIImage imageNamed:@"corner_topright"] forState:UIControlStateNormal];
        
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options: UIViewAnimationCurveEaseOut
                         animations:^{                                                      
                             NSLog(@"Exiting Full Screen...");

                             NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
                             originalFrame = [prefs objectForKey:@"Originalframe"];
                           self.frame = CGRectFromString(originalFrame) ;

                             
                             if ([contentType isEqualToString:@"web"]) {
                                 web.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
                             }
                             else if ([contentType isEqualToString:@"image"]) {
                                 imageView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
                             }
                             else if ([contentType isEqualToString:@"map"]) {
                                 map.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
                             }

                             
                             fullScreen.frame = CGRectMake(self.frame.size.width-30, 0, 30, 30);
                             
                         } 
                         completion:^(BOOL finished){
                             NSLog(@"Exited...!");
                             
                             
                             contentChooser.hidden =NO;
                             fullScreen.hidden = NO;
                             fullScreenView =NO;
                         }];
    }
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    NSLog(@"enterd the alert view ");
    if(buttonIndex ==0 ){
        NSString *urlAddress= [contentAlert textFieldAtIndex:0].text;
        
        [self setContentToContentView: @"web" contentToAdd:urlAddress];
        
//        //Create an URL object.
//        NSURL *url = [NSURL URLWithString:urlAddress];
//        //URL Requst Object
//        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
//        //Load the request in the UIWebView.
//        [web loadRequest:requestObj];    
//        
//        
//        
//        NSLog(@"%@", urlAddress);
    }
    else {
        
    }
}


- (void)webViewDidStartLoad:(UIWebView *)webView{
    [spinner startAnimating];
    NSLog(@"WebView startedLoading");
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [spinner stopAnimating];
    NSLog(@"WebView stopedLoading");
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {  
    UITouch *touch = [touches anyObject];
    NSLog(@"%@", touch);
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

-(void)stopWorking{
    [web stopLoading];
}

-(IBAction)showActionSheet:(id)sender {
	
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:@"Pick a content to add" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Add an URL",@"Add an image",@"Add a map", nil];
	
    popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
	
    [popupQuery showInView:self];
}



-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	
    NSLog(@"button selected");
	if (buttonIndex == 0) {

//		[self setContentToContentView: @"web" contentToAdd:nil];
        [self showUrlChooser:self];
        
    } else if (buttonIndex == 1) {
		

        [self setContentToContentView:@"image" contentToAdd:nil];
        [self openGallery:self];
        NSLog(@"%d", buttonIndex);
		
    } else if (buttonIndex == 2) {
        
        [self setContentToContentView:@"map" contentToAdd:nil];
        NSLog(@"%d", buttonIndex);
        
    } else if (buttonIndex == 3) {
        NSLog(@"%d", buttonIndex);
    }
	
}

-(void) setContentToContentView:(NSString *)givenContent contentToAdd:(NSString *)contentValue{
    NSLog(@"inside SetContent %@", givenContent);
    
    NSLog(@"Printing self index");
    NSLog(@"%d",  [self getIndex]);
    
	if ([givenContent isEqualToString:@"web"]) {
        [self addSubview:web];
        [self bringSubviewToFront:contentChooser];
        [self bringSubviewToFront:fullScreen];
        
        contentType = @"web";
        
        NSString *urlAddress= contentValue;
        
        if ([engine.contentType count]>0) {
            NSLog(@"Now saving the contentType");
            [engine.contentType replaceObjectAtIndex:[self getIndex] withObject:@"web"];
            [engine.content replaceObjectAtIndex:[self getIndex] withObject:urlAddress];
            
         NSLog(@"Object at index %d is %@ ", [self getIndex],   [engine.content objectAtIndex:[self getIndex]]);
        }
        
        //Create an URL object.
        NSURL *url = [NSURL URLWithString:urlAddress];
        //URL Requst Object
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        //Load the request in the UIWebView.
        [web loadRequest:requestObj];
        
        
    } else if ([givenContent isEqualToString:@"image"]) {
		
        [self addSubview:imageView];
        [self bringSubviewToFront:contentChooser];
        [self bringSubviewToFront:fullScreen];
        
        contentType = @"image";
        
        if ([engine.contentType count]>0) {
            [engine.contentType replaceObjectAtIndex:[self getIndex] withObject:@"image"];
        }
        contentType = @"image";
        
    } else if ([givenContent isEqualToString:@"map"]) {
        [self addSubview:map];
        [self bringSubviewToFront:contentChooser];
        [self bringSubviewToFront:fullScreen];
        
        contentType = @"map";
        if ([engine.contentType count]>0) {
            [engine.contentType replaceObjectAtIndex:[self getIndex] withObject:@"map"];
        }
        contentType = @"map";
        
    }
}


-(void) openGallery:(id) sender
{
    NSLog(@"opening gallery...");
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    
    [imagePicker setDelegate:self];
    [self.window.rootViewController presentModalViewController:imagePicker animated:YES];
}

-(void) takePicture:(id) sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        
    {
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    }
    else
        
    {
        [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
    
    [imagePicker setDelegate:self];
    
    [self.window.rootViewController presentModalViewController:imagePicker animated:YES];
}

-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [imageView setImage:image];
    
    [picker dismissModalViewControllerAnimated:YES];
}

-(void)inJson{
    
}

-(void)setIndex:(int) index{
    self.viewIndex =  [NSString stringWithFormat:@"%d", index];
    [self.viewIndex retain];
}

-(int)getIndex{
    NSLog(@"My index value is-%@", self.viewIndex);
    return  [viewIndex intValue];
}

@end
