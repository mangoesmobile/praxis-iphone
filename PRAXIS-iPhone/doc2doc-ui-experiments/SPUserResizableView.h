
#import <Foundation/Foundation.h>
#import "QuartzCore/QuartzCore.h"
#import "AppDelegate.h"
#import "MMAppEngine.h"

typedef struct SPUserResizableViewAnchorPoint {
    CGFloat adjustsX;
    CGFloat adjustsY;
    CGFloat adjustsH;
    CGFloat adjustsW;
} SPUserResizableViewAnchorPoint;

@protocol SPUserResizableViewDelegate;
@class SPGripViewBorderView;

@interface SPUserResizableView : UIView {
    SPGripViewBorderView *borderView;
    UIView *__weak contentView;
    UIView *__weak fullScreen;
    CGPoint touchStart;
    CGFloat minWidth;
    CGFloat minHeight;
    NSString *contentType;
    NSString *contentString;
    
    // Used to determine which components of the bounds we'll be modifying, based upon where the user's touch started.
    SPUserResizableViewAnchorPoint anchorPoint;
    
    NSMutableArray *allRect;
    
    id <SPUserResizableViewDelegate> __unsafe_unretained delegate;
    
    NSManagedObjectContext *managedObjectContext; 
    
    int grabbed;
    CGPoint grabPosition;
}

//changed to strong
@property(nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, unsafe_unretained) id <SPUserResizableViewDelegate> delegate;

// Will be retained as a subview.
@property (nonatomic, weak) UIView *contentView;
@property (nonatomic, weak) UIView *fullScreen;

// Default is 48.0 for each.
@property (nonatomic) CGFloat minWidth;
@property (nonatomic) CGFloat minHeight;

//changed to strong
@property (nonatomic, strong) NSString *contentType;
@property (nonatomic, strong) NSString *contentString;

// Defaults to YES. Disables the user from dragging the view outside the parent view's bounds.
@property (nonatomic) BOOL preventsPositionOutsideSuperview;

- (void)hideEditingHandles;
- (void)showEditingHandles;
- (void)printYourSelf;

@end

@protocol SPUserResizableViewDelegate <NSObject>

@optional

// Called when the resizable view receives touchesBegan: and activates the editing handles.
- (void)userResizableViewDidBeginEditing:(SPUserResizableView *)userResizableView;

// Called when the resizable view receives touchesEnded: or touchesCancelled:
- (void)userResizableViewDidEndEditing:(SPUserResizableView *)userResizableView;

@end
