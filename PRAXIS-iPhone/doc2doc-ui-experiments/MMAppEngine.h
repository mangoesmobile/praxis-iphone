//
//  MMAppEngine.h
//  FrameWork
//
//  Created by Mahbub Morhsed on 19/6/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

/**
 * SingleTon class that holds the framework
 */

#import <Foundation/Foundation.h>

@interface MMAppEngine : NSObject {    
    NSMutableArray *currentStates;
    NSMutableArray *contentType;
    NSMutableArray *content;
}

@property (strong, retain) NSMutableArray *currentStates;
@property (strong, retain) NSMutableArray *contentType;
@property (strong, retain) NSMutableArray *content;

+(id) sharedManager;
+(void)saveToEngine:(NSMutableArray *) current;
+(void)saveToEngineContentType:(NSMutableArray *) current;
+(void)saveToEngineContent:(NSMutableArray *) current;
+(void)replaceContentType:(int)index withContent:(NSString *)content;

+(void)writeStringToAFile:(NSString *)writeThisString;
+(void)readStringFromFile;

@end
