//
//  MYRootViewController.m
//  FrameWork
//
//  Created by Mahbub Morshed on 9/1/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "MYRootViewController.h"
#import "VIHorizontalTableView.h"
#import "MYTableViewCell.h"

static NSString *const kCellIdentifier = @"kCellIdentifier";

@interface MYRootViewController ()

@end

@implementation MYRootViewController
@synthesize myFrameWorkTable,sharedFrameWorkTable, sharedFrameWorkActivity, myFrameWorkActivity, myFrameWorkArray,sharedFrameWorkArray, load, myFrameWorkArrayofId, sharedFrameWorkArrayofId,lastContent, backToWallButton, loading, invalid;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self update];
    
    self.myFrameWorkTable.layer.borderWidth =1.0;
    self.myFrameWorkTable.layer.borderColor = [UIColor grayColor].CGColor;
    self.sharedFrameWorkTable.layer.borderWidth =1.0;
    self.sharedFrameWorkTable.layer.borderColor =[UIColor grayColor].CGColor;
    
    UINib *cellNib = [UINib nibWithNibName:@"MYTableViewCell" bundle:nil];
    [myFrameWorkTable registerNib:cellNib forCellReuseIdentifier:kCellIdentifier];
    [sharedFrameWorkTable registerNib:cellNib forCellReuseIdentifier:kCellIdentifier];
    
    invalid.hidden= YES;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int number;
    
    if (tableView == myFrameWorkTable) {
        number= [self.myFrameWorkArray count];
    }
    else
        number=[self.sharedFrameWorkArray count];
    
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MYTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    
    if (tableView == myFrameWorkTable) {
        cell.textLabel.text = [self.myFrameWorkArray objectAtIndex:indexPath.row];
    }
    else if (tableView == sharedFrameWorkTable) {
        cell.textLabel.text = [self.sharedFrameWorkArray objectAtIndex:indexPath.row];
    }
    return cell;
}



#pragma mark - UITableView Delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    backToWallButton.hidden =YES;
    
    NSString *msg= nil;
    
    if (tableView== myFrameWorkTable) {
        
        msg= [NSString stringWithFormat:@"Do you want to load framework name %@ ?", [self.myFrameWorkArray objectAtIndex:indexPath.row]];
        
        [myFrameWorkTable reloadData];
        currentTable =1;
    }
    else{
        
        msg= [NSString stringWithFormat:@"Do you want to load framework name %@ ?", [self.sharedFrameWorkArray objectAtIndex:indexPath.row] ];
        
        [sharedFrameWorkTable reloadData];
        currentTable =2;
    }
    
    currentIndex = indexPath.row;
    
    load = [[UIAlertView alloc]initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
    load.delegate= self;
    [load show];
}

-(IBAction)backToWall:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}

-(void)update{
    [self startLoading];
    [self fetchOwned];
    [self fetch_followed];
}


-(void)startLoading{
    self.myFrameWorkArray= [[NSMutableArray alloc]initWithCapacity:1];
    self.myFrameWorkArrayofId= [[NSMutableArray alloc]initWithCapacity:1];
    
    self.sharedFrameWorkArray= [[NSMutableArray alloc]initWithCapacity:1];
    self.sharedFrameWorkArrayofId= [[NSMutableArray alloc]initWithCapacity:1];
    
    self.myFrameWorkTable.hidden= YES;
    self.sharedFrameWorkTable.hidden= YES;
    
    [sharedFrameWorkActivity performSelectorInBackground: @selector(startAnimating) withObject:nil];
    [myFrameWorkActivity performSelectorInBackground: @selector(startAnimating) withObject:nil];
}

-(void)myFrameWorkLoaded{
    [myFrameWorkTable reloadData];
    self.myFrameWorkTable.hidden= NO;
    [myFrameWorkActivity stopAnimating];
}

-(void)sharedFrameWorkLoaded{
    [sharedFrameWorkTable reloadData];
    self.sharedFrameWorkTable.hidden= NO;
    [sharedFrameWorkActivity stopAnimating];
}

-(void)fetchOwned{
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    @"all", @"access_level",
                                    @"2", @"page_no", nil];
    
    
    //request block
    [[RKClient sharedClient] post:@"/shareables/fetch_owned" usingBlock:^(RKRequest *request) {
        [request setParams:params];
        
        request.onDidLoadResponse = ^(RKResponse *response) {
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            
            //parsing the JSON response
            NSString *re= [response bodyAsString];
            NSDictionary *dict= [par objectFromString:re error:nil];
            
            NSArray *frameworks=[dict objectForKey:@"RESPONSE"];
            
            
            for (int i=0; i< [frameworks count]; i++) {
                
                NSDictionary *__response=  [frameworks objectAtIndex:i];
                NSString *name= [__response objectForKey:@"name"];
                NSString *frame_id= [__response objectForKey:@"id"];
                
                NSDictionary *contents=[__response objectForKey:@"contents"];
                
                if (i==1) {
                    NSLog(@"content 1 is %@", contents);
                }
                
                [self.myFrameWorkArray addObject:name];
                [self.myFrameWorkArrayofId addObject:frame_id];
            }
            [self myFrameWorkLoaded];
        };//End of onDidLoadResponse
    }];//End of block
}


-(void)fetch_followed{
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    @"0", @"page_no", nil];
    
    
    //request block
    [[RKClient sharedClient] post:@"/shareables/fetch_public" usingBlock:^(RKRequest *request) {
        [request setParams:params];
        
        request.onDidLoadResponse = ^(RKResponse *response) {
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            
            //parsing the JSON response
            NSString *re= [response bodyAsString];
            NSDictionary *dict= [par objectFromString:re error:nil];
            NSArray *frameworks=[dict objectForKey:@"RESPONSE"];
            
            for (int i=0; i< [frameworks count]; i++) {
                
                NSDictionary *__response=  [frameworks objectAtIndex:i];
                
                NSString *name= [__response objectForKey:@"name"];
                NSString *frame_id= [__response objectForKey:@"id"];
                
                NSLog(@"id is %@", frame_id);
                
                [self.sharedFrameWorkArray addObject:name];
                [self.sharedFrameWorkArrayofId addObject:frame_id];
                
                [self.sharedFrameWorkTable reloadData];
                [self sharedFrameWorkLoaded];
            }
            
        };//End of onDidLoadResponse
    }];//End of block
}

-(void)get_item:(int) id {
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    
    [loading startAnimating];
    
    NSString *sharable_id= [NSString stringWithFormat:@"%d", id];
    
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    sharable_id, @"shareable_id", nil];
    
    
    //request block
    [[RKClient sharedClient] post:@"/shareables/get_item" usingBlock:^(RKRequest *request) {
        [request setParams:params];
        
        request.onDidLoadResponse = ^(RKResponse *response) {
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            
            //parsing the JSON response
            NSString *re= [response bodyAsString];
            NSDictionary *__response= [par objectFromString:re error:nil];
            
            NSLog(@"%@", re);
            
            NSDictionary *inside_json= [__response objectForKey:@"RESPONSE"];
            
            NSString *contentString= [inside_json objectForKey:@"contents"];
            NSDictionary *contentDictionary= [par objectFromString:contentString error:nil];
            
            NSString *width= [contentDictionary objectForKey:@"screen_width"];
            NSString *height= [contentDictionary objectForKey:@"screen_height"];
            NSArray *frames= [contentDictionary objectForKey:@"frames"];
            
            NSLog(@"%d", [frames count]);
            
            NSMutableArray *arrayOfFrame= [[NSMutableArray alloc]initWithCapacity:1];
            NSMutableArray *arrayOfContentType= [[NSMutableArray alloc]initWithCapacity:1];
            NSMutableArray *arrayOfContent= [[NSMutableArray alloc]initWithCapacity:1];
            
            int y=[height intValue];
            int x=[width intValue];
            
            if(y !=0 || x !=0 ){
                
                [prefs setInteger:x forKey:@"xval"];
                [prefs setInteger:y forKey:@"yval"];
                
                self.lastContent= [[NSString alloc]initWithString:re];
                
                for(int i=0; i< [frames count]; i++){
                    NSDictionary *d= [frames objectAtIndex:i];
                    NSString *start_x= [self handleNullValues:[d objectForKey:@"start_x"]];
                    NSString *start_y= [self handleNullValues:[d objectForKey:@"start_y"]];
                    NSString *width= [self handleNullValues:[d objectForKey:@"width"]];
                    NSString *height= [self handleNullValues:[d objectForKey:@"height"]];
                    NSString *content= [self handleNullValues:[d objectForKey:@"content"]];
                    NSString *content_type= [self handleNullValues:[d objectForKey:@"content_type"]];
                    
                    int minW= 320/ x;
                    int minH= 400/ y;
                    
                    CGRect r= CGRectMake([start_x intValue]*minW, [start_y intValue]*minH, [width intValue]*minW, [height intValue]*minH);
                    NSString *v= NSStringFromCGRect(r);
                    
                    [arrayOfFrame addObject:v];
                    [arrayOfContent addObject:content];
                    [arrayOfContentType addObject:content_type];
                }
                
                MMAppEngine *engine= [MMAppEngine sharedManager];
                [engine.currentStates removeAllObjects];
                [engine.content removeAllObjects];
                [engine.contentType removeAllObjects];
                
                engine.currentStates = [NSMutableArray arrayWithArray:arrayOfFrame];
                engine.content = [NSMutableArray arrayWithArray:arrayOfContent];
                engine.contentType = [NSMutableArray arrayWithArray:arrayOfContentType];
                
                [loading stopAnimating];
                backToWallButton.hidden = NO;
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refresh" object:nil];
            }
            else{
                invalid.hidden = NO;
                
                [loading stopAnimating];
                backToWallButton.hidden = NO;
            }
        };//End of onDidLoadResponse
    }];//End of block
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(buttonIndex ==0)
    {
        if (currentTable == 1) {
            [self get_item:[[myFrameWorkArrayofId objectAtIndex: currentIndex] intValue]];
        }
        else
            [self get_item:[[sharedFrameWorkArrayofId objectAtIndex: currentIndex] intValue]];
    }
}

-(NSString *)handleNullValues:(id)checkThis{
    NSLog(@"MyRootViewController handleNull values method called");
    
    NSString *returnedString;
    
    if (checkThis != [NSNull null]) {
        returnedString = (NSString *)checkThis;
    }
    else
    {
    returnedString = @"";
    }
    
    return returnedString;
}
@end
