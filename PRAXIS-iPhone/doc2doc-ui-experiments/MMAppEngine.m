//
//  Engine.m
//  FrameWork
//
//  Created by Mahbub Morhsed on 19/6/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//


#import "MMAppEngine.h"

static MMAppEngine *sharedMyManager = nil;

@implementation MMAppEngine
@synthesize contentType, currentStates,content;


#pragma mark Singleton Methods
+ (id)sharedManager {
    @synchronized(self) {
        if(sharedMyManager == nil)
            sharedMyManager = [[super allocWithZone:NULL] init];
   }
    return sharedMyManager;
}
+ (id)allocWithZone:(NSZone *)zone {
    return [self sharedManager];
}
- (id)copyWithZone:(NSZone *)zone {
    return self;
}
- (id)init {
    if (self = [super init]) {
        self.currentStates = [[NSMutableArray alloc]initWithCapacity:1];
        self.content= [[NSMutableArray alloc]initWithCapacity:1];
        self.contentType= [[NSMutableArray alloc]initWithCapacity:1];
        
        [self.content retain];
        [self.contentType retain];
        [self.currentStates retain];
    }
    return self;
}

+(void)saveToEngine:(NSMutableArray *) current{
    MMAppEngine *engine = [MMAppEngine sharedManager];
    engine.currentStates= [[NSMutableArray alloc]initWithArray:current];
    
    NSLog(@"Current State saved to Engine");
}

+(void)saveToEngineContentType:(NSMutableArray *) current{
    MMAppEngine *engine = [MMAppEngine sharedManager];
    engine.contentType= [[NSMutableArray alloc]initWithArray:current];
    [engine.contentType retain];
    
    NSLog(@"Content types are saved to Engine");
}

+(void)saveToEngineContent:(NSMutableArray *)current{
    MMAppEngine *engine = [MMAppEngine sharedManager];
    engine.content= [[NSMutableArray alloc]initWithArray:current];
    [engine.content retain];
    
    NSLog(@"Contents are saved to Engine");
}

+(void)replaceContentType:(int)index withContent:(NSString *)content{
    MMAppEngine *engine = [MMAppEngine sharedManager];
    [engine.contentType replaceObjectAtIndex:index withObject:content];
    [engine.contentType retain];
}

+(void)writeStringToAFile:(NSString *)writeThisString{
    //TODO: Write the string to file send a NOTIFICATION that will load the string.
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSLog(@"document directory is %@", documentsDirectory);
    
    // the path to write file
    NSString *myFile = [documentsDirectory stringByAppendingPathComponent:@"json_value.txt"];
    
    if ([writeThisString writeToFile:myFile atomically:YES encoding:NSUTF8StringEncoding error:nil]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"loadjson" object:nil];
    }
    
    NSLog(@"file written");
}

+(void)readStringFromFile{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //Read the file
    NSString *documentsDirectoryPath = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectoryPath  stringByAppendingPathComponent:@"json_value.txt"];
    
    NSString* content = [NSString stringWithContentsOfFile:filePath
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];
    NSLog(@"content of written file %@", content);

}
@end
