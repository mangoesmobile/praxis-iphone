//
//  BlankView.h
//  doc2doc-ui-experiments
//
//  Created by Mahbub Morhsed on 21/5/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

/**
 * BlankView
 * 
 * currentTag is worthless
 * Touchcounting holds
 * SuggestionPoint holds
 * points holds starting points of the grids
 * boxHeights and boxWidths is worthless
 * grids holds all the grids
 * Positions holds all the current position
 */

#import <UIKit/UIKit.h>
#import "SPUserResizableView.h"
#import "SuggestionView.h"
#import "QuartzCore/QuartzCore.h"
#import "AppDelegate.h"
#import "MMAppEngine.h"
#import <MapKit/MapKit.h>

@interface EditMode : UIView<SPUserResizableViewDelegate>{
    

    SPUserResizableView *currentlyEditingView;
    SPUserResizableView *lastEditedView;
    
    int current;
    
    //This one helps with tracking whether two finger is placed or one finger is placed and waiting for another.
    NSMutableArray *touchCounting;
    CGPoint suggestionPoint;
    
    //starting points of the grids
    NSMutableArray *points;
    
    //Used while snapping to grid
    NSMutableArray *grids;
    
    //All the current states
    NSMutableArray *currentState;
    //current state contents
    NSMutableArray *currentStateContents;
    //current state conents type
    NSMutableArray *currentStateContentType;

    //Previous state
    NSMutableArray *previousState;
    //previous state contents
    NSMutableArray *previousStateContents;
    //previous state conents type
    NSMutableArray *previousStateContentType;

    
    //Min height and width of a view
    float minHeight; 
    float minWidth;
    
    NSTimer *myTimer;
    
    //For restoring
    NSMutableArray *restore;
    NSMutableArray *restoreState;
    
    //Undo
    UIButton *__weak undo;
    
    //Engine
    MMAppEngine *engine;
    
    //Don't check content type
    BOOL check;
}

-(BOOL)detectCollision: (CGRect)rect;
-(void) redraw;
-(void)undo:(NSNotification*)notification;
-(void) refreshView:(NSNotification *)notification;
//-(void)upDateEngine;
//-(void)LoadJSON;
-(void)initStates;

@property(weak, nonatomic) IBOutlet UIButton *undo;

@property(strong) NSMutableArray *currentState;
//current state contents
@property(strong) NSMutableArray *currentStateContents;
//current state conents type
@property(strong) NSMutableArray *currentStateContentType;

@end

