//
//  MYRootViewController.h
//  FrameWork
//
//  Created by Mahbub Morshed on 9/1/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuartzCore/QuartzCore.h"
#import "MMAppEngine.h"


@class VIHorizontalTableView;

@interface MYRootViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>{
    
    int currentIndex;
    int currentTable;
}


@property(strong) IBOutlet UIButton *backToWallButton;
@property(strong) IBOutlet UIActivityIndicatorView *loading;

@property(strong)NSMutableArray *myFrameWorkArrayofId;
@property(strong)NSMutableArray *myFrameWorkArray;
@property(strong)IBOutlet VIHorizontalTableView *myFrameWorkTable;
@property(strong)IBOutlet UIActivityIndicatorView *myFrameWorkActivity;
@property(strong)NSString *lastContent;

@property(strong)NSMutableArray *sharedFrameWorkArrayofId;
@property(strong)NSMutableArray *sharedFrameWorkArray;
@property(strong)IBOutlet VIHorizontalTableView *sharedFrameWorkTable;
@property(strong)IBOutlet UIActivityIndicatorView *sharedFrameWorkActivity;
@property(strong)IBOutlet UIAlertView *load;

@property(strong)IBOutlet UILabel *invalid;


-(IBAction)backToWall:(id)sender;
-(void)fetchOwned;
-(void)startLoading;
-(void)myFrameWorkLoaded;
-(void)sharedFrameWorkLoaded;
-(void)get_item:(int) id;

-(void)update;

-(NSString *)handleNullValues:(id)checkThis;

@end
