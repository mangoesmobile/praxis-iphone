//
//  BlankView.m
//  doc2doc-ui-experiments
//
//  Created by Mahbub Morhsed on 21/5/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "EditMode.h"

@implementation EditMode
@synthesize undo, currentState, currentStateContents, currentStateContentType;

-(void)initStates{
    //initializing the state and the state contents
    currentState= [[NSMutableArray alloc]initWithCapacity:1];
    currentStateContents =[[NSMutableArray alloc]initWithCapacity:1];
    currentStateContentType =[[NSMutableArray alloc]initWithCapacity:1];

    //initializing the previous state and previous state contents
    previousState=[[NSMutableArray alloc]initWithCapacity:1];
    previousStateContents =[[NSMutableArray alloc]initWithCapacity:1];
    previousStateContentType =[[NSMutableArray alloc]initWithCapacity:1];
    
    restore=[[NSMutableArray alloc]initWithCapacity:1];
    restoreState=[[NSMutableArray alloc]initWithCapacity:1];
    
    // Clear Arrays
    [currentState removeAllObjects];
    [previousState removeAllObjects];
    [restore removeAllObjects];
    [restoreState removeAllObjects];
}

//At start remove all the saved rectangles
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        //This is to know whether to load JSON or not
        NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
        int load= [prefs integerForKey:@"loadjson"];
        
        //the singleton class that stores the framework
        engine = [MMAppEngine sharedManager];
        
        [self initStates];
        
        self.backgroundColor =[UIColor whiteColor];
        self.multipleTouchEnabled= YES;
        
        [prefs setInteger:0 forKey:@"suggestionViewExist"];

        check= NO;
        if (load==1) {
            [prefs setInteger:0 forKey:@"loadjson"];
        }
    
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(undo:) name:@"undo" object:nil];
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(LoadJSON:) name:@"loadjson" object:nil];
        
           [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshView:) name:@"refresh" object:nil];
    }
    return self;
}


/**
 * Calculates the distance between two points
 *
 * @param CGPoint point1, CGPoint point2
 * @returns the distance in CGFloat
 */

static CGFloat distanceBetweenTwoPoints(CGPoint point1, CGPoint point2) {
    CGFloat dx = point2.x - point1.x;
    CGFloat dy = point2.y - point1.y;
    return sqrt(dx*dx + dy*dy);
};


//draws the grid
- (void)drawRect:(CGRect)rect
{
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    int x=[prefs integerForKey:@"xval"];
    int y=[prefs integerForKey:@"yval"];

    
    points = [[NSMutableArray alloc]initWithCapacity:((x+1)*(y+1))];
    grids = [[NSMutableArray alloc] initWithCapacity:((x+1)*(y+1))];
    

    CGContextRef context = UIGraphicsGetCurrentContext( );
    
    NSInteger suggestionViewExist= [prefs integerForKey:@"suggestionViewExist"];
    

    if (suggestionViewExist == 1) {
        CGContextSetStrokeColorWithColor(context, [UIColor greenColor].CGColor);
    }
    else {
        CGContextSetStrokeColorWithColor(context, [UIColor groupTableViewBackgroundColor].CGColor);
    }
    
    if (context == NULL){
//        NSLog(@"Context is Null");
    }
    CGContextSaveGState(context);
    
    
    int width = rect.size.width ;
    int height = rect.size.height ;
    
    
//    minWidth = width/x;
//    minHeight= height/y;

    minWidth = 320/x;
    minHeight= 400/y;
    
//    NSLog(@"minwidth %f", minWidth);
//    NSLog(@"minheight %f", minHeight);
    
    [prefs setInteger:minHeight forKey:@"minH"];
    [prefs setInteger:minWidth forKey:@"minW"];
    
    int i = 0 ;
    
    // Draw a grid
    // first the vertical lines
    for( i = 0 ; i <= width ; i=i+minWidth ) {
        CGPoint verticalLine[2] = { CGPointMake(i, 0), CGPointMake(i, height)};
        CGContextStrokeLineSegments(context, verticalLine, 2);
    } // then the horizontal lines
    
    for( i = 0 ; i <= height ; i=i+minHeight ) {
        CGPoint horizontalLine[2] = { CGPointMake(0, i), CGPointMake(width,i)};
        CGContextStrokeLineSegments(context, horizontalLine, 2);
    } // actually draw the grid

    
    //calculating box starting points
    for( i = 0 ; i <= width+minWidth ; i=i+minWidth ) {
        for(int j = 0 ; j <= height+minHeight ; j=j+minHeight ) {
            CGPoint point= CGPointMake(i, j);
            [points addObject:NSStringFromCGPoint(point)];
        }
    }

    
    //Calculating grid rects
    for(i=0; i< [points count]; i++){
        CGPoint start= CGPointFromString([points objectAtIndex:i]);
        CGRect rect1= CGRectMake(start.x, start.y, minWidth, minHeight);
        [grids addObject: NSStringFromCGRect(rect1)];
    }
    

    //Drawing Circles in touchPoints
    for(i=0; i< [points count]; i++){
        CGPoint currentPoint= CGPointFromString([points objectAtIndex:i]);
        CGContextFillEllipseInRect(context, CGRectMake(currentPoint.x-2.5,currentPoint.y-2.5, 5,5));
    }
//    NSLog(@"end of drawRect");
    [self redraw];
}


-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
//    NSLog(@"Touches moved");
}

/*
 1 finger placed=>
 ** add the current touch position to touchCounting.
 ** If no suggestionView
 **** Add a suggestionView
 ** else
 **** create a ResizableView
 ****** if no collision
 ******** save the position.
 ****** else
 ******** remove the ResizableView
 
 
 2 finger placed=>
 ** create a ResizableView
 ** if no collision
 ***** save the position.
 ** else
 ***** remove the ResizableView
 */

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    previousState = [[NSMutableArray alloc]initWithArray:currentState];
    
    //1st September
    previousStateContents = [[NSMutableArray alloc]initWithArray:currentStateContents];
    previousStateContentType = [[NSMutableArray alloc]initWithArray:currentStateContentType];
    
    switch ([touches count]) {
        case 1:
        {
            // handle a single touch
            UITouch *touch = [touches anyObject];
            CGPoint startTouchPosition = [touch locationInView:self];
            
            NSString *touchToStore= NSStringFromCGPoint(startTouchPosition);
            [touchCounting addObject :touchToStore];
            
            NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
            NSInteger suggestionViewExist= [prefs integerForKey:@"suggestionViewExist"];
            
            if (suggestionViewExist == 1) {
//                NSLog(@"Multiple Touch detected");
                
                // handle multi touch
                CGPoint one= startTouchPosition;
                CGPoint two= suggestionPoint;
                
                for (int i=0; i< [grids count]; i++) {
                    CGRect rect= CGRectFromString([grids objectAtIndex:i]);
                    if(CGRectContainsPoint(rect, one)== YES){
                        one= rect.origin;
                    }
                    
                    if(CGRectContainsPoint(rect, two)== YES){
                        two= rect.origin;
                    }
                }
                
                // This one makes sure the point at right is always Point one
                if(one.x> two.x){
                    CGPoint three= one;
                    one= two;
                    two= three;
                }
                
                CGPoint lowerLeft;
                CGPoint upperRight;
                
                //Calulate the lowerleft and upperRight point of the new box
                if((one.y+minHeight) > (two.y+minHeight)){
                    lowerLeft.y = one.y+minHeight;
                }
                else{
                    lowerLeft.y= two.y+minHeight;
                }
                
                if(one.x < two.x){
                    lowerLeft.x= one.x;
                    
                }
                else {
                    lowerLeft.x= two.x;
                }
                
                if((two.x+minWidth) > (one.x+minWidth)){
                    upperRight.x = two.x+minWidth;
                }
                else {
                    upperRight.x= one.x+minWidth;
                }
                
                if(two.y< one.y){
                    upperRight.y= two.y;
                }
                else {
                    upperRight.y= one.y;
                }
                
                CGFloat p1;
                CGFloat p2;
                CGFloat h;
                CGFloat w;
                
                
                p1= lowerLeft.x;
                p2= upperRight.y;
                
                h= lowerLeft.y-p2;
                w= upperRight.x-p1;
                
                CGRect parentFrame = CGRectMake(p1,p2,w,h);

                SPUserResizableView *imageResizableView = [[SPUserResizableView alloc] initWithFrame:parentFrame];
                imageResizableView.contentView = nil;
                imageResizableView.contentType= @"none";
                imageResizableView.delegate = self;
                imageResizableView.layer.backgroundColor= [UIColor whiteColor].CGColor;
                imageResizableView.layer.opacity =0.9;
                imageResizableView.layer.borderWidth= 2.0;
                imageResizableView.tag= 10;
                imageResizableView.minWidth= minWidth;
                imageResizableView.minHeight=minHeight;
                
                [self addSubview:imageResizableView];
                //After adding a view redraw everything
                [self redraw];
                

                if ([self detectCollision:imageResizableView.frame]== YES) {
                    currentState= previousState;
                    
                    //1st September
                    currentStateContents = previousStateContents;
                    currentStateContentType= previousStateContentType;
                    
                    //[self redraw];
                
                    [UIView animateWithDuration:0.5
                                          delay:0.0
                                        options: UIViewAnimationCurveEaseOut
                                     animations:^{
//                                         NSValue *restoreVaule= [restore lastObject];
//                                         CGRect parentFrame= [restoreVaule CGRectValue];
                                         NSString *restoreVaule= [restore lastObject];
                                         CGRect parentFrame= CGRectFromString(restoreVaule);
                                         imageResizableView.frame= parentFrame;
                                     }
                                     completion:^(BOOL finished){
//                                         NSLog(@"Done!");
                                     }];
                    
                    
                }
                else {
                    //Added to currentState
                    CGRect rectToAdd= imageResizableView.frame;
//                    NSValue *v= [NSValue valueWithCGRect:rectToAdd];
                    NSString *v = NSStringFromCGRect(rectToAdd);
                    [currentState addObject:v];
                    
                    //1st September
                    [currentStateContentType addObject:@"web"];
                    [currentStateContents addObject:@"http://www.google.com"];
                    
                    [engine.contentType addObject:@"web"];
                    [engine.content addObject:@"http://www.google.com"];
                }
                break;
            }
            
            //When there is no suggestion view
            else{
                CGRect parentFrame= CGRectMake(startTouchPosition.x, startTouchPosition.y-150, 150, 150);
                
                SuggestionView *suggest= [[SuggestionView alloc]initWithFrame:parentFrame];
                suggest.tag= 420;
                [self addSubview:suggest];
                [self insertSubview:suggest atIndex:0];
                [self setNeedsDisplay];
                suggestionPoint = startTouchPosition;
                
                NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
                [prefs setInteger:1 forKey:@"suggestionViewExist"];
                
                [self insertSubview:suggest atIndex:0];
            }
            
            break;
        }
        default:
        {
            // handle multi touch
            UITouch *touch1 = [[touches allObjects] objectAtIndex:0];
            UITouch *touch2 = [[touches allObjects] objectAtIndex:1];
            
            CGPoint one= [touch1 locationInView:self];
            CGPoint two= [touch2 locationInView:self];
            
            
            for (int i=0; i< [grids count]; i++) {
                CGRect rect= CGRectFromString([grids objectAtIndex:i]);
                if(CGRectContainsPoint(rect, one)== YES){
                    
                    one= rect.origin;
                }
                
                if(CGRectContainsPoint(rect, two)== YES){
                    
                    two= rect.origin;
                }
            }
            
            // This one makes sure the point at right is always Point one
            if(one.x> two.x){
                CGPoint three= one;
                one= two;
                two= three;
            }
            
            CGPoint lowerLeft;
            CGPoint upperRight;
            
            //Calulate the lowerleft and upperRight point of the new box
            if((one.y+minHeight) > (two.y+minHeight)){
                lowerLeft.y = one.y+minHeight;
            }
            else{
                lowerLeft.y= two.y+minHeight;
            }
            
            if(one.x < two.x){
                lowerLeft.x= one.x;
                
            }
            else {
                lowerLeft.x= two.x;
            }
            
            if((two.x+minWidth) > (one.x+minWidth)){
                upperRight.x = two.x+minWidth;
            }
            else {
                upperRight.x= one.x+minWidth;
            }
            
            if(two.y< one.y){
                upperRight.y= two.y;
            }
            else {
                upperRight.y= one.y;
            }
            
            CGFloat p1;
            CGFloat p2;
            CGFloat h;
            CGFloat w;
            
            p1= lowerLeft.x;
            p2= upperRight.y;
            
            h= lowerLeft.y-p2;
            w= upperRight.x-p1;
            
            CGRect parentFrame = CGRectMake(p1,p2,w,h);
            
            SPUserResizableView *imageResizableView = [[SPUserResizableView alloc] initWithFrame:parentFrame];
            imageResizableView.contentView = nil;
            imageResizableView.contentType = @"none";
            imageResizableView.delegate = self;
            imageResizableView.layer.backgroundColor= [UIColor whiteColor].CGColor;
            imageResizableView.layer.opacity =0.9;
            imageResizableView.layer.borderWidth= 2.0;
            imageResizableView.tag= 10;
            imageResizableView.minWidth= minWidth;
            imageResizableView.minHeight=minHeight;
            
            [self addSubview:imageResizableView];
            
            //After adding the view Redraw
            [self redraw];
            
            if ([self detectCollision:imageResizableView.frame]== YES) {
                
                currentState= previousState;

                [UIView animateWithDuration:0.5
                                      delay:0.0
                                    options: UIViewAnimationCurveEaseOut
                                 animations:^{
                                     
                                     CGRect parentFrame= CGRectMake(0, 0, 0, 0);
                                     imageResizableView.frame= parentFrame;
 
                                 }
                                 completion:^(BOOL finished){
//                                     NSLog(@"Done!");
                                 }];
            }
            else {

                //Added to currentState
                CGRect rectToAdd= imageResizableView.frame;
//                NSValue *v= [NSValue valueWithCGRect:rectToAdd];
                NSString *v= NSStringFromCGRect(rectToAdd);
                [currentState addObject:v];
                
                //1st September
                [currentStateContentType addObject:@"web"];
                [currentStateContents addObject:@"http://www.google.com"];
                
                //29th August
                [engine.contentType addObject:@"web"];
                [engine.content addObject:@"http://www.google.com"];
            }
            break;
        }
            
    }
}


/*
 ** remove suggestionView
 ** Redraw
 */

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint position = [touch locationInView:self];
    NSString *toRemove=NSStringFromCGPoint(position);
    [touchCounting removeObject: toRemove];
    
    SuggestionView *suggestion=(SuggestionView *)[self viewWithTag:420];
    [suggestion removeFromSuperview];
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    [prefs setInteger:0 forKey:@"suggestionViewExist"];
    
    [self setNeedsDisplay];
}


- (void)userResizableViewDidBeginEditing:(SPUserResizableView *)userResizableView {
    
    previousState= [[NSMutableArray alloc]initWithArray:currentState];
    previousStateContents = [[NSMutableArray alloc]initWithArray:currentStateContents];
    previousStateContentType = [[NSMutableArray alloc]initWithArray:currentStateContentType];

    
    //    work inside this
    //    CGRect rectToAdd= userResizableView.frame;
    //    NSValue *val= [NSValue valueWithCGRect:rectToAdd];
    //    NSLog(@"index of the current object- %d",[currentState indexOfObject:val]);
    //    work inside this

    [self bringSubviewToFront:userResizableView];

    [restoreState addObject:previousState];

    //TODO: Save and remove the contenttype and content
    //removing the current rect
    CGRect rectToRemove= userResizableView.frame;
    NSString *v= NSStringFromCGRect(rectToRemove);

    current=[currentState indexOfObject:v];
    [currentState removeObject:v];
    
//    NSLog(@"value of current is- %d", current);
    
    [currentStateContents removeObjectAtIndex:current];
    [currentStateContentType removeObjectAtIndex:current];
    [currentlyEditingView hideEditingHandles];
    currentlyEditingView = userResizableView;
    
    CGRect rectToRestore= userResizableView.frame;
    NSString *restoreValue= NSStringFromCGRect(rectToRestore);
    [restore addObject:restoreValue];
    
    
    [MMAppEngine saveToEngine:currentState];
    [MMAppEngine saveToEngineContent:currentStateContents];
    [MMAppEngine saveToEngineContentType:currentStateContentType];
    
    for (NSString *s in currentState) {
//        NSLog(@"%@", s);
    }
}

- (void)userResizableViewDidEndEditing:(SPUserResizableView *)userResizableView {
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    
    //If less than minimum remove
    if (userResizableView.frame.size.width < userResizableView.minWidth*0.5 || userResizableView.frame.size.height < userResizableView.minHeight*0.5) {
        [self redraw];

        UIImageView*animationView = [[UIImageView alloc] initWithFrame:CGRectMake(userResizableView.frame.origin.x, userResizableView.frame.origin.y, 48, 48)];
        animationView.animationImages = [NSArray arrayWithObjects:
                                         [UIImage imageNamed:@"cloud0.png"],
                                         [UIImage imageNamed:@"cloud.png"],nil];
        
        animationView.animationDuration = 0.5;
        animationView.animationRepeatCount = 1;
        [animationView startAnimating];
        [self addSubview:animationView];
    }
    else {
        CGPoint finalPoint;
        
        int x=[prefs integerForKey:@"xval"];
        int y=[prefs integerForKey:@"yval"];
        
        minWidth = 320/x;
        minHeight= 460/y;
        
        CGFloat minDistance= 10000;
        
        for (int i=0; i< [points count]; i++) {
            CGPoint comparingWith= CGPointFromString([points objectAtIndex:i]);
            
            if(minDistance > distanceBetweenTwoPoints(userResizableView.frame.origin,comparingWith)){
                minDistance = distanceBetweenTwoPoints(userResizableView.frame.origin,comparingWith);
                finalPoint= CGPointFromString([points objectAtIndex:i]);
            }
        }
        
        CGPoint upperLeft= CGPointMake(finalPoint.x + userResizableView.frame.size.width, finalPoint.y);
        CGPoint lowerRight= CGPointMake(finalPoint.x, finalPoint.y + userResizableView.frame.size.height);
        CGPoint finalUpper;
        CGPoint finalLower;
        
        minDistance= 500;
        
        for (int i=0; i< [points count]; i++) {
            
            CGPoint comparingWith= CGPointFromString([points objectAtIndex:i]);
            
            if(minDistance > distanceBetweenTwoPoints(upperLeft,comparingWith)){
                minDistance = distanceBetweenTwoPoints(upperLeft,comparingWith);
                finalUpper= CGPointFromString([points objectAtIndex:i]);
            }
        }
        
        minDistance= 500;
        
        for (int i=0; i< [points count]; i++) {
            CGPoint comparingWith= CGPointFromString([points objectAtIndex:i]);
            
            if(minDistance > distanceBetweenTwoPoints(lowerRight,comparingWith)){
                minDistance = distanceBetweenTwoPoints(lowerRight,comparingWith);
                finalLower= CGPointFromString([points objectAtIndex:i]);
            }
        }
        
        float h= distanceBetweenTwoPoints(finalPoint,finalLower);
        float w= distanceBetweenTwoPoints(finalPoint,finalUpper);
        
        
        [UIView setAnimationsEnabled:YES];
        
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options: UIViewAnimationCurveEaseInOut
                         animations:^{
                             userResizableView.frame= CGRectMake(finalPoint.x, finalPoint.y, w, h);
                             
                             lastEditedView = userResizableView;
                             userResizableView.backgroundColor= [UIColor whiteColor];
                             
                         }
                         completion:^(BOOL finished){
                             
                             if ([self detectCollision:userResizableView.frame]== YES) {
                                 [UIView animateWithDuration:1.0
                                                       delay:0.0
                                                     options: UIViewAnimationCurveEaseOut
                                                  animations:^{
                                                      userResizableView.layer.borderColor=[UIColor redColor].CGColor;
                                                      userResizableView.layer.shadowColor= [UIColor redColor].CGColor;
                                                      userResizableView.layer.shadowOpacity= 1.0;
                                                      
                                                      NSString *restoreVaule=[restore lastObject];
                                                      CGRect rectToRestore= CGRectFromString(restoreVaule);
                                                      userResizableView.frame= rectToRestore;

                                                  }
                                                  completion:^(BOOL finished){
                                                      currentState= [[NSMutableArray alloc]initWithArray:previousState];
                                                      
                                                      //1st September
                                                      previousStateContents = [[NSMutableArray alloc]initWithArray:currentStateContents];
                                                      previousStateContentType = [[NSMutableArray alloc]initWithArray:currentStateContentType];

                                                      
                                                      userResizableView.layer.borderColor=[UIColor blackColor].CGColor;
                                                      userResizableView.layer.shadowColor= [UIColor redColor].CGColor;
                                                      userResizableView.layer.shadowOpacity= 0.0;
                                                      
                                                  }];
                             }
                             else {
                                 //Added to currentState
                                 CGRect rectToAdd= userResizableView.frame;
                                 NSString *v = NSStringFromCGRect(rectToAdd);
                                 [currentState addObject:v];
                                 
                                 //1st September
                                 [currentStateContentType addObject:@"web"];
                                 [currentStateContents addObject:@"http://www.google.com"];
                                 
                                 [engine.contentType addObject:@"web"];
                                 [engine.content addObject:@"http://www.google.com"];
                                 
                                 //Come back here
                                 
//                                 NSLog(@"value of current is %d",current);
                                 /*
                                 if (current != 0) {
                                     NSString *toReplace=[currentStateContents objectAtIndex:current];
                                     [currentStateContents removeObjectAtIndex:current];
                                     NSLog(@"object removed");
                                     [currentStateContents addObject: toReplace];
                                     NSLog(@"object added");
                                     
                                     toReplace = [currentStateContentType objectAtIndex:current];
                                     [currentStateContentType removeObjectAtIndex: current];
                                     [currentStateContentType addObject: toReplace];
                                 }
                                  */

                             }
                         }];
    }
    
    
    for (NSString *s in currentState) {
        NSLog(@"%@", s);
    }
    
    [MMAppEngine saveToEngine:currentState];
    
    //1st September
    [MMAppEngine saveToEngineContentType: currentStateContentType];
    [MMAppEngine saveToEngineContent: currentStateContents];
}

- (void)hideEditingHandles {
    // We only want the gesture recognizer to end the editing session on the last
    // edited view. We wouldn't want to dismiss an editing session in progress.
    [lastEditedView hideEditingHandles];
}

/**
 While redrawing first remove everything from the view. 
 Than from the currentState array redraw everything.
 **/
-(void) redraw{
//    NSLog(@"Redrawing");
    [[self subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    int j;

    for (j=0; j< [currentState count]; j++) {

        NSString *v= [currentState objectAtIndex:j];
        CGRect parentFrame= CGRectFromString(v);

        SPUserResizableView *imageResizableView = [[SPUserResizableView alloc] initWithFrame:parentFrame];
        imageResizableView.delegate = self;
        imageResizableView.layer.backgroundColor= [UIColor whiteColor].CGColor;
        imageResizableView.layer.opacity =0.9;
        imageResizableView.layer.borderWidth= 2.0;
        imageResizableView.tag= 10;
        imageResizableView.minWidth= minWidth;
        imageResizableView.minHeight=minHeight;
        
        [self addSubview:imageResizableView];
    }

    currentlyEditingView= nil;

    [MMAppEngine saveToEngine:currentState];
    [MMAppEngine saveToEngineContent: currentStateContents];
    [MMAppEngine saveToEngineContentType: currentStateContentType];
}

-(BOOL)detectCollision: (CGRect)rect{
    NSUInteger i, count = [currentState count];
    
    BOOL result= NO;
    
    if(count>0){
        
        for (i=0; i<count; i++) {
            NSString *temp= [currentState objectAtIndex:i];
            CGRect comparingWith= CGRectFromString(temp);
            
            if (CGRectIntersectsRect(comparingWith, rect)) {
                result =YES;
                break;
            }
        }
    }
    return result;
}

-(void)undo:(NSNotification*)notification{
    
    if([restoreState count] > 0){
        currentState = [NSMutableArray arrayWithArray:[restoreState lastObject]];
        [restoreState removeLastObject];
        [self redraw];
//        NSLog(@"%@", currentState);
    }//Eof if
    else {
        currentState= [[NSMutableArray alloc]initWithCapacity:1];
        previousState= [[NSMutableArray alloc]initWithCapacity:1];
        [currentState removeAllObjects];
        [self redraw];
    }//Eof else
    
    
    for (NSString *s in currentState) {
//        NSLog(@"%@", s);
    }//Eof for
    
    [MMAppEngine saveToEngine:currentState];
}//Eof undo

-(void)upDateEngine{
    
    [MMAppEngine saveToEngine:currentState];
//    NSLog(@"saved");
    
    NSMutableArray *contentTypes= [[NSMutableArray alloc]initWithCapacity:10];
    
    for (int i=0; i<100; i++) {
        [contentTypes addObject:@"none"];
    }
    
    NSMutableArray *all= [[NSMutableArray alloc]initWithArray:[self subviews]];
    
    for(int i=0; i< [all count]; i++){
        
        if([[all objectAtIndex:i] isKindOfClass:[SPUserResizableView class]]){
            SPUserResizableView *a= [all objectAtIndex:i];
            [a printYourSelf];
            
            
            if (![a.contentType isEqualToString:nil]){
//                NSLog(@"content type is- %@", a.contentType);
                [contentTypes replaceObjectAtIndex:i withObject:a.contentType];
            }
            
            
        }//Eof if
    }//Eof For
    
    [MMAppEngine saveToEngineContentType: contentTypes];
//    NSLog(@"Engine updated");
}

-(void)LoadJSON:(NSNotification *)notification{
    RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,  NSUserDomainMask, YES);
    NSString *documentsDirectoryPath = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectoryPath  stringByAppendingPathComponent:@"json_value.txt"];
    
    
    NSString* content = [NSString stringWithContentsOfFile:filePath
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];

    NSDictionary *contents= [par objectFromString:content error:nil];
    
    int xval = [[contents objectForKey:@"screen_width"]intValue];
    int yval = [[contents objectForKey:@"screen_height"]intValue];
    
    NSMutableArray *frames= [contents objectForKey:@"frames"];
    
//    for(int i=0; i< [frames count]; i++){
//        NSDictionary *frame= [frames objectAtIndex:i];
//        NSString *index= [frame objectForKey:@"index"];
//        NSString *start_x= [frame objectForKey:@"start_x"];
//        NSString *start_y= [frame objectForKey:@"start_y"];
//        NSString *width= [frame objectForKey:@"width"];
//        NSString *height= [frame objectForKey:@"height"];
//        NSString *content= [frame objectForKey:@"content"];
//        NSString *content_type= [frame objectForKey:@"content_type"];
//        
//        NSLog(@"index is %@, start x is %@, start_y is %@, width is %@, height is %@, content is %@, content type is %@", index, start_x, start_y, width,height, content, content_type);
//    }

    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    [prefs setInteger:xval forKey:@"xval"];
    [prefs setInteger:yval forKey:@"yval"];
    
//    NSLog(@"xvalues that is saved is - %d", xval);
//    NSLog(@"yvalues that is saved is - %d", yval);

    [[self subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self setNeedsDisplay];
    
//currentState= [[NSMutableArray alloc]initWithArray:engine.currentStates];
    /*currentStateContents = [[NSMutableArray alloc]initWithArray:engine.content];
    currentStateContentType = [[NSMutableArray alloc]initWithArray:engine.contentType];
    */

}


-(void) refreshView:(NSNotification *)notification{
    
    [self setNeedsDisplay];
    
    self.currentState= [NSMutableArray arrayWithArray:engine.currentStates];
    self.currentStateContents=[NSMutableArray arrayWithArray:engine.content];
    self.currentStateContentType = [NSMutableArray arrayWithArray:engine.contentType];
    
    [currentState retain];
    [currentStateContents retain];
    [currentStateContentType retain];

    [self redraw];
}

@end
