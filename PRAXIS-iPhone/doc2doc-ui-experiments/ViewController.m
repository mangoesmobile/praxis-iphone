//
//  ViewController.m
//  doc2doc-ui-experiments
//
//  Created by Mahbub Morhsed on 9/5/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#define kAesKey @"jc0gvDzyg3lpwmQyTbHhBSFlD4wkiLy8"

#import "ViewController.h"

@implementation ViewController
@synthesize undo, mode, activity, blackCover, praxisLogo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.view.backgroundColor=[UIColor whiteColor];
        currentMode=0;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    
    self.view.backgroundColor= [UIColor whiteColor];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    praxisLogo.animationImages = [NSArray arrayWithObjects:
                                  [UIImage imageNamed:@"Tri-01.jpg"],
                                  [UIImage imageNamed:@"Tri-02.jpg"],
                                  [UIImage imageNamed:@"Tri-03.jpg"],
                                  [UIImage imageNamed:@"Tri-04.jpg"],
                                  [UIImage imageNamed:@"Tri-05.jpg"],
                                  [UIImage imageNamed:@"Tri-06.jpg"],
                                  [UIImage imageNamed:@"Tri-07.jpg"],
                                  [UIImage imageNamed:@"Tri-08.jpg"],
                                  nil];
    
    praxisLogo.animationDuration = 2;
    praxisLogo.animationRepeatCount = 0;

    
    [self.view bringSubviewToFront:blackCover];
    [self.view bringSubviewToFront:praxisLogo];
    [self.view bringSubviewToFront:activity];
    
    login = [[UIAlertView alloc]initWithTitle:@"Enter login credentials" message:@"" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
    login.alertViewStyle= UIAlertViewStyleLoginAndPasswordInput;
    login.delegate= self;
    
    [login show];
    
    xInput = [[UIAlertView alloc]initWithTitle:@"Enter X value" message:@"X value is" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles: nil];
    xInput.alertViewStyle= UIAlertViewStylePlainTextInput;
    xInput.delegate= self;
    [[xInput textFieldAtIndex:0]setKeyboardType:UIKeyboardTypeNumberPad];

    
    yInput= [[UIAlertView alloc]initWithTitle:@"Enter Y value" message:@"Y value is" delegate:self cancelButtonTitle:@"okay" otherButtonTitles: nil];
    yInput.alertViewStyle= UIAlertViewStylePlainTextInput;
    yInput.delegate= self;
    [[yInput textFieldAtIndex:0]setKeyboardType:UIKeyboardTypeNumberPad];
}

- (NSString *) LoginWIthUserName:(NSString *)username password:(NSString *)password{
    __block NSString *returnString= nil;
    
    int i =16-[password length];
    for ( ; i>0 ; i--) {
        password=[password stringByAppendingString:@" "];
    }
    
    NSData *passdata=[password dataUsingEncoding:NSUTF8StringEncoding];
    
    NSCharacterSet *charsToRemove = [NSCharacterSet characterSetWithCharactersInString:@"< >"];
    NSString *s = [[[passdata AES256EncryptWithKey:kAesKey] description]stringByTrimmingCharactersInSet:charsToRemove];
    NSCharacterSet *charsToRemove1 = [NSCharacterSet characterSetWithCharactersInString:@" "];
    NSString *s1= [s stringByTrimmingCharactersInSet:charsToRemove1];
    
    NSString *stringWithoutSpaces = [s1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    username, @"username",
                                    stringWithoutSpaces, @"enc_password", nil];
    
    //request block
    [[RKClient sharedClient] post:@"/authorize/login" usingBlock:^(RKRequest *request) {
        [request setParams:params];
        
        request.onDidLoadResponse = ^(RKResponse *response) {
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            
            //parsing the JSON response
            NSString *re= [response bodyAsString];
            NSDictionary *dict= [par objectFromString:re error:nil];
            NSString *status=[dict objectForKey:@"STATUS_MESSAGE"];
            NSString *statusCode=[dict objectForKey:@"STATUS_CODE"];
            NSString *error_log=[dict objectForKey:@"ERROR_LOG"];
            
            statusCode = [NSString stringWithFormat:@"%@", statusCode];
            error_log=[NSString stringWithFormat:@"%@", error_log];
            
            returnString = [NSString stringWithString:status];
            
            if([statusCode isEqualToString:@"200"]){
                NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
                [prefs setObject:@"200" forKey:@"logged_in"];
                
                [self.activity stopAnimating];
                [praxisLogo stopAnimating];
                
                [xInput show];
                
            }//End of If
            else{
                login = [[UIAlertView alloc]initWithTitle:error_log message:@"" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                login.alertViewStyle= UIAlertViewStyleLoginAndPasswordInput;
                login.delegate= self;
                [login show];
            }
        };//End of onDidLoadResponse
    }];//End of block
    
    return returnString;
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    
    if (alertView == login) {
        NSString *userName= [login textFieldAtIndex:0].text;
        NSString *passWord= [login textFieldAtIndex:1].text;
        
        self.activity.hidden = NO;
        [self.view bringSubviewToFront:activity];
        [activity performSelectorInBackground: @selector(startAnimating) withObject:nil];
        [praxisLogo performSelectorInBackground: @selector(startAnimating) withObject:nil];
        
        [self LoginWIthUserName:userName password:passWord];
    }
    
    
    if(alertView== xInput){
        if (buttonIndex == 0) {
            NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
            [prefs setInteger:0 forKey:@"loadjson"];
            
            NSString *entered=[xInput textFieldAtIndex:0].text;
            
            if( [entered length] >= 1 )
            {
                [yInput show];
                
                int x= [entered intValue];
                [prefs setInteger:x forKey:@"xval"];
            }
            else
            {
                [xInput show];
            }
        }
        else {
            NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
            [prefs setInteger:1 forKey:@"loadjson"];
            
            //JSON Loading
            NSString *myText;
            
            NSString *filePath = [[NSBundle mainBundle] pathForResource:@"json_value" ofType:@"txt"];
            if (filePath) {
                myText = [NSString stringWithContentsOfFile:filePath usedEncoding:nil error:nil];
                if (myText) {
//                    NSLog(@"%@", myText);
                }
            }
            
            
            //parse json and save the values
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            NSDictionary *dict= [par objectFromString:myText error:nil];
            
            NSMutableArray *arrayOfFrame= [[NSMutableArray alloc]initWithCapacity:1];
            NSMutableArray *arrayOfContentType= [[NSMutableArray alloc]initWithCapacity:1];
            NSMutableArray *arrayOfContent= [[NSMutableArray alloc]initWithCapacity:1];
            
            NSMutableArray *currentJSONFrames= [dict objectForKey:@"frames"];
            
            int x= [[dict objectForKey:@"screen_width"] intValue];
            int y= [[dict objectForKey:@"screen_height"] intValue];
            
            for(int i=0; i< [currentJSONFrames count]; i++){
                NSDictionary *d= [currentJSONFrames objectAtIndex:i];
                NSString *start_x= [d objectForKey:@"start_x"];
                NSString *start_y= [d objectForKey:@"start_y"];
                NSString *width= [d objectForKey:@"width"];
                NSString *height= [d objectForKey:@"height"];
                NSString *content= [d objectForKey:@"content"];
                NSString *content_type= [d objectForKey:@"content_type"];
                
                
                int minW= 320/ x;
                int minH= 400/ y;
   
                CGRect r= CGRectMake([start_x intValue]*minW, [start_y intValue]*minH, [width intValue]*minW, [height intValue]*minH);
                NSValue *v= [NSValue valueWithCGRect:r];
                
                [arrayOfFrame addObject:v];
                [arrayOfContent addObject:content];
                [arrayOfContentType addObject:content_type];
                
            }
            
            //Load a JSON
            [prefs setInteger:x forKey:@"xval"];
            [prefs setInteger:y forKey:@"yval"];
            
            MMAppEngine *engine= [MMAppEngine sharedManager];
            engine.currentStates = [NSMutableArray arrayWithArray:arrayOfFrame];
            engine.content = [NSMutableArray arrayWithArray:arrayOfContent];
            engine.contentType = [NSMutableArray arrayWithArray:arrayOfContentType];
        }
    }
    
    if (alertView==yInput) {
        NSString *entered=[yInput textFieldAtIndex:0].text;
        if( [entered length] >= 1 )
        {
            int y= [entered intValue];
            
            [prefs setInteger:y forKey:@"yval"];

            editMode= [[EditMode alloc]initWithFrame:CGRectMake(0, 30, 320, 400)];
            editMode.layer.shadowOpacity=1.0;
            
            viewMode= [[ViewMode alloc]initWithFrame:CGRectMake(0, 30, 320, 400)];
            [self.view addSubview:viewMode];
            

            editMode.frame = CGRectMake(0, 30, 320, 400);
                                 
            editMode.layer.borderColor= [UIColor blackColor].CGColor;
            editMode.layer.borderWidth = 1;
            editMode.layer.shadowOpacity= 0.0;
            [self.view addSubview:editMode];
            
            [self.view bringSubviewToFront:blackCover];
            [self.view bringSubviewToFront:praxisLogo];
            
            [UIView animateWithDuration:1.0
                                  delay:0.0
                                options: UIViewAnimationCurveEaseOut
                             animations:^{
                                 self.praxisLogo.layer.opacity= 0.0;
                             }
                             completion:^(BOOL finished){
                                [self.blackCover removeFromSuperview];
                                [self.praxisLogo removeFromSuperview];
                                [self.activity removeFromSuperview];
                             }];
            

        }
        else
        {
            [yInput show];
        }
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction)undoButton:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"undo" object:nil];
}

-(IBAction)changeMode:(id)sender{
    [UIView setAnimationTransition:UIViewAnimationOptionTransitionCurlUp forView:undo cache:YES];
    
    //Change to View Mode.
    if (currentMode ==0) {
//        [editMode upDateEngine];
        [UIView animateWithDuration:0.25
                              delay:0.0
                            options: UIViewAnimationCurveEaseOut
                         animations:^{
                             
                             editMode.frame= CGRectMake(-320, 0, 320, 400);
                             editMode.layer.shadowOpacity=1.0;
                         }
                         completion:^(BOOL finished){
                             [viewMode stopWorking];
                             viewMode.frame = CGRectMake(0, 30, 320, 400);
                             
                             viewMode.layer.borderColor= [UIColor blackColor].CGColor;
                             viewMode.layer.borderWidth = 2;
                             
                             [viewMode redraw];
                             [viewMode setNeedsLayout];
                             
                         }];
        currentMode=1;
        
        [mode setText:@"View Mode"];
        self.undo.hidden =YES;
        
    }
    
    //Change to Edit Mode.
    else {
        
        MMAppEngine *engine= [MMAppEngine sharedManager];
        editMode.currentStateContents= [[NSMutableArray alloc]initWithArray:engine.content];
        editMode.currentStateContentType=[[NSMutableArray alloc]initWithArray:engine.contentType];
        
        [viewMode stopWorking];
        

        [UIView animateWithDuration:0.25
                              delay:0.0
                            options: UIViewAnimationCurveLinear
                         animations:^{
                             viewMode.frame= CGRectMake(-320, 0, 320, 400);
                         }
                         completion:^(BOOL finished){
                             editMode.frame = CGRectMake(0, 30, 320, 400);
                             editMode.layer.borderColor= [UIColor blackColor].CGColor;
                             editMode.layer.borderWidth = 1;
                             editMode.layer.shadowOpacity= 0.0;
                             
                         }];
        currentMode=0;
        [mode setText:@"Edit Mode"];
        self.undo.hidden =NO;
    }
}


- (IBAction)curlAction:(id)sender
{
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:1.0];
	
	[UIView setAnimationTransition:((currentMode==0) ?
									UIViewAnimationTransitionCurlUp : UIViewAnimationTransitionCurlDown)
						   forView:editMode cache:YES];
    //Change to View Mode
	if (currentMode ==0){
//        [editMode upDateEngine];
        
        [self.view bringSubviewToFront:editMode];
        editMode.hidden= YES;
        viewMode.hidden= NO;
        
        [viewMode redraw];
        [viewMode setNeedsLayout];
        [viewMode setNeedsDisplay];
        currentMode=1;
        
        [mode setText:@"View Mode"];
        self.undo.hidden =YES;
	}
    
    //Change to Edit Mode
	else
	{
        [self.view bringSubviewToFront:editMode];
        editMode.hidden= NO;
        currentMode=0;
        [mode setText:@"Edit Mode"];
        self.undo.hidden =NO;
	}
	
	[UIView commitAnimations];
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView{
    NSString *inputText = [[alertView textFieldAtIndex:0] text];
    if( [inputText length] >= 10 )
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

-(IBAction)showTables:(id)sender{
    MYRootViewController *controller = [[[MYRootViewController alloc] initWithNibName:@"MYRootViewController" bundle:nil] autorelease];
    controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentModalViewController:controller animated:YES];
}


-(IBAction)sharingIsCaring:(id)sender{
    ShareViewController *controller = [[[ShareViewController alloc] initWithNibName:@"ShareViewController" bundle:nil] autorelease];
    controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentModalViewController:controller animated:YES];
}
@end