//
//  ViewMode.h
//  FrameWork
//
//  Created by Mahbub Morhsed on 3/7/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMAppEngine.h"
#import "QuartzCore/QuartzCore.h"
#import "ViewContent.h"

@interface ViewMode : UIView{
    MMAppEngine *engine;
    
    NSMutableArray *currentState;
    NSMutableArray *currentStateContentType;
    NSMutableArray *currentStateContent;
}

-(void) redraw;
-(void) stopWorking;
-(void)refresh:(NSNotification *)notification;

@end
