//
//  SuggestionView.m
//  doc2doc-ui-experiments
//
//  Created by Mahbub Morhsed on 21/5/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "SuggestionView.h"


@implementation SuggestionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        
    }
    return self;
}


-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self removeFromSuperview];
}
@end
