//
//  ViewModeContent.h
//  FrameWork
//
//  Created by Mahbub Morshed on 7/8/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

/**
 * This is each view in ViewMode
 * It holds everything inside the view
 */


#import <UIKit/UIKit.h>
#import "QuartzCore/QuartzCore.h"
#import <MapKit/MapKit.h>
#import "MMAppEngine.h"


@interface ViewContent : UIView<UIWebViewDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>{
    
    NSMutableString *viewIndex;
    
    UIWebView *web;
    UIImageView *imageView;
    MKMapView *map;
    
    UIButton *contentChooser;
    UIButton *fullScreen;
    
    UIAlertView *contentAlert;
    
    UIActivityIndicatorView *spinner;
    NSString __strong *originalFrame;

    BOOL fullScreenView;
    
    NSString *contentType;
    NSString *content;
    
    MMAppEngine *engine;
}

@property(strong) UIButton *contentChooser;
@property(strong) UIButton *fullScreen;
@property(strong) NSString *originalFrame;
@property(strong) UIWebView *web;
@property(strong) NSMutableString *viewIndex;

-(IBAction)showUrlChooser:(id)sender;
-(IBAction)toggleFullScreen:(id)sender;
-(void) stopWorking;
-(IBAction)showActionSheet:(id)sender;
-(void) takePicture:(id) sender;
-(void) openGallery:(id) sender;
-(void) setContentToContentView:(NSString *)givenContent contentToAdd:(NSString *)type;
-(void) setIndex:(int) index;
-(int) getIndex;

@end