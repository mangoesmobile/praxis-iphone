//
//  ViewMode.m
//  FrameWork
//
//  Created by Mahbub Morhsed on 3/7/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#import "ViewMode.h"

@implementation ViewMode

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refresh:) name:@"refresh" object:nil];
        
        // Initialization code
        engine= [MMAppEngine sharedManager];
        
        currentState= [NSMutableArray arrayWithArray:engine.currentStates];
        currentStateContent = [NSMutableArray arrayWithArray:engine.content];
        currentStateContentType = [NSMutableArray arrayWithArray:engine.contentType];

        self.backgroundColor= [UIColor whiteColor];
        
        [self setAutoresizesSubviews:YES];
    }
    return self;
}

-(void)refresh:(NSNotification *)notification{
    [self redraw];
}

-(void) redraw{
    [[self subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    currentState= [NSMutableArray arrayWithArray:engine.currentStates];
    currentStateContent = [NSMutableArray arrayWithArray:engine.content];
    currentStateContentType = [NSMutableArray arrayWithArray:engine.contentType];

    int j;
    NSLog(@"%d", [currentState count]);
    for (j=0; j< [currentState count]; j++) {
        NSString *v= [currentState objectAtIndex:j];
        CGRect parentFrame= CGRectFromString(v);
        
        ViewContent *content= [[ViewContent alloc]initWithFrame:parentFrame];
        [content setIndex:j];
        
        NSLog(@"Index is %d", [content getIndex]);
        
        [self addSubview:content];
        
        
        if ([engine.content count]>0) {
            
            NSLog(@"number of content is greater than 0");
            NSString *contType= [engine.contentType objectAtIndex:j];
            NSString *cont= [engine.content objectAtIndex:j];
            
            
            NSLog(@"%@", contType);
            [content setContentToContentView:contType contentToAdd:cont];
        }
    }
}


-(void) stopWorking{
    NSArray *sub=[self subviews];
    for(int i=0; i<[sub count]; i++){
        if([[sub objectAtIndex:i] isKindOfClass: [ViewContent class]]){
            [((ViewContent *)[sub objectAtIndex:i]) stopWorking];
        }
    }
}

@end
