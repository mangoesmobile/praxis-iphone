//
//  TesmListViewController.h
//  Messenger
//
//  Created by Mahbub Morshed on 10/21/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import "MMAppEngine.h"
#import "VcardData.h"
#import "TeamCell.h"

@interface TeamListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>{
    
    UIImageView *sliderbg;
    UISlider *slider;
    UIImageView *changeToMapSlider;
    NSString *hidden;
    
    NSMutableArray *onlineusers;
    NSMutableArray *onlineuserID;
    NSMutableArray *offlineusers;
    NSMutableArray *offlineuserID;
    
    MMAppEngine *engine;
    
}

@property(strong) NSString *hidden;
@property(strong) IBOutlet UISlider *slider;
@property(strong) IBOutlet UIImageView *sliderbg;
@property(strong) IBOutlet UIImageView *changeToMapSlider;

@property(strong) NSMutableArray *onlineusers;
@property(strong) NSMutableArray *offlineusers;
@property(strong) NSMutableArray *onlineuserID;
@property(strong) NSMutableArray *offlineuserID;

@property(strong) IBOutlet UITableView *onlineTable;
@property(strong) IBOutlet UITableView *offlineTable;

@property(strong) IBOutlet UILabel *onlineLabel;
@property(strong) IBOutlet UILabel *offlineLabel;

-(IBAction)back:(id)sender;
-(void)populateOnlineandOfflineUserList;

-(void)createNewConversation:(NSNotification *)notification;

//Todo
-(IBAction)ToggleOnlineTable:(id)sender;
-(IBAction)ToggleOfflineTable:(id)sender;
@end
