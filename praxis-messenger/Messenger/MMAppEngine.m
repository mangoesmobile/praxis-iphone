//
//  Engine.m
//  FrameWork
//
//  Created by Mahbub Morhsed on 19/6/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//


#import "MMAppEngine.h"

static MMAppEngine *sharedMyManager = nil;

@implementation MMAppEngine
@synthesize userVcards, userIds, selectedUserIds, selectedUserNames, userAvailability, userGroupIds, userGroups, currentMessageAttachments;
@synthesize currentMessageUserID, userVcardDictionary;

#pragma mark Singleton Methods
+ (id)sharedManager {
    @synchronized(self) {
        if(sharedMyManager == nil)
            sharedMyManager = [[super allocWithZone:NULL] init];
    }
    return sharedMyManager;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [self sharedManager];
}
- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (id)init {
    if (self = [super init]) {
        userVcards=[[NSMutableArray alloc]init];
        userIds=[[NSMutableArray alloc]init];
        userAvailability=[[NSMutableArray alloc]init];
        
        selectedUserIds=[[NSMutableArray alloc]init];
        selectedUserNames=[[NSMutableArray alloc]init];
        
        userGroups= [[NSMutableArray alloc]init];
        userGroupIds= [[NSMutableArray alloc]init];
        
        currentMessageAttachments= [[NSMutableArray alloc]init];
        
        userVcardDictionary= [[NSMutableDictionary alloc]init];
        
        currentMessageUserID =[[NSMutableArray alloc]init];
    }
    return self;
}

-(void)vcardUpdated{
    NSLog(@"VCARD Updated");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"vcards_updated" object:nil];
}



@end
