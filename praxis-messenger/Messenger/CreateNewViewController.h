//
//  CreateNewViewController.h
//  Messenger
//
//  Created by Mahbub Morshed on 9/23/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuartzCore/QuartzCore.h"
#import "MMAppEngine.h"
#import "PreviewViewController.h"
#import "KGModal.h"

#import "WEPopoverController.h"
#import "UserGroupViewController.h"

@interface CreateNewViewController : UIViewController<UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate, UITextViewDelegate>{

    BOOL attachmentBarHidden;
    
    int numberOfattachment;
    
    UIImagePickerController *imagePicker;
    UIPopoverController* popover;
    
    NSString *need_repositioning;
    
    UserGroupViewController *create_new_msg_content;
    WEPopoverController *create_new_msg_popovercontroller;
}

@property(nonatomic, strong) IBOutlet UIView *textBottomBar;
@property(nonatomic, strong) IBOutlet UIView *attachmentBottomBar;
@property(nonatomic, strong) IBOutlet PreviewViewController *preview;

@property(nonatomic, strong) IBOutlet UIView *previewBoard;
@property(nonatomic, strong) IBOutlet UITextField *subjectTextField;
@property(nonatomic, strong) IBOutlet UITextView *messageTextView;

@property(nonatomic, strong) IBOutlet UILabel *attachmentIndicator;

@property(strong) IBOutlet UIPopoverController* popover;
@property(nonatomic, strong) IBOutlet UIImagePickerController *imagePicker;

@property(nonatomic, strong) IBOutlet UIImageView *attachedImage1;
@property(nonatomic, strong) IBOutlet UIImageView *attachedImage2;
@property(nonatomic, strong) IBOutlet UIImageView *attachedImage3;
@property(nonatomic, strong) IBOutlet UIImageView *attachedImage4;
@property(nonatomic, strong) IBOutlet UIImageView *attachedImage5;

@property(nonatomic, strong) IBOutlet UIButton *dummyButton1;
@property(nonatomic, strong) IBOutlet UIButton *dummyButton2;
@property(nonatomic, strong) IBOutlet UIButton *dummyButton3;
@property(nonatomic, strong) IBOutlet UIButton *dummyButton4;
@property(nonatomic, strong) IBOutlet UIButton *dummyButton5;


@property(nonatomic, strong) IBOutlet UIImageView *largeImage;

@property(nonatomic, strong) IBOutlet UILabel *recipient_list_label;

@property(nonatomic, strong) IBOutlet UILabel *subjects;

-(IBAction)toggleAttachmentBar:(id)sender;
-(void)createNewMessage;
-(IBAction)send:(id)sender;
-(IBAction)GoBack:(id)sender;
-(IBAction)showImagePicker:(id)sender;
-(void)resetAttachMentIndicatorValue;
-(void)showPreview;
-(IBAction)hidePreview:(id)sender;
-(IBAction)showModalPanel:(id)sender;

//Bad Codes- Must be reviewed
-(IBAction)previewImage1:(id)sender;
-(IBAction)previewImage2:(id)sender;
-(IBAction)previewImage3:(id)sender;
-(IBAction)previewImage4:(id)sender;
-(IBAction)previewImage5:(id)sender;

-(BOOL)isPad;

-(IBAction)create_new_msg_popover:(id)sender;

-(void)update_recipient_list;
-(void)update_recipient_list_notification:(NSNotification *)notification;

@end
