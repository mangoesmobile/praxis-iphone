//
//  MJDetailViewController.m
//  MJPopupViewControllerDemo
//
//  Created by Martin Juhasz on 24.06.12.
//  Copyright (c) 2012 martinjuhasz.de. All rights reserved.
//

#import "MJDetailViewController.h"

@implementation MJDetailViewController
@synthesize name,loc,teamList,settings,statusmsg,logout;

- (void)viewWillAppear:(BOOL)animated{

    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    
    [name setText:[prefs objectForKey:@"name"]];
    statusmsg.placeholder=[prefs objectForKey:@"status"];
    loc.placeholder=[prefs objectForKey:@"location"];
    
    [self.logout addTarget:self action:@selector(logout) forControlEvents:UIControlEventTouchUpInside];
    [self.settings addTarget:self action:@selector(viewSettingsPage) forControlEvents:UIControlEventTouchUpInside];
}


-(IBAction)logout:(id)sender{
    NSLog(@"logout button clicked");
}

-(IBAction)viewSettingsPage:(id)sender{
    NSLog(@"viewSettings button clicked");

}
@end
