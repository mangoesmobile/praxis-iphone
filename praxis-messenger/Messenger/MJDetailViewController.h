//
//  MJDetailViewController.h
//  MJPopupViewControllerDemo
//
//  Created by Martin Juhasz on 24.06.12.
//  Copyright (c) 2012 martinjuhasz.de. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface MJDetailViewController : UIViewController{


}
@property(strong, nonatomic) IBOutlet UILabel *name;
@property(strong, nonatomic) IBOutlet UITextField *statusmsg;
@property(strong, nonatomic) IBOutlet UITextField *loc;
@property(strong, nonatomic) IBOutlet UIButton *teamList;
@property(strong, nonatomic) IBOutlet UIButton *settings;
@property(strong, nonatomic) IBOutlet UIButton *logout;


-(IBAction)logout:(id)sender;
-(IBAction)viewSettingsPage:(id)sender;

@end
