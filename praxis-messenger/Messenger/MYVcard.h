//
//  MYVcard.h
//  Messenger
//
//  Created by Mahbub Morshed on 10/15/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MYVcard : NSManagedObject

@property (nonatomic, retain) NSString * first_name;
@property (nonatomic, retain) NSString * last_name;
@property (nonatomic, retain) NSString * custom_status;
@property (nonatomic, retain) NSString * availability_status;
@property (nonatomic, retain) NSString * base_location;
@property (nonatomic, retain) NSString * geo_longitude;
@property (nonatomic, retain) NSString * geo_latitude;
@property (nonatomic, retain) NSString * is_workgroup_admin;
@property (nonatomic, retain) NSString * org_approved;
@property (nonatomic, retain) NSString * organization;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * profile_pic_file_path;
@property (nonatomic, retain) NSString * specialization;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * user_id;
@property (nonatomic, retain) NSString * username;

@end
