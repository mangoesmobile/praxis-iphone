//
//  VcardViewControllerDelegate.h
//  Messenger
//
//  Created by Mahbub Morshed on 11/14/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

@class VcardViewController;
#import <Foundation/Foundation.h>

@protocol VcardViewControllerDelegate<NSObject>
@required
-(void)update:(VcardViewController *)vcard;
@end
