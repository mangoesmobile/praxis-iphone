//
//  VcardData.m
//  Messenger
//
//  Created by Mahbub Morshed on 11/13/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import "VcardData.h"

@implementation VcardData

@synthesize profile_pic_path;
@synthesize current_location;
@synthesize current_status;
@synthesize phone;

+ (id)profile_pic_path:(NSString *)profile_pic_path current_status:(NSString *)current_status current_location:(NSString *)current_location phone_no:(NSString *)phone_no
{
    return [[VcardData alloc] initWithProfilePicPath:profile_pic_path initWithCurrentStatus:current_status initWithCurrentLocation:current_location phone_no:phone_no];
}

- (id)initWithProfilePicPath:(NSString *)init_profile_pic_path initWithCurrentStatus:(NSString *)init_current_status initWithCurrentLocation:(NSString *)init_current_location phone_no:(NSString *)init_phone_no
{
    self = [super init];
    if (self)
    {
        profile_pic_path= init_profile_pic_path;
        current_status= init_current_status;
        current_location= init_current_location;
        phone= init_phone_no;
    }
    return self;
}

- (NSString *)description{
    NSString *description_string= [NSString stringWithFormat:@"Path [%@], Status [%@], Location[%@], Phone [%@]", profile_pic_path, current_status, current_location, phone];
    return description_string;
}


- (void)encodeWithCoder:(NSCoder *)encoder {
    //serialization
    
    [encoder encodeObject:self.profile_pic_path forKey:@"profile_pic_path"];
    [encoder encodeObject:self.current_location forKey:@"current_location"];
    [encoder encodeObject:self.current_status forKey:@"current_status"];
    [encoder encodeObject:self.phone forKey:@"phone_no"];
}
- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self) {
        //deserialization
        self.profile_pic_path = [decoder decodeObjectForKey:@"profile_pic_path"];
        self.current_location = [decoder decodeObjectForKey:@"current_location"];
        self.current_status = [decoder decodeObjectForKey:@"current_status"];
        self.phone = [decoder decodeObjectForKey:@"phone_no"];
    }
    return self;
}

@end
