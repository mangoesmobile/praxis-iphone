//
//  Files.m
//  Messenger
//
//  Created by Mahbub Morshed on 11/9/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import "Attached_File.h"

@implementation Attached_File
@synthesize file_path;
@synthesize file_type;
@synthesize thumb_path;

+ (id)fileWithPath:(NSString *)file_path fileWithType:(NSString *)file_type fileWithThumb:(NSString *)thumb_path
{
    return [[Attached_File alloc] initWithPath:file_path initWithType: file_type initWithThumb:thumb_path];
}

- (id)initWithPath:(NSString *)initPath initWithType:(NSString *)initType initWithThumb:(NSString *)initThumb
{
    self = [super init];
    if (self)
    {
        file_path =[NSString stringWithFormat:@"%@%@",kBASE_URL_FILE, initPath];
        file_type= initType;
        thumb_path= [NSString stringWithFormat:@"%@%@",kBASE_URL_FILE, initThumb];
    }
    return self;
}

- (NSString *)description{
    NSString *description_string=[NSString stringWithFormat:@"Attached file path is -[%@]\nfile type is-[%@] ",file_path, file_type];
    return description_string;
}


@end
