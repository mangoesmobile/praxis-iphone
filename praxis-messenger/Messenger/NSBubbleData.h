//
//  NSBubbleData.h
//
//  Created by Alex Barinov
//  StexGroup, LLC
//  http://www.stexgroup.com
//
//  Project home page: http://alexbarinov.github.com/UIBubbleTableView/
//
//  This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.
//  To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/
//

#import <Foundation/Foundation.h>
#import "Message_attachments.h"

typedef enum _NSBubbleType
{
    BubbleTypeMine = 0,
    BubbleTypeSomeoneElse = 1
} NSBubbleType;

typedef enum _NSBubbleAttachmentIndicator
{
    AttachmentIndicatorShow = 0,
    AttachmentIndicatorHide = 1
} NSBubbleAttachmentIndicator;

@interface NSBubbleData : NSObject

@property (readonly, nonatomic, strong) NSDate *date;
@property (readonly, nonatomic) NSBubbleType type;
@property (readonly, nonatomic) NSBubbleAttachmentIndicator indicator;
@property (readonly, nonatomic, strong) NSString *sendername;
@property (readonly, nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *currentIndex;
@property (nonatomic, strong) NSMutableArray *attached_files;

- (id)initWithText:(NSString *)text andDate:(NSDate *)date andType:(NSBubbleType)type andIdicator:(NSBubbleAttachmentIndicator)indicator andAttachmentArray:(NSArray *)attachments andIndex:(NSString *)currentIndex andSenderName:(NSString *)currentSenderName;
+ (id)dataWithText:(NSString *)text andDate:(NSDate *)date andType:(NSBubbleType)type andIdicator:(NSBubbleAttachmentIndicator)indicator andAttachmentArray:(NSArray *)attachments andIndex:(NSString *)currentIndex andSenderName:(NSString *)currentSenderName;

@end
