//
//  UserGroupViewController.h
//  Messenger
//
//  Created by Mahbub Morshed on 9/23/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMAppEngine.h"
#import "UserTableViewCell.h"
#import "MMAppEngine.h"
#import "SVSegmentedControl.h"

@interface UserGroupViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>{
    NSMutableArray *userListArray;
    MMAppEngine *engine;
    
    UIActivityIndicatorView *activity;
    UILabel *loading;
    
    NSString *userListShown;
    
    NSMutableArray *selectedGroups;
    
    SVSegmentedControl *navSC;
}
@property(strong) IBOutlet UITableView *userListTableView;
@property(strong) IBOutlet UITableView *groupList;
@property(strong) IBOutlet UIButton *doneButton;
@property(strong) IBOutlet UIActivityIndicatorView *activity;
@property(strong) IBOutlet UILabel *loading;
@property(strong) NSString *userListShown;
@property(strong) NSMutableArray *selectedGroups;

@property(strong) SVSegmentedControl *navSC;

-(void)updateTableView:(NSNotification *)notification;
-(void)showActivity:(NSNotification *)notification;
-(IBAction)doneAction:(id)sender;
-(IBAction)GoBack:(id)sender;

- (void)segmentedControlChangedValue:(SVSegmentedControl*)segmentedControl;

-(void)showGroupList;
-(void)fetch_members:(NSString *)group_id;

- (void)registerNotifications;

-(void)selectionDidChange:(NSNotification *)notification;
@end
