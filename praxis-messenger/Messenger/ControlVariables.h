/*
    Common variable values used whole app wide
 */

#ifndef Messenger_ControlVariables_h
#define Messenger_ControlVariables_h

//Restkit client base url
#define kBASE_URL @"http://50.18.120.168/praxis-master/index.php/"

//Used to find the acutal file path
#define kBASE_URL_FILE @"http://50.18.120.168/praxis-master/"

//AES key for AES256 Encoding
#define kAesKey @"jc0gvDzyg3lpwmQyTbHhBSFlD4wkiLy8"

/********PULL TO REFRESH HEADER VALUES******/
//Pull to refresh refresh header height
#define REFRESH_HEADER_HEIGHT 52.0f


/******** NSUserDefaults values *******/
//FIND DEVICE TYPE
#define kDEVICE_TYPE @"device"


//Save and retrieve encrypted value of password
#define kENC_PASS @"enc_pass"
#define kLOGGED_IN @"logged_in"

//USER VARIABLES
#define kMY_FIRST_NAME @"first_name"
#define kMY_NAME @"name"
#define kMY_CUSTOM_STATUS @"status"
#define kMY_BASE_LOCATION @"location"
#define kMY_PROFILE_PIC @"profile_pic"
#define kMY_USER_ID @"USERID"
#define kCOOKIE @"COOKIE"

//Longitude n Latitude
#define kMY_LONGITUDE @"longitude"
#define kMY_LATITUDE @"latitude"

//Checking if there is any attachment
#define kATTACHMENT_AVAILABLE @"hasAttachments"

//Some nasty NSUserDefaults used at CreateNewMessageViewController
#define kSUBJECT_OF_MESSAGE @"SUBJECT"

//Names of people who has seen the method
#define kSEEN @"seen"
/******** NSUserDefaults values *******/

/******** NOTIFICATION values *******/

/******** NOTIFICATION values *******/

#endif
