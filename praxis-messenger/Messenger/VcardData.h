//
//  VcardData.h
//  Messenger
//
//  Created by Mahbub Morshed on 11/13/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VcardData : NSObject<NSCoding>

@property(nonatomic,strong) NSString *profile_pic_path;
@property(nonatomic,strong) NSString *current_status;
@property(nonatomic,strong) NSString *current_location;
@property(nonatomic,strong) NSString *phone;

+ (id)profile_pic_path:(NSString *)profile_pic_path current_status:(NSString *)current_status current_location:(NSString *)current_location phone_no:(NSString *)phone_no;

- (id)initWithProfilePicPath:(NSString *)init_profile_pic_path initWithCurrentStatus:(NSString *)initType initWithCurrentLocation:(NSString *)init_current_location phone_no:(NSString *)phone_no;

- (void)encodeWithCoder:(NSCoder *)encoder;

- (id)initWithCoder:(NSCoder *)decoder;
@end
