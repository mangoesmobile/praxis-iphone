//
//  UIBubbleTableView.h
//
//  Created by Alex Barinov
//  StexGroup, LLC
//  http://www.stexgroup.com
//
//  Project home page: http://alexbarinov.github.com/UIBubbleTableView/
//
//  This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.
//  To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/
//

#import <UIKit/UIKit.h>

#import "UIBubbleTableViewDataSource.h"
#import "UIBubbleTableViewCell.h"
#import "WEPopoverController.h"
#import "VcardViewController.h"
#import "VcardData.h"

typedef enum _NSBubbleTypingType
{
    NSBubbleTypingTypeNobody = 0,
    NSBubbleTypingTypeMe = 1,
    NSBubbleTypingTypeSomebody = 2
} NSBubbleTypingType;

@interface UIBubbleTableView : UITableView <UITableViewDelegate, UITableViewDataSource, WEPopoverControllerDelegate, UIPopoverControllerDelegate, VcardViewControllerDelegate>
{
    IBOutlet UIBubbleTableViewCell *bubbleCell;
    
    //WEPopOverController uses it.
    Class popoverClass;
    
    NSString *currentTouchPosition;
}

@property (nonatomic, assign) id<UIBubbleTableViewDataSource> bubbleDataSource;

@property (nonatomic) NSTimeInterval snapInterval;

@property (nonatomic) NSBubbleTypingType typingBubble;

@property (nonatomic, retain)VcardData *vcardcurrent ;

@property (nonatomic, retain) WEPopoverController *popoverController;

@property (nonatomic, retain) NSString *currentTouchPosition;

@property(nonatomic, retain) VcardViewController *show_vcard;

//- (IBAction)showPopover:(id)sender;

@end
