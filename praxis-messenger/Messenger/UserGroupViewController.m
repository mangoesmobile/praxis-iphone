//
//  UserGroupViewController.m
//  Messenger
//
//  Created by Mahbub Morshed on 9/23/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import "UserGroupViewController.h"
#import "VcardData.h"

@interface UserGroupViewController ()

@end

@implementation UserGroupViewController
@synthesize userListTableView, groupList, doneButton, activity, userListShown,selectedGroups, navSC;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        userListArray= [[NSMutableArray alloc]init];
        engine= [MMAppEngine sharedManager];
        
        userListArray =[[NSMutableArray alloc]initWithArray: engine.userVcards];
        userListShown= @"YES";
        
        selectedGroups =[[NSMutableArray alloc]init];
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    engine= [MMAppEngine sharedManager];
    
    selectedGroups =[[NSMutableArray alloc]init];
    
    navSC = [[SVSegmentedControl alloc] initWithSectionTitles:[NSArray arrayWithObjects:@"User List", @"Group List", nil]];
    
    navSC.changeHandler = ^(NSUInteger newIndex) {
        NSLog(@"segmentedControl did select index %i (via block handler)", newIndex);
        if (newIndex ==0) {
            [self updateTableView:nil];
        }
        else{
            [self showGroupList];
        }
    };
    
	[self.view addSubview:navSC];
	
	navSC.center = CGPointMake(self.view.frame.size.width/2-30,30);
    
    [self registerNotifications];
    
    self.userListTableView.allowsMultipleSelection= YES;
    
    
}

-(void)selectionDidChange:(NSNotification *)notification{
    
    if ([userListShown isEqualToString:@"YES"]) {
        NSArray *indexes= [(UITableView *)notification.object indexPathsForSelectedRows];
        
        //This is buggy. Have to find some other way.
        //NSLog(@"%@", indexes);
        if ([indexes count ]>0) {
            [engine.selectedUserIds removeAllObjects];
            [engine.selectedUserNames removeAllObjects];
            
            for (int i=0; i< [indexes count]; i++) {
                NSIndexPath *indexPath= [indexes objectAtIndex:i];
                
                [engine.selectedUserIds addObject:[engine.userIds objectAtIndex:indexPath.row]];
                [engine.selectedUserNames addObject:[engine.userVcards objectAtIndex:indexPath.row]];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"updaterecipientList" object:nil];
        }
    }
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [userListArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"userTableViewCell";
    
    UserTableViewCell *cell = [tableView
                               dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UserTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    NSString *user_name= [userListArray objectAtIndex:indexPath.row];
    cell.userName.text = user_name;
    
    if ([engine.selectedUserNames containsObject:user_name]) {
        [tableView
         selectRowAtIndexPath:indexPath
         animated:TRUE
         scrollPosition:UITableViewScrollPositionNone
         ];
    }
    else if ([self.selectedGroups containsObject:user_name]){
        [tableView
         selectRowAtIndexPath:indexPath
         animated:TRUE
         scrollPosition:UITableViewScrollPositionNone
         ];
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([userListShown isEqualToString:@"YES"]) {
        if ([engine.selectedUserIds containsObject:[engine.userIds objectAtIndex:indexPath.row]]
            ) {
            [engine.selectedUserIds removeObject:[engine.userIds objectAtIndex:indexPath.row]];
            [engine.selectedUserNames removeObject:[engine.userVcards objectAtIndex:indexPath.row]];
        }
        else{
            [engine.selectedUserIds addObject:[engine.userIds objectAtIndex:indexPath.row]];
            [engine.selectedUserNames addObject:[engine.userVcards objectAtIndex:indexPath.row]];
        }
        [self updateTableView:nil];
    }
    else{
        [self fetch_members:[engine.userGroupIds objectAtIndex:indexPath.row]];
        
        if ([selectedGroups containsObject:[engine.userGroups objectAtIndex:indexPath.row]]) {
            [selectedGroups removeObject:[engine.userGroups objectAtIndex:indexPath.row]];
        }
        else{
            [selectedGroups addObject:[engine.userGroups objectAtIndex:indexPath.row]];
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updaterecipientList" object:nil];
}


-(void)showActivity:(NSNotification *)notification{
    self.userListTableView.hidden= YES;
    self.loading.hidden= NO;
    self.navSC.hidden = YES;
    [self.activity performSelectorInBackground:@selector(startAnimating) withObject:nil];
    
    [SVProgressHUD show];
}

-(void)updateTableView:(NSNotification *)notification{
    self.userListTableView.hidden= NO;
    self.navSC.hidden= NO;
    
    NSLog(@"notification received");
    [self.activity stopAnimating];
    [SVProgressHUD dismiss];
    
    self.loading.hidden= YES;
    
    userListArray =[[NSMutableArray alloc]initWithArray: engine.userVcards];
    [self.userListTableView reloadData];
    self.userListShown= @"YES";
}

-(void)showGroupList{
    self.userListShown= @"NO";
    self.userListTableView.hidden= NO;
    
    [self.activity stopAnimating];
    [SVProgressHUD dismiss];
    
    self.loading.hidden= YES;
    
    userListArray =[[NSMutableArray alloc]initWithArray: engine.userGroups];
    [self.userListTableView reloadData];
}


-(IBAction)doneAction:(id)sender{
    [engine.selectedUserIds removeAllObjects];
    [engine.selectedUserNames removeAllObjects];
    
    NSArray *allIndexpath= [userListTableView indexPathsForSelectedRows];
    
    for (int i= 0; i< [allIndexpath count]; i++) {
        NSIndexPath *indexPath= [allIndexpath objectAtIndex:i];
        [engine.selectedUserIds addObject:[engine.userIds objectAtIndex:indexPath.row]];
        [engine.selectedUserNames addObject:[engine.userVcards objectAtIndex:indexPath.row]];
        
    }
    
    NSString *selected;
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    
    NSLog(@"done button clicked");
    
    if ([userListShown isEqualToString:@"YES"]) {
        [selectedGroups removeAllObjects];
    }
    
    if ([selectedGroups count]> 0) {
        for (int i=0; i< [selectedGroups count]; i++) {
            if (i>0) {
                selected = [NSString stringWithFormat:@"%@,[%@]",selected,[selectedGroups objectAtIndex:i]];
            }
            else
                selected= [NSString stringWithFormat:@"[%@]",[selectedGroups objectAtIndex:0]];
            
        }
        selected= [NSString stringWithFormat:@"%@ -",selected];
        
        NSLog(@"selected subject is [%@]", selected);
        [prefs setObject:selected forKey:kSUBJECT_OF_MESSAGE];
    }
    else{
        [prefs setObject:@"" forKey:kSUBJECT_OF_MESSAGE];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"createNewConversation" object:nil];
    NSLog(@"notification posted");
}

-(IBAction)GoBack:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)segmentedControlChangedValue:(SVSegmentedControl*)segmentedControl {
	NSLog(@"segmentedControl %i did select index %i (via UIControl method)", segmentedControl.tag, segmentedControl.selectedIndex);
}

-(void)fetch_members:(NSString *)group_id{
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    group_id, @"group_id",
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/usergroup/fetch_members" usingBlock:^(RKRequest *request) {
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            
            //parsing the JSON response
            NSString *re= [response bodyAsString];
            NSDictionary *__response= [par objectFromString:re error:nil];
            
            NSLog(@"Member list is- [%@]", __response);
            NSDictionary *response_dictionary=[__response objectForKey:@"RESPONSE"];
            
            NSArray *member=[response_dictionary objectForKey:@"members"];
            
            for (int i=0; i<[member count]; i++) {
                NSDictionary *member_dictionary=[member objectAtIndex:i];
                
                NSString *current_user_id= [member_dictionary objectForKey:@"user_id"];
                
                NSString *firstName= [member_dictionary objectForKey:@"first_name"];
                NSString *lastName= [member_dictionary objectForKey:@"last_name"];
                NSString *fullName= [NSString stringWithFormat:@"%@ %@",firstName ,lastName];
                
                if ([engine.selectedUserIds containsObject:current_user_id]) {
                    //Doing nothing
                }
                else{
                    [engine.selectedUserIds addObject:current_user_id];
                    [engine.selectedUserNames addObject:fullName];
                }
            }
            [SVProgressHUD dismiss];
        };//End of onDidLoadResponse
        
        request.onDidFailLoadWithError= ^(NSError *error) {
            NSLog(@"error2:%@",error);
        };//End of onDidFailLoadWithError
    }];//End of block
}

- (void)registerNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showActivity:) name:@"vcards_loading" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTableView:) name:@"vcards_updated" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(selectionDidChange:) name:@"UITableViewSelectionDidChangeNotification" object:nil];
}

@end
