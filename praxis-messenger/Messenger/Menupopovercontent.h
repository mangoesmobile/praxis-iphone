//
//  Menupopovercontent.h
//  Messenger
//
//  Created by Mahbub Morshed on 10/16/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Menupopovercontent : UIViewController<UITextFieldDelegate>{

}

@property(strong, nonatomic) IBOutlet UILabel *name;
@property(strong, nonatomic) IBOutlet UITextField *statusmsg;
@property(strong, nonatomic) IBOutlet UITextField *loc;
@property(strong, nonatomic) IBOutlet UIButton *teamList;
@property(strong, nonatomic) IBOutlet UIButton *settings;
@property(strong, nonatomic) IBOutlet UIButton *logout;


-(IBAction)logout:(id)sender;
-(IBAction)viewSettingsPage:(id)sender;
-(IBAction)viewTeamList:(id)sender;
-(IBAction)updateStatus:(id)sender;

@end
