//
//  Message_attachments.h
//  Messenger
//
//  Created by Mahbub Morshed on 11/5/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message_attachments : NSObject

@property(strong, retain) NSString *file_path;
@property(strong, retain) NSString *file_type;
@property(strong, retain) NSString *thumb_path;
@end
