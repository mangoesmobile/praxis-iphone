//
//  Menupopovercontent.m
//  Messenger
//
//  Created by Mahbub Morshed on 10/16/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import "Menupopovercontent.h"

@interface Menupopovercontent ()

@end

@implementation Menupopovercontent
@synthesize name, statusmsg, loc, settings, logout, teamList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    
    [name setText:[prefs objectForKey:kMY_NAME]];
    statusmsg.placeholder=[prefs objectForKey:kMY_CUSTOM_STATUS];
    statusmsg.delegate= self;
    loc.placeholder=[prefs objectForKey:kMY_BASE_LOCATION];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(IBAction)logout:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
}

-(IBAction)viewSettingsPage:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"showsettings" object:nil];
}

-(IBAction)viewTeamList:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"showteam" object:nil];
}

-(IBAction)updateStatus:(id)sender{
    NSString *custom_status= statusmsg.text;
    
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    custom_status, @"status_msg",nil];
    
    //request block
    [[RKClient sharedClient] post:@"/user/set_custom_status" usingBlock:^(RKRequest *request) {
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            
            //parsing the JSON response
            NSString *re= [response bodyAsString];
            NSDictionary *__response= [par objectFromString:re error:nil];
            
            [SVProgressHUD showSuccessWithStatus:@"new status message set"];
        };//End of onDidLoadResponse
        
        request.onDidFailLoadWithError= ^(NSError *error) {
            NSLog(@"error2:%@",error);
        };//End of onDidFailLoadWithError
    }];//End of block

    //Todo-remove this
    /*After updating the status update getmyvcard again.*/
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == statusmsg) {
        [textField resignFirstResponder];
        [self updateStatus:nil];
        
        [SVProgressHUD showWithStatus:@"Updating your status"];
    }
    
    return YES;
}
@end
