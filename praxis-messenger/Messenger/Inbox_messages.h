//
//  Inbox_messages.h
//  Messenger
//
//  Created by Mahbub Morshed on 10/15/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Inbox_messages : NSManagedObject

@property (nonatomic, retain) NSString * author;
@property (nonatomic, retain) NSString * messageId;
@property (nonatomic, retain) NSString * subject;
@property (nonatomic, retain) NSString * followers;

@end
