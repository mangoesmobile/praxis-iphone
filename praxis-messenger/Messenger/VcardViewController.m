//
//  VcardViewController.m
//  Messenger
//
//  Created by Mahbub Morshed on 11/13/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//


#import "VcardViewController.h"

@interface VcardViewController ()

@end

@implementation VcardViewController
@synthesize user_pic;
@synthesize status_label;
@synthesize location_label;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(setupvcard:) name:@"LoadCurrentVcard" object:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(void)setupvcard:(NSNotification *)notification{
    VcardData *vcardCurrent = (VcardData *)notification.object;
    
    self.status_label.text= vcardCurrent.current_status;
    self.location_label.text= vcardCurrent.current_location;
    

     NSString *profile_pic_string= [NSString stringWithFormat:@"%@%@",kBASE_URL_FILE,vcardCurrent.profile_pic_path];
     
     [self.user_pic setImageWithURL:[NSURL URLWithString:profile_pic_string]
     placeholderImage:[UIImage imageNamed:@"profile_picture_thumbnail"]];
}
@end
