//
//  FlipsideViewController.m
//  Messenger
//
//  Created by Mahbub Morshed on 9/3/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import "MessengerViewController.h"
#import "UIBubbleTableViewDataSource.h"
#import "NSBubbleData.h"

///////////////////////////////////////////////////////////////////////
@interface Message : NSManagedObject {
    
}

@property (nonatomic, retain) NSNumber *messageID;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *body;

@end

@implementation Message

@dynamic messageID;
@dynamic title;
@dynamic body;

@end
///////////////////////////////////////////////////////////////////////


@interface MessengerViewController (){
    //PULL TO REFRESH
    UIView *refreshHeaderView;
    
    UILabel *refreshLabel;
    
    UIImageView *refreshArrow;
    
    UIActivityIndicatorView *refreshSpinner;
    
    BOOL isDragging;
    
    BOOL isLoading;
    
    NSString *textPull;
    
    NSString *textRelease;
    
    NSString *textLoading;
    //PULL TO REFRESH
    
}

// PULL TO REFRESH
@property (nonatomic, retain) UIView *refreshHeaderView;

@property (nonatomic, retain) UILabel *refreshLabel;

@property (nonatomic, retain) UIImageView *refreshArrow;

@property (nonatomic, retain) UIActivityIndicatorView *refreshSpinner;

@property (nonatomic, copy) NSString *textPull;

@property (nonatomic, copy) NSString *textRelease;

@property (nonatomic, copy) NSString *textLoading;

//@property (nonatomic, retain) UIView *refreshFooterView;

@property (nonatomic, retain) IBOutlet UILabel *perticipants;
// PULL TO REFRESH

// PULL TO REFRESH
- (void)setupStrings;

- (void)addPullToRefreshHeader;

- (void)startLoading;

- (void)stopLoading;

- (void)pull_refresh;
// PULL TO REFRESH

-(void)resetVariables;

@end

@implementation MessengerViewController
@synthesize inboxTableView, tableHidingButton, bottomBar, textBottomBar, text, sendButton, trashButton, messageAuthorArray, messageUnreadArray, bubbleTable, messageSubjectArray, inboxLoadingActivity, loadingInbox, messageIdArray, popOverPlacingView, unreadCount, page_no,attachment_button, perticipants,attachmentBottomBar, attachmentIndicatorBar, cornerMButton;

@synthesize textPull, textRelease, textLoading, refreshHeaderView, refreshLabel, refreshArrow, refreshSpinner;

@synthesize attachedImage1,attachedImage2, attachedImage3, attachedImage4, attachedImage5;
@synthesize dummyButton1, dummyButton2, dummyButton3, dummyButton4, dummyButton5;

@synthesize popover, imagePicker;

@synthesize seenLabel;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //Setting Up RKObjectManager
    RKObjectManager *manager = [RKObjectManager managerWithBaseURLString:@"http://restkit.org"];
    manager.objectStore = [RKManagedObjectStore objectStoreWithStoreFilename:@"RKCoreDataExample.sqlite"];
    [RKObjectManager setSharedManager:manager];
    //Setting up RKObjectManager
    
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"MainStoryboard"
                                                  bundle:nil];
    
    create_new_msg_content = [sb instantiateViewControllerWithIdentifier:@"UserGroupViewController"];
    create_new_msg_popovercontroller= [[WEPopoverController alloc] initWithContentViewController:create_new_msg_content];
    create_new_msg_popovercontroller.popoverContentSize= CGSizeMake(260, 370);
    
    
    menu_content = [sb instantiateViewControllerWithIdentifier:@"Menucontent"];
    menu_popovercontroller= [[WEPopoverController alloc] initWithContentViewController:menu_content];
    menu_popovercontroller.popoverContentSize= CGSizeMake(160, 220);
    
    hidden= NO;
    
    self.sendButton.hidden=YES;
    
    inboxTableView.layer.borderWidth=2.0;
    bottomBar.layer.borderWidth=2.0;
    textBottomBar.layer.borderWidth= 2.0;
    inboxTableView.delegate= self;
    inboxTableView.dataSource= self;
    inboxTableView.rowHeight= 65;
    [self addPullToRefreshHeader];
    
    inboxtableRect= NSStringFromCGRect(inboxTableView.frame);
    bottomBarRect= NSStringFromCGRect(bottomBar.frame);
    textBottomBarRect= NSStringFromCGRect(textBottomBar.frame);
    sendButtonRect= NSStringFromCGRect(sendButton.frame);
    textRect= NSStringFromCGRect(text.frame);
    tableHiddingButtonRect= NSStringFromCGRect(tableHidingButton.frame);
    
    self.textBottomBar.hidden = YES;
    
    engine = [MMAppEngine sharedManager];
    
    self.messageAuthorArray= [[NSMutableArray alloc] initWithCapacity:1];
    self.messageSubjectArray= [[NSMutableArray alloc] initWithCapacity:1];
    self.messageIdArray= [[NSMutableArray alloc] initWithCapacity:1];
    self.messageUnreadArray=[[NSMutableArray alloc]initWithCapacity:1];
    
    [self performSelectorInBackground:@selector(fetchInbox:) withObject:nil];
    
    [self.view bringSubviewToFront:inboxTableView];
    [self.view bringSubviewToFront:textBottomBar];
    
    bubbleData = [[NSMutableArray alloc] initWithObjects:
                  nil];
    
    bubbleTable.bubbleDataSource = self;
    
    bubbleTable.snapInterval = 130;
    bubbleTable.typingBubble = NSBubbleDataTypeNormalBubble;
    [bubbleTable reloadData];
    [bubbleTable scrollToRowAtIndexPath:nil atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    self.text.clearsOnBeginEditing = YES;
    
    [self registerNotifications];
    
    [self resetVariables];
    
    
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideTableView:)];
    
    [self.bubbleTable addGestureRecognizer:tap];
    
    imagePicker = [[UIImagePickerController alloc] init];
    
    // Set source to the Photo library
    imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    
    //Hide the attachment bottom bar
    self.attachmentBottomBar.layer.borderWidth= 2.0;
    self.attachmentBottomBar.hidden =YES;
    self.attachmentIndicatorBar.hidden= YES;
    
    self.previewBoard.hidden= YES;
    
    [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(get_new_count) userInfo:nil repeats:YES];
    
    [self create_new_msg_popover:nil];
    [create_new_msg_popovercontroller dismissPopoverAnimated:NO];
    
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    [prefs setBool:NO forKey:kATTACHMENT_AVAILABLE];
}

-(void)resetVariables{
    
    //Todo-remove this
    /*Use the Notification API instead*/
    unreadCount= @"0";
    
    perticipants.text= @"";
    seenLabel.text=@"";
    page_no= @"0";
    firstLoad= true;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Actions

- (IBAction)done:(id)sender
{
    [self.delegate flipsideViewControllerDidFinish:self];
}

- (IBAction)toogle:(id)sender{
    if (hidden == YES) {
        [self show:sender];
        self.attachmentIndicatorBar.hidden= YES;
    }
    else{
        [self hideTableView:sender];
    }
}

-(IBAction)hideTableView:(id)sender{
    if ([bubbleData count]>0) {
        [self.text resignFirstResponder];
        
        [UIView animateWithDuration:0.25
                              delay:0.0
                            options: UIViewAnimationCurveEaseOut
                         animations:^{
                             tableHidingButton.frame= CGRectMake(0, 36, 30, 30);
                             CGRect newR= CGRectFromString(inboxtableRect);
                             inboxTableView.frame= CGRectMake(-newR.size.width, 0, newR.size.width, newR.size.height);
                             bottomBar.frame =CGRectMake(0, self.view.frame.size.height + 50, 165, 41);
                             textBottomBar.frame= CGRectMake(0, self.view.frame.size.height-46, self.view.frame.size.width, 46);
                             text.frame= CGRectMake(53, 7, self.view.frame.size.width-63-45, 31);
                             sendButton.frame= CGRectMake(self.view.frame.size.width-52, 4, 44, 37);
                         }
                         completion:^(BOOL finished){
                             //Done
                         }];
        self.textBottomBar.hidden = NO;
        
        [self.tableHidingButton setBackgroundImage:[UIImage imageNamed:@"showButton.png"] forState: UIControlStateNormal];
        hidden = YES;
        
        [self.bubbleTable removeGestureRecognizer:tap];
    }
}

-(IBAction)show:(id)sender{
    [self.text resignFirstResponder];
    
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^{
                         tableHidingButton.frame= CGRectFromString(tableHiddingButtonRect);
                         inboxTableView.frame= CGRectFromString(inboxtableRect);
                         bottomBar.frame=CGRectFromString(bottomBarRect);
                         textBottomBar.frame= CGRectFromString(textBottomBarRect);
                         text.frame= CGRectFromString(textRect);
                         sendButton.frame= CGRectFromString(sendButtonRect);
                     }
                     completion:^(BOOL finished){
                         //Done
                     }];
    [self.tableHidingButton setBackgroundImage:nil forState: UIControlStateNormal];
    self.textBottomBar.hidden = YES;
    hidden = NO;
    
    [self.bubbleTable addGestureRecognizer:tap];
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    NSString *device= [prefs objectForKey:kDEVICE_TYPE];
    
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^{
                         if ([device isEqualToString:@"iPad"]) {
                             self.textBottomBar.transform= CGAffineTransformTranslate(self.textBottomBar.transform, 0, -260);
                             self.bubbleTable.transform= CGAffineTransformMakeScale(1, 0.5);
                         }
                         else
                             self.textBottomBar.transform= CGAffineTransformTranslate(self.textBottomBar.transform, 0, -215);
                     }
                     completion:^(BOOL finished){
                         //Done
                     }];
    self.sendButton.hidden= NO;
    return YES;
}

-(IBAction)send:(id)sender{
    
    //Todo: Find the current user name and add it as sendername
    [bubbleData addObject:[NSBubbleData dataWithText:self.text.text andDate:nil andType:BubbleTypeMine andIdicator:AttachmentIndicatorHide andAttachmentArray:nil andIndex:nil andSenderName:nil]];
    
    [self post_reply:currentMessageId contents:self.text.text];
    
    [bubbleTable reloadData];
    [bubbleTable scrollToRowAtIndexPath:nil atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    NSString *device= [prefs objectForKey:kDEVICE_TYPE];
    
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^{
                         [self.text resignFirstResponder];
                         if ([device isEqualToString:@"iPad"]) {
                             self.textBottomBar.transform= CGAffineTransformTranslate(self.textBottomBar.transform, 0, 260);
                         }
                         else
                             self.textBottomBar.transform= CGAffineTransformTranslate(self.textBottomBar.transform, 0, 215);
                     }
                     completion:^(BOOL finished){
                         //Done
                     }];
    self.sendButton.hidden= YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    NSString *my_name=[prefs objectForKey:kMY_USER_ID];
    NSString *device= [prefs objectForKey:kDEVICE_TYPE];
    
    [bubbleData addObject:[NSBubbleData dataWithText:textField.text andDate:nil andType:BubbleTypeSomeoneElse andIdicator:AttachmentIndicatorHide andAttachmentArray:nil andIndex:nil andSenderName:my_name]];
    
    [bubbleTable reloadData];
    [bubbleTable scrollToRowAtIndexPath:nil atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^{
                         
                         [self.text resignFirstResponder];
                         
                         if ([device isEqualToString:@"iPad"]) {
                             self.textBottomBar.transform= CGAffineTransformTranslate(self.textBottomBar.transform, 0, 300);
                         }
                         else
                             self.textBottomBar.transform= CGAffineTransformTranslate(self.textBottomBar.transform, 0, 215);
                     }
                     completion:^(BOOL finished){
                         //Done
                     }];
    self.sendButton.hidden= YES;
    return YES;
}

-(IBAction)create_new_msg_popover:(id)sender{
    UIView *v= (UIView *)sender;
    
    [self getAllVcards];
    [self fetch_membership_of_user];
    
    [create_new_msg_popovercontroller presentPopoverFromRect:v.frame inView:self.bottomBar permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    create_new_msg_popovercontroller.view.transform= CGAffineTransformTranslate(create_new_msg_popovercontroller.view.transform, 20, 0);
}

-(IBAction)show_menu_popover:(id)sender{
    [self hideTableView:nil];
    
    UIView *v= (UIView *)sender;
    
    [menu_popovercontroller presentPopoverFromRect:v.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    menu_popovercontroller.view.transform= CGAffineTransformTranslate(menu_popovercontroller.view.transform, 0, -20);
}

-(void)getAllVcards{
    NSMutableDictionary *userDictionary= [[NSMutableDictionary alloc]init];
    
    //request block
    [[RKClient sharedClient] post:@"/user/all_vcards" usingBlock:^(RKRequest *request) {
        
        request.onDidLoadResponse = ^(RKResponse *response) {
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            //parsing the JSON response
            NSString *re= [response bodyAsString];
            
            NSDictionary *dict= [par objectFromString:re error:nil];
            
            NSDictionary *all_vcards=[dict objectForKey:@"RESPONSE"];
            
            NSString *total_users=[all_vcards objectForKey:@"total_users"];
            
            NSMutableArray *vcardArray= [all_vcards objectForKey:@"vcards"];
            
            int i;
            
            if ([engine.userVcards count]== [total_users integerValue]) {
                //Doing nothing
            }
            else{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"vcards_loading" object:nil];
                
                [engine.userVcards removeAllObjects];
                [engine.userIds removeAllObjects];
                
                for (i=0; i< [total_users intValue];i++ ) {
                    NSDictionary *currentVCard= [vcardArray objectAtIndex: i];
                    
                    NSLog(@"CurrentVcard is  [%@]", currentVCard);
                    
                    NSString *firstName= [currentVCard objectForKey:@"first_name"];
                    NSString *lastName= [currentVCard objectForKey:@"last_name"];
                    NSString *availability_status=[currentVCard objectForKey:@"availability_status"];
                    
                    NSString *fullName= [NSString stringWithFormat:@"%@ %@",firstName ,lastName];
                    NSString *user_id=[currentVCard objectForKey:@"user_id"];
                    
                    NSString *custom_status=[currentVCard objectForKey:@"custom_status"];
                    NSString *base_location=[currentVCard objectForKey:@"base_location"];
                    NSString *profile_pic_path=[currentVCard objectForKey:@"profile_pic_file_path"];
                    NSString *phone=[currentVCard objectForKey:@"phone"];
                    
                    //Storing VCard Data
                    VcardData *vcardofuser= [[VcardData alloc]initWithProfilePicPath:profile_pic_path initWithCurrentStatus:custom_status initWithCurrentLocation:base_location phone_no: phone];
                    
                    NSData *encodedVcard = [NSKeyedArchiver archivedDataWithRootObject:vcardofuser];
                    [engine.userVcardDictionary setObject:encodedVcard forKey:user_id];
                    
                    [userDictionary setValue:fullName forKey:user_id];
                    
                    if (currentVCard != nil) {
                        [engine.userVcards addObject: fullName];
                        [engine.userIds addObject: user_id];
                        [engine.userAvailability addObject: availability_status];
                    }
                }
                [engine vcardUpdated];
            }
        };//End of onDidLoadResponse
    }];//End of block
    
}

-(void)fetch_membership_of_user{
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [prefs objectForKey:kMY_USER_ID], @"user_id",nil];
    
    //request block
    [[RKClient sharedClient] post:@"/usergroup/fetch_memberships_of_user" usingBlock:^(RKRequest *request) {
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            
            //parsing the JSON response
            NSString *re= [response bodyAsString];
            NSDictionary *__response= [par objectFromString:re error:nil];
            
            //From string to array
            NSDictionary *groups_dictionary= [__response objectForKey:@"RESPONSE"];
            NSArray *groups_array= [groups_dictionary objectForKey:@"groups"];
            
            //          Removing previous group values
            [engine.userGroups removeAllObjects];
            [engine.userGroupIds removeAllObjects];
            
            for (int i=0; i< [groups_array count]; i++) {
                NSDictionary *group_info= [groups_array objectAtIndex:i];
                NSString *group_name= [group_info objectForKey:@"group_name"];
                NSString *group_id= [group_info objectForKey:@"group_id"];
                
                [engine.userGroups addObject:group_name];
                [engine.userGroupIds addObject:group_id];
            }
        };//End of onDidLoadResponse
        
        request.onDidFailLoadWithError= ^(NSError *error) {
            NSLog(@"error2:%@",error);
        };//End of onDidFailLoadWithError
    }];//End of block
    
}

-(IBAction)fetchInbox:(id)sender{
    self.tableHidingButton.hidden = YES;
    
    [SVProgressHUD showWithStatus:@"Loading Inbox"];
    
    
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    @"0", @"page_no",nil];
    
    //request block
    [[RKClient sharedClient] post:@"/messages/fetch_inbox" usingBlock:^(RKRequest *request) {
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            [self.messageAuthorArray removeAllObjects];
            [self.messageSubjectArray removeAllObjects];
            [self.messageIdArray removeAllObjects];
            [self.messageUnreadArray removeAllObjects];
            
            //Initialize Parser
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            NSString *re= [response bodyAsString];
            NSDictionary *__response= [par objectFromString:re error:nil];
            
            NSArray *messages= [__response objectForKey:@"RESPONSE"];
            
            for (int i=0; i <[messages count]; i++) {
                NSDictionary *messageDetails= [messages objectAtIndex:i];
                
                NSLog(@"Message Details is [%@]", messageDetails);
                
                NSDictionary *authorDict= [messageDetails objectForKey:@"author"];
                NSString *author=[authorDict objectForKey:@"first_name"];
                
                NSString *messageID= [messageDetails objectForKey:@"id"];
                NSString *tableHeader= [NSString stringWithFormat: @"%@", author];
                
                NSString *subject= [messageDetails objectForKey:@"subject"];
                NSString *contents= [messageDetails objectForKey:@"contents"];
                
                NSString *read_status= [messageDetails objectForKey:@"my_read_status"];
                NSString *seen= @"";
                
                if ([read_status isEqualToString:@"new"]) {
                    int unread= [unreadCount intValue];
                    unread= unread+1;
                    
                    unreadCount= [NSString stringWithFormat:@"%d", unread];
                }
                
                NSMutableArray *followers= [messageDetails objectForKey:@"followers"];
                
                for (int j=0; j< [followers count]; j++) {
                    NSDictionary *followerDictionary= [followers objectAtIndex:j];
                    
                    
                    if ([[followerDictionary objectForKey:@"read_status"] isEqualToString:@"read"]) {
                        tableHeader= [NSString stringWithFormat:@"%@,%@", tableHeader, [followerDictionary objectForKey:@"first_name"]];
                        if ([seen length]>0) {
                            seen= [NSString stringWithFormat:@"%@,%@",seen,[followerDictionary objectForKey:@"first_name"] ];
                        }
                        else
                            seen= [followerDictionary objectForKey:@"first_name"];
                    }
                    else{
                        tableHeader= [NSString stringWithFormat:@"%@,%@", tableHeader, [followerDictionary objectForKey:@"first_name"]];
                    }
                }
                
                [self.messageAuthorArray addObject: tableHeader];
                [self.messageSubjectArray addObject: [NSString stringWithFormat:@"%@-  %@", subject, contents]];
                [self.messageIdArray addObject:messageID];
                [self.messageUnreadArray addObject:read_status];
                
                [self.inboxTableView reloadData];
            }
            
            self.inboxTableView.hidden = NO;
            self.loadingInbox.hidden = YES;
            self.bubbleTable.hidden = NO;
            self.tableHidingButton.hidden = NO;
            [SVProgressHUD dismiss];
            
            [self performSelector:@selector(stopLoading) withObject:nil afterDelay:0.0];
            
            [SVProgressHUD showImage:[UIImage imageNamed:@"message.png"] status:[NSString stringWithFormat:@"%@ new messages", unreadCount]];
            
            page_no= [NSString stringWithFormat:@"%d",1];
            [self fetch_more:nil];
        };//End of onDidLoadResponse
    }];//End of block
}

-(void)LoadingMore{
    UIView *refreshFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
    refreshFooterView.backgroundColor = [UIColor clearColor];
    
    UILabel *loadingMoreLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, REFRESH_HEADER_HEIGHT)];
    
    loadingMoreLabel.backgroundColor = [UIColor clearColor];
    loadingMoreLabel.font = [UIFont boldSystemFontOfSize:12.0];
    loadingMoreLabel.textAlignment = UITextAlignmentCenter;
    loadingMoreLabel.text= @"Loading Next Page";
    loadingMoreLabel.textColor=[UIColor grayColor];
    
    UIActivityIndicatorView *footerRefreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    footerRefreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 20) / 2), floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    footerRefreshSpinner.hidesWhenStopped = YES;
    
    [refreshFooterView addSubview:footerRefreshSpinner];
    [refreshFooterView addSubview:loadingMoreLabel];
    
    self.inboxTableView.tableFooterView= refreshFooterView;
    [footerRefreshSpinner startAnimating];
}

-(IBAction)fetch_more:(id)sender{
    [self performSelector:@selector(LoadingMore) withObject:nil afterDelay:0];
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    page_no, @"page_no",nil];
    
    //request block
    [[RKClient sharedClient] post:@"/messages/fetch_inbox" usingBlock:^(RKRequest *request) {
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            //Initialize Parser
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            NSString *re= [response bodyAsString];
            NSDictionary *__response= [par objectFromString:re error:nil];
            
            NSArray *messages= [__response objectForKey:@"RESPONSE"];
            
            NSString *status_msg=[__response objectForKey:@"STATUS_MESSAGE"];
            
            if ([status_msg isEqualToString:@"Inbox is empty"]) {
                [SVProgressHUD showSuccessWithStatus:@"No more messages"];
                self.inboxTableView.tableFooterView= nil;
                
                page_no= [NSString stringWithFormat:@"%d",-1];
            }
            else{
                for (int i=0; i <[messages count]; i++) {
                    NSDictionary *messageDetails= [messages objectAtIndex:i];
                    
                    NSDictionary *authorDict= [messageDetails objectForKey:@"author"];
                    NSString *author=[authorDict objectForKey:@"first_name"];
                    
                    NSString *messageID= [messageDetails objectForKey:@"id"];
                    NSString *tableHeader= [NSString stringWithFormat: @"%@", author];
                    
                    NSString *subject= [messageDetails objectForKey:@"subject"];
                    NSString *contents= [messageDetails objectForKey:@"contents"];
                    
                    NSString *read_status= [messageDetails objectForKey:@"my_read_status"];
                    
                    if ([read_status isEqualToString:@"new"]) {
                        int unread= [unreadCount intValue];
                        unread= unread+1;
                        
                        unreadCount= [NSString stringWithFormat:@"%d", unread];
                    }
                    
                    NSMutableArray *followers= [messageDetails objectForKey:@"followers"];
                    
                    for (int j=0; j< [followers count]; j++) {
                        NSDictionary *followerDictionary= [followers objectAtIndex:j];
                        tableHeader= [NSString stringWithFormat:@"%@,%@", tableHeader, [followerDictionary objectForKey:@"first_name"]];
                    }
                    
                    [self.messageAuthorArray addObject: tableHeader];
                    [self.messageSubjectArray addObject: [NSString stringWithFormat:@"%@-  %@", subject, contents]];
                    [self.messageIdArray addObject:messageID];
                    [self.messageUnreadArray addObject:read_status];
                    
                    [self.inboxTableView reloadData];
                }
                
                self.inboxTableView.hidden = NO;
                self.loadingInbox.hidden = YES;
                self.bubbleTable.hidden = NO;
                self.tableHidingButton.hidden = NO;
                [SVProgressHUD dismiss];
                
                int page= [page_no intValue];
                page= page+1;
                page_no= [NSString stringWithFormat:@"%d",page];
            }
        };//End of onDidLoadResponse
    }];//End of block
    //  Set the first row as selected
    [inboxTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
}

-(void)fetchMessageDetails:(NSString *)message_id latest_reply_id:(NSString *)reply_id{
    self.loadingInbox.hidden = NO;
    
    [SVProgressHUD showWithStatus:@"Loading Details"];
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    NSString *loggedInAs= [prefs objectForKey:kMY_FIRST_NAME];
    
    bubbleData= nil;
    bubbleData= [[NSMutableArray alloc]init];
    
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    message_id, @"message_id",
                                    reply_id, @"latest_reply_id", nil];
    
    //request block
    [[RKClient sharedClient] post:@"/messages/message_details" usingBlock:^(RKRequest *request) {
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            int bubbleTableViewCellIndex=0;
            
            NSMutableArray *current_attachments= [[NSMutableArray alloc]initWithCapacity:1];
            NSMutableArray *current_message_user_id= [[NSMutableArray alloc]init];
            
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            
            //parsing the JSON response
            NSString *re= [response bodyAsString];
            NSDictionary *__response= [par objectFromString:re error:nil];
            
            NSDictionary *dict= [__response objectForKey:@"RESPONSE"];
            
            NSLog(@"Message detail is [%@]", dict);
            
            NSString *content= [dict objectForKey:@"contents"];
            NSString *totalNumOfReplies= [dict objectForKey:@"total_num_of_replies"];
            NSString *authorFirstName= [[dict objectForKey:@"author_first_name"]lowercaseString];
            NSString *author_user_id= [dict objectForKey:@"author_user_id"];
            int total= [totalNumOfReplies intValue];
            
            //Adding the user id for the first object
            //This should be nil at this point
            [current_message_user_id addObject:author_user_id];
            
            NSMutableArray *hasAttachment= [[NSMutableArray alloc]init];
            
            //Attached by the ownner of the thread
            if (![[dict objectForKey:@"attached_files"] isEqual:[NSNull null]]) {
                hasAttachment=[NSMutableArray arrayWithArray:[dict objectForKey:@"attached_files"]] ;
                
                //If there are attachment parse it
                for (int i=0; i< [hasAttachment count]; i++) {
                    NSDictionary *file_object= [hasAttachment objectAtIndex:i];
                    NSString *path=[file_object objectForKey:@"file_path"];
                    NSString *type=[file_object objectForKey:@"filetype"];
                    NSString *thumb=[file_object objectForKey:@"thumb_path"];
                    
                    [current_attachments addObject:[Attached_File fileWithPath:path fileWithType:type fileWithThumb:thumb]];
                }
            }
            
            NSArray *arrayOfReplies= [dict objectForKey:@"replies"];
            
            NSBubbleData *data;
            
            if ([authorFirstName isEqualToString:[loggedInAs lowercaseString]]) {
                
                if ([hasAttachment count]>0) {
                    data = [NSBubbleData dataWithText:content andDate:[NSDate dateWithTimeIntervalSinceNow:400]  andType:BubbleTypeSomeoneElse andIdicator:AttachmentIndicatorShow andAttachmentArray:hasAttachment andIndex:[NSString stringWithFormat:@"%d",bubbleTableViewCellIndex ] andSenderName:loggedInAs];
                    bubbleTableViewCellIndex++;
                }
                else{
                    data = [NSBubbleData dataWithText:content andDate:[NSDate dateWithTimeIntervalSinceNow:400]  andType:BubbleTypeSomeoneElse andIdicator:AttachmentIndicatorHide andAttachmentArray:hasAttachment andIndex:nil andSenderName:[dict objectForKey:@"author_first_name"]];
                }
            }
            else
            {
                if ([hasAttachment count]>0) {
                    data= [NSBubbleData dataWithText:content andDate:[NSDate dateWithTimeIntervalSinceNow:400]  andType:BubbleTypeMine andIdicator:AttachmentIndicatorShow andAttachmentArray:hasAttachment andIndex:[NSString stringWithFormat:@"%d",bubbleTableViewCellIndex ]andSenderName:[dict objectForKey:@"author_first_name"]];
                    
                    bubbleTableViewCellIndex++;
                }
                else{
                    data= [NSBubbleData dataWithText:content andDate:[NSDate dateWithTimeIntervalSinceNow:400]  andType:BubbleTypeMine andIdicator:AttachmentIndicatorHide andAttachmentArray:hasAttachment andIndex:[NSString stringWithFormat:@"%d",bubbleTableViewCellIndex ] andSenderName:[dict objectForKey:@"author_first_name"]];
                }
            }
            
            [bubbleData addObject:data];
            
            for (int i=0; i< total; i++) {
                NSBubbleData *data1;
                
                NSDictionary *reply= [arrayOfReplies objectAtIndex:i];
                
                NSString *name= [[reply objectForKey:@"author_first_name"]lowercaseString];
                NSString *cont= [reply objectForKey:@"contents"];
                NSString *auth_user_id= [reply objectForKey:@"author_user_id"];
                
                [current_message_user_id addObject:auth_user_id];
                
                NSString *posted_at= [reply objectForKey:@"posted_at"];
                
                [NSDateFormatter setDefaultFormatterBehavior:NSDateFormatterBehavior10_4];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                
                //[NSDateFormatter setDefaultFormatterBehavior:NSDateFormatterBehaviorDefault];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss ZZZ"];
                
                NSDate *dateFromString = [[NSDate alloc] init];
                dateFromString = [dateFormatter dateFromString:posted_at];
                
                NSMutableArray *attachment= [[NSMutableArray alloc]init];
                
                if (![[reply objectForKey:@"attached_files"] isEqual:[NSNull null]]) {
                    attachment = [reply objectForKey:@"attached_files"];
                    
                    for (int i=0; i< [attachment count]; i++) {
                        NSDictionary *file_object= [attachment objectAtIndex:i];
                        NSString *path=[file_object objectForKey:@"file_path"];
                        NSString *type=[file_object objectForKey:@"filetype"];
                        NSString *thumb=[file_object objectForKey:@"thumb_path"];
                        
                        [current_attachments addObject:[Attached_File fileWithPath:path fileWithType:type fileWithThumb:thumb]];
                    }
                    
                }
                
                NSString *bubbleContent=cont;
                
                if ([name isEqualToString:loggedInAs]) {
                    
                    if ([attachment count]>0) {
                        data1= [NSBubbleData dataWithText:bubbleContent andDate:dateFromString  andType:BubbleTypeSomeoneElse andIdicator:AttachmentIndicatorShow andAttachmentArray:attachment andIndex:nil andSenderName:[reply objectForKey:@"author_first_name"]];
                        bubbleTableViewCellIndex++;
                    }
                    else{
                        data1= [NSBubbleData dataWithText:bubbleContent andDate:dateFromString  andType:BubbleTypeSomeoneElse andIdicator:AttachmentIndicatorHide andAttachmentArray:attachment andIndex:[NSString stringWithFormat:@"%d",bubbleTableViewCellIndex ]andSenderName:[reply objectForKey:@"author_first_name"]];
                    }
                    
                }
                
                else{
                    
                    if ([attachment count]>0) {
                        data1= [NSBubbleData dataWithText:bubbleContent andDate:dateFromString  andType:BubbleTypeMine andIdicator:AttachmentIndicatorShow andAttachmentArray:[NSArray arrayWithArray: attachment] andIndex:[NSString stringWithFormat:@"%d",bubbleTableViewCellIndex ] andSenderName:[reply objectForKey:@"author_first_name"]];
                        
                        bubbleTableViewCellIndex++;
                    }
                    else{
                        data1= [NSBubbleData dataWithText:bubbleContent andDate:dateFromString  andType:BubbleTypeMine andIdicator:AttachmentIndicatorHide andAttachmentArray:hasAttachment andIndex:[NSString stringWithFormat:@"%d",bubbleTableViewCellIndex ] andSenderName:[reply objectForKey:@"author_first_name"]];
                    }
                }
                
                [bubbleData addObject:data1];
            }
            [self.bubbleTable reloadData];
            [bubbleTable scrollToRowAtIndexPath:nil atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            
            
            //Update the currentMessageAttachments
            [engine.currentMessageAttachments removeAllObjects];
            [engine.currentMessageUserID removeAllObjects];
            
            
            for (int i=0; i< [current_attachments count]; i++) {
                [engine.currentMessageAttachments addObject:[current_attachments objectAtIndex:i ]];
            }
            for (int i=0; i< [current_message_user_id count]; i++) {
                [engine.currentMessageUserID addObject:[current_message_user_id objectAtIndex:i ]];
            }
            
            self.loadingInbox.hidden = YES;
            [SVProgressHUD dismiss];
        };//End of onDidLoadResponse
    }];//End of block
}

-(void)post_reply:(NSString *)message_id contents:(NSString *)contents {
    
    [SVProgressHUD show];
    
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    
    float longitude= [prefs floatForKey:kMY_LONGITUDE];
    float latitude= [prefs floatForKey:kMY_LATITUDE];
    
    NSString *geo_longitude= [NSString stringWithFormat:@"%f", longitude];
    NSString *geo_latitude= [NSString stringWithFormat:@"%f", latitude];
    
    RKParams* params = [RKParams params];
    
    if ([prefs objectForKey:@"hasAttachments"]) {
        RKParamsAttachment* attachment;
        
        for (int i=1; i<= numberOfattachment; i++) {
            
            //            NSString* imageName = [NSString stringWithFormat:@"photo%d.png",i];
            
            //Obtaining saved path
            //            NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            //            NSString* documentsDirectory = [paths objectAtIndex:0];
            //            NSString* fullPathToFile = [documentsDirectory stringByAppendingPathComponent:imageName];
            
            //            NSLog(@"fullPathToFile: %@", fullPathToFile);
            
            //Try to rewrite if possible
            UIImage *imageForUploading;
            switch (numberOfattachment) {
                case 1:
                    imageForUploading = self.attachedImage1.image;
                    break;
                case 2:
                    imageForUploading = self.attachedImage2.image;
                    break;
                case 3:
                    imageForUploading = self.attachedImage3.image;
                    break;
                case 4:
                    imageForUploading = self.attachedImage4.image;
                    break;
                case 5:
                    imageForUploading = self.attachedImage5.image;
                    break;
                default:
                    break;
            }
            NSData *imageData =  UIImagePNGRepresentation(imageForUploading);
            
            attachment = [params setData:imageData forParam:[NSString stringWithFormat:@"file_%d",i]];
            attachment.MIMEType = @"image/png";
            attachment.fileName = [NSString stringWithFormat:@"photo%d.png",i];
        }
    }
    
    // Set some values
    [params setValue:message_id forParam:@"message_id"];
    [params setValue:contents forParam:@"contents"];
    [params setValue:geo_longitude forParam:@"geo_longitude"];
    [params setValue:geo_latitude forParam:@"geo_latitude"];
    
    // Create an Attachment
    // Let's examine the RKRequestSerializable info...
    //    NSLog(@"RKParams HTTPHeaderValueForContentType = %@", [params HTTPHeaderValueForContentType]);
    //    NSLog(@"RKParams HTTPHeaderValueForContentLength = %d", [params HTTPHeaderValueForContentLength]);
    
    //request block
    [[RKClient sharedClient] post:@"/messages/post_reply" usingBlock:^(RKRequest *request) {
        request.timeoutInterval = 500;
        [request setParams:params];
        [request setBackgroundPolicy: RKRequestBackgroundPolicyContinue];
        request.onDidLoadResponse = ^(RKResponse *response) {
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            
            //parsing the JSON response
            NSString *re= [response bodyAsString];
            NSDictionary *__response= [par objectFromString:re error:nil];
            
            //            NSLog(@"%@", __response);
            
            //Todo-remove this
            /*Parse the response
             Only show success in case it is a succsess otherwise show appropiate message
             */
            
            [SVProgressHUD showSuccessWithStatus:@"File(s) successfully uploaded"];
            [prefs setBool:NO forKey:kATTACHMENT_AVAILABLE];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refresh" object:nil];
        };//End of onDidLoadResponse
    }];//End of block
    
    [SVProgressHUD showSuccessWithStatus:@"uploading in background"];
    [self resetAttachMentIndicatorValue];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refresh" object:nil];
    
}

#pragma mark - UITableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == inboxTableView) {
        return 1;
    }
    else return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == inboxTableView) {
        return [self.messageAuthorArray count];
    }
    else return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView
                             dequeueReusableCellWithIdentifier:@"CustomCellPraxis"];
    
    UILabel *textLabel = (UILabel *)[cell viewWithTag:1];
    
    UILabel *detailTextLabel = (UILabel *)[cell viewWithTag:2];
    detailTextLabel.numberOfLines =2;
    
    cell.backgroundColor= [UIColor grayColor];
    
    textLabel.text = [NSString stringWithFormat:@"%@", [self.messageAuthorArray objectAtIndex: indexPath.row]];
    
    detailTextLabel.text = [NSString stringWithFormat:@"%@", [self.messageSubjectArray objectAtIndex: indexPath.row]];
    
    
    UIImageView *imageView = (UIImageView *)
    [cell viewWithTag:3];
    
    
    if ([[messageUnreadArray objectAtIndex:indexPath.row] isEqualToString:@"new"]) {
        UIImage *cellImage = [UIImage imageNamed:@"unread_dot.png"];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.image = cellImage;
    }
    else{
        imageView.image= nil;
    }
    
    return cell;
}

#pragma mark - UITableView Delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int index= indexPath.row;
    
    currentMessageId = [self.messageIdArray objectAtIndex:index];
    
    [self.messageUnreadArray replaceObjectAtIndex:index withObject:@"read"];
    [self fetchMessageDetails:currentMessageId latest_reply_id:[NSString stringWithFormat:@"%d", 0]];
    
    [self.inboxTableView reloadData];
    
    perticipants.text= [NSString stringWithFormat:@"%@", [self.messageAuthorArray objectAtIndex: indexPath.row]];
}


#pragma mark - UIBubbleTableViewDataSource implementation

- (NSInteger)rowsForBubbleTable:(UIBubbleTableView *)tableView
{
    return [bubbleData count];
}

- (NSBubbleData *)bubbleTableView:(UIBubbleTableView *)tableView dataForRow:(NSInteger)row
{
    return [bubbleData objectAtIndex:row];
}

-(void)createNewConversation:(NSNotification *)notification{
    [self performSegueWithIdentifier: @"CreateNewConversation"
                              sender: self];
}

-(void)show_settings_page:(NSNotification *)notification{
    [self performSegueWithIdentifier: @"showsettingspage"
                              sender: self];
}

-(void)show_team_list:(NSNotification *)notification{
    [self performSegueWithIdentifier: @"showteamlist"
                              sender: self];
}

-(void)refresh:(NSNotification *)notification{
    
    [self.messageAuthorArray removeAllObjects];
    [self.messageSubjectArray removeAllObjects];
    [self.messageIdArray removeAllObjects];
    [self.messageUnreadArray removeAllObjects];
    
    [self fetchInbox:nil];
    [create_new_msg_popovercontroller dismissPopoverAnimated:YES];
}

#pragma mark MJDetailViewController Delegate
-(void)logout:(NSNotification *)notification{
    [SVProgressHUD showWithStatus:@"Logging out..."];
    
    //request block
    [[RKClient sharedClient] post:@"/authorize/logout" usingBlock:^(RKRequest *request) {
        request.onDidLoadResponse = ^(RKResponse *response) {
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            
            //parsing the JSON response
            NSString *re= [response bodyAsString];
            NSDictionary *__response= [par objectFromString:re error:nil];
            
            //Todo-remove this
            /*Parse the response
             Show logout anyway*/
            NSLog(@"%@", __response);
            
            NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
            [prefs setObject:@"NO" forKey:kLOGGED_IN];
            
            [SVProgressHUD showSuccessWithStatus:@"Logged Out"];
            [self dismissModalViewControllerAnimated:NO];
        };//End of onDidLoadResponse
    }];//End of block
}


//Pull to refresh

- (void)setupStrings{
    textPull = @"Pull down to refresh...";
    textRelease = @"Release to refresh...";
    textLoading = @"Loading...";
}

- (void)addPullToRefreshHeader {
    refreshHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 200, REFRESH_HEADER_HEIGHT)];
    refreshHeaderView.backgroundColor = [UIColor clearColor];
    
    refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, REFRESH_HEADER_HEIGHT)];
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = [UIFont boldSystemFontOfSize:12.0];
    refreshLabel.textAlignment = UITextAlignmentCenter;
    
    refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow.png"]];
    refreshArrow.frame = CGRectMake(floorf((REFRESH_HEADER_HEIGHT - 27) / 2),
                                    (floorf(REFRESH_HEADER_HEIGHT - 44) / 2),
                                    27, 44);
    
    refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    refreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 20) / 2), floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    
    [refreshHeaderView addSubview:refreshLabel];
    [refreshHeaderView addSubview:refreshArrow];
    [refreshHeaderView addSubview:refreshSpinner];
    
    [self.inboxTableView addSubview:refreshHeaderView];
    [self setupStrings];
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (isLoading) return;
    isDragging = YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (isLoading) {
        // Update the content inset, good for section headers
        if (scrollView.contentOffset.y > 0)
            self.inboxTableView.contentInset = UIEdgeInsetsZero;
        else if (scrollView.contentOffset.y >= -REFRESH_HEADER_HEIGHT)
            self.inboxTableView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (isDragging && scrollView.contentOffset.y < 0) {
        // Update the arrow direction and label
        [UIView animateWithDuration:0.25 animations:^{
            if (scrollView.contentOffset.y < -REFRESH_HEADER_HEIGHT) {
                // User is scrolling above the header
                refreshLabel.text = self.textRelease;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            } else {
                // User is scrolling somewhere within the header
                refreshLabel.text = self.textPull;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
            }
        }];
    }
    
    //The bottom is reached
    if (scrollView.contentOffset.y == scrollView.contentSize.height - scrollView.frame.size.height){
        // reached the bottom
        if (![page_no isEqualToString:@"-1"]) {
            [self fetch_more:nil];
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (isLoading) return;
    isDragging = NO;
    if (scrollView.contentOffset.y <= -REFRESH_HEADER_HEIGHT) {
        // Released above the header
        [self startLoading];
    }
}

- (void)startLoading {
    isLoading = YES;
    unreadCount = @"0";
    
    // Show the header
    [UIView animateWithDuration:0.3 animations:^{
        self.inboxTableView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
        refreshLabel.text = self.textLoading;
        refreshArrow.hidden = YES;
        [refreshSpinner startAnimating];
    }];
    
    // Refresh action!
    [self pull_refresh];
}

- (void)stopLoading {
    isLoading = NO;
    
    // Hide the header
    [UIView animateWithDuration:0.3 animations:^{
        self.inboxTableView.contentInset = UIEdgeInsetsZero;
        [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(stopLoadingComplete)];
                     }];
}

- (void)stopLoadingComplete {
    // Reset the header
    refreshLabel.text = self.textPull;
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}

- (void)pull_refresh {
    // Don't forget to call stopLoading at the end.
    [self fetchInbox:nil];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    [prefs setBool:YES forKey:kATTACHMENT_AVAILABLE];
    
    numberOfattachment++;
    [self resetAttachMentIndicatorValue];
    
    NSString *imageName= [NSString stringWithFormat:@"photo%d.png", numberOfattachment];
    
    //extracting image from the picker and saving it
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:@"public.image"]){
        
        UIImage *editedImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        NSData *webData = UIImagePNGRepresentation(editedImage);
        //        NSData *webData = UIImageJPEGRepresentation(editedImage, 0.7);
        
        //obtaining saving path
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* documentsDirectory = [paths objectAtIndex:0];
        NSString* fullPathToFile = [documentsDirectory stringByAppendingPathComponent:imageName];
        [webData writeToFile:fullPathToFile atomically:NO];
        
        switch (numberOfattachment) {
            case 1:
                [self.attachedImage1 setImage:[UIImage imageWithContentsOfFile:fullPathToFile]];
                break;
            case 2:
                [self.attachedImage2 setImage:[UIImage imageWithContentsOfFile:fullPathToFile]];
                break;
            case 3:
                [self.attachedImage3 setImage:[UIImage imageWithContentsOfFile:fullPathToFile]];
                break;
            case 4:
                [self.attachedImage4 setImage:[UIImage imageWithContentsOfFile:fullPathToFile]];
                break;
            case 5:
                [self.attachedImage5 setImage:[UIImage imageWithContentsOfFile:fullPathToFile]];
                break;
            default:
                break;
        }
    }
    [picker dismissModalViewControllerAnimated:YES];
}



-(IBAction)showImagePicker:(id)sender{
    //Message text field resign first responder
    [self.text resignFirstResponder];
    [self resignFirstResponder];
    
    
    if ([self isPad]) {
        //if iPad
        popover = [[UIPopoverController alloc]
                   initWithContentViewController:imagePicker];
        
        [popover presentPopoverFromRect:self.textBottomBar.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
        
    }
    else{
        
        // Delegate is self
        imagePicker.delegate = self;
        
        // Show image picker
        [self presentModalViewController:imagePicker animated:YES];
    }
}

-(BOOL)isPad {
    return (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad);
}

-(IBAction)toggleAttachmentBar:(id)sender{
    if (attachmentBarHidden == YES) {
        self.attachmentBottomBar.hidden= NO;
        attachmentBarHidden= NO;
    }
    else{
        self.attachmentBottomBar.hidden= YES;
        self.attachmentIndicatorBar.hidden= YES;
        attachmentBarHidden= YES;
    }
}

-(void)resetAttachMentIndicatorValue{
    self.attachmentIndicator.text = [NSString stringWithFormat:@"+%d", numberOfattachment];
}

-(IBAction)showPreview:(id)sender{
    self.previewBoard.hidden =NO;
    [[KGModal sharedInstance] showWithContentView:self.previewBoard andAnimated:YES];
}
-(IBAction)hidePreview:(id)sender{
    self.previewBoard.hidden =YES;
    [[KGModal sharedInstance] hideAnimated:YES];
}

-(IBAction)previewImage1:(id)sender{
    [self.largeImage setImage:[attachedImage1 image]];
}

-(IBAction)previewImage2:(id)sender{
    [self.largeImage setImage:[attachedImage2 image]];
}
-(IBAction)previewImage3:(id)sender{
    [self.largeImage setImage:[attachedImage3 image]];
}
-(IBAction)previewImage4:(id)sender{
    [self.largeImage setImage:[attachedImage4 image]];
}
-(IBAction)previewImage5:(id)sender{
    [self.largeImage setImage:[attachedImage5 image]];
}

-(void)get_all_groups{
    //request block
    [[RKClient sharedClient] post:@"/usergroup/list_all_groups" usingBlock:^(RKRequest *request) {
        request.onDidLoadResponse = ^(RKResponse *response) {
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            
            //parsing the JSON response
            NSString *re= [response bodyAsString];
            NSDictionary *__response= [par objectFromString:re error:nil];
            
            //Todo-remove this
            /*Pase response
             find usefullness of this method*/
            
        };//End of onDidLoadResponse
        
        request.onDidFailLoadWithError= ^(NSError *error) {
            NSLog(@"error2:%@",error);
        };//End of onDidFailLoadWithError
    }];//End of block
    
}

-(void)get_new_count{
    //request block
    [[RKClient sharedClient] post:@"/notification/get_all_new_counts" usingBlock:^(RKRequest *request) {
        request.onDidLoadResponse = ^(RKResponse *response) {
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            
            //parsing the JSON response
            NSString *re= [response bodyAsString];
            NSDictionary *__response= [par objectFromString:re error:nil];
            
            NSDictionary *_response=[__response objectForKey:@"RESPONSE"];
            
            int messageCount=[[_response objectForKey:@"message"]intValue];
            
            NSString *statusToShow= [__response objectForKey:@"STATUS_MESSAGE"];
            
            //This is not tested yet
            if (messageCount > 0) {
                [SVProgressHUD showSuccessWithStatus:statusToShow];
                [self fetchInbox:nil];
            }
        };//End of onDidLoadResponse
        request.onDidFailLoadWithError= ^(NSError
                                          *error) {
            NSLog(@"error2:%@",error);
        };//End of onDidFailLoadWithError
    }];//End of block
}

//Notification sent from -
//BubbleTableViewCell Method (previewAttachmentArray:)

-(void)Load_preview_of_images:(NSNotification *)notification{
    //The array which holds the images to preview
    NSMutableArray *files_to_preview=[[NSMutableArray alloc]init];
    
    Attached_File *file_object=notification.object;
    [files_to_preview addObject:file_object];
    
    [SVProgressHUD showWithStatus:@"Loading Preview"];
    [self performSelectorInBackground:@selector(show_preview:) withObject:files_to_preview];
}

-(void)show_preview:(NSArray *)files_to_preview{
    if (![[files_to_preview objectAtIndex:0] isEqual:[NSNull null]]) {
        Attached_File *attached_file=(Attached_File *)[files_to_preview objectAtIndex:0];
        
        //        NSLog(@"IS it Attached_file class? [%c]",[[files_to_preview objectAtIndex:0] isKindOfClass:[Attached_File class]]);
        
        
        if ([attached_file.file_type isEqualToString:@"jpg"]||[attached_file.file_type isEqualToString:@"jpeg"]||[attached_file.file_type isEqualToString:@"png" ]|| [attached_file.file_type isEqualToString:@"gif"] ) {
            
            NSString *desired_path= attached_file.file_path;
            
            UIImage *image1 = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:desired_path]]];
            self.attachedImage1.image= image1;
            
        }
        else if ([attached_file.file_type isEqualToString:@"pdf"]){
            UIImage *image1 = [UIImage imageNamed:@"pdf_icon"];
            self.attachedImage1.image= image1;
        }
        else if ([attached_file.file_type isEqualToString:@"mp3"]){
            UIImage *image1 = [UIImage imageNamed:@"mp3_icon"];
            self.attachedImage1.image= image1;
        }
        else if ([attached_file.file_type isEqualToString:@"mp4"]){
            UIImage *image1 = [UIImage imageNamed:@"mp4_icon"];
            self.attachedImage1.image= image1;
        }
        else{
            UIImage *image1 = [UIImage imageNamed:@"unknown_file_format"];
            self.attachedImage1.image= image1;
        }
        
    }
    else{
        self.attachedImage1.image= nil;
    }
    
    self.previewBoard.hidden =NO;
    
    [SVProgressHUD dismiss];
    [[KGModal sharedInstance] showWithContentView:self.previewBoard andAnimated:YES];
}



- (void)registerNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(createNewConversation:) name:@"createNewConversation" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(show_settings_page:) name:@"showsettings" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(show_team_list:) name:@"showteam" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh:) name:@"refresh" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logout:) name:@"logout" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Load_preview_of_images:) name:@"showAttachmentFromArray" object:nil];
}
@end