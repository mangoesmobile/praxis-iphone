//
//  UserTableViewCell.m
//  Messenger
//
//  Created by Mahbub Morshed on 9/23/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import "UserTableViewCell.h"

@implementation UserTableViewCell
@synthesize userName= _userName;
@synthesize selection= _selection;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
