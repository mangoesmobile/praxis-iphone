//
//  ForgotPasswordView.h
//  Messenger
//
//  Created by Mahbub Morshed on 10/4/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController<UITextFieldDelegate>{
    
}
@property(strong, nonatomic) IBOutlet UIButton *praxisButton;
@property(strong, nonatomic) IBOutlet UIView *imageCollection;
@property(strong, nonatomic) IBOutlet UITextField *enterEmail;
@property(strong, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property(strong, nonatomic) IBOutlet UILabel *label;

-(IBAction)GoBack:(id)sender;
@end
