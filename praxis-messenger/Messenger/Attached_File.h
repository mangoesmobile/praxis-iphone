//
//  Files.h
//  Messenger
//
//  Created by Mahbub Morshed on 11/9/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Attached_File : NSObject

@property(nonatomic,strong) NSString *file_path;
@property(nonatomic,strong) NSString *file_type;
@property(nonatomic,strong) NSString *thumb_path;

+ (id)fileWithPath:(NSString *)file_path fileWithType:(NSString *)file_type fileWithThumb:(NSString *)thumb_path;

- (id)initWithPath:(NSString *)initPath initWithType:(NSString *)initType initWithThumb:(NSString *)initThumb;
@end
