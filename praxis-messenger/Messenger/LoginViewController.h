//
//  MainViewController.h
//  Messenger
//
//  Created by Mahbub Morshed on 9/3/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import "MessengerViewController.h"
#import "NSData+AES256.h"

#import "MyCLController.h"
#import "MYVcard.h"

#import "VcardData.h"


@interface LoginViewController : UIViewController <MessengerViewControllerDelegate, UITextFieldDelegate>{
//	MyCLController *locationController;
}

@property(strong) IBOutlet UITextField *userNameTextField;
@property(strong) IBOutlet UITextField *passwordTextField;
@property(strong) IBOutlet UIButton *loginButton;
@property(strong) IBOutlet UIActivityIndicatorView *activity;
@property(strong) IBOutlet UILabel *loginfailed;

@property(strong) IBOutlet UIButton *forgotPasswordButton;

-(IBAction)login:(id)sender;
-(void)get_my_vcard;
-(void)loggedOut:(NSNotification *)notification;

//- (void)locationUpdate:(CLLocation *)location;
//- (void)locationError:(NSError *)error;

//-(void) additionalSetup:(id)sender;

-(BOOL)isPad;

@end
