//
//  TeamCell.m
//  Messenger
//
//  Created by Mahbub Morshed on 11/20/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import "TeamCell.h"

@implementation TeamCell
@synthesize user_name;
@synthesize write_msg_button;
@synthesize phone_call_button;
@synthesize phone_no;
@synthesize userID;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(IBAction)doPhoneCall:(id)sender{
    [SVProgressHUD showErrorWithStatus: phone_no];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phone_no]];
}

-(IBAction)write_msg:(id)sender{
    MMAppEngine *engine=[MMAppEngine sharedManager];
    
    [engine.selectedUserIds removeAllObjects];
    [engine.selectedUserNames removeAllObjects];
    
    [engine.selectedUserIds addObject:userID];
    [engine.selectedUserNames addObject:user_name.text];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"createNewConversationFromTeamList" object:nil];
}

@end
