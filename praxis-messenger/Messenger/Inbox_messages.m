//
//  Inbox_messages.m
//  Messenger
//
//  Created by Mahbub Morshed on 10/15/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import "Inbox_messages.h"


@implementation Inbox_messages

@dynamic author;
@dynamic messageId;
@dynamic subject;
@dynamic followers;

@end
