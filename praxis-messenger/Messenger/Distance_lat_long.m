//
//  Distance_lat_long.m
//  Messenger
//
//  Created by Mahbub Morshed on 10/21/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import "Distance_lat_long.h"

@implementation Distance_lat_long

const double PIx = 3.141592653589793;
const double RADIO = 6371; // Mean radius of Earth in Km

double convertToRadians(double val) {
    
    return val * PIx / 180;
}

+(double)kilometresBetweenPlace1:(CLLocationCoordinate2D) place1 andPlace2:(CLLocationCoordinate2D) place2 {
    
    double dlon = convertToRadians(place2.longitude - place1.longitude);
    double dlat = convertToRadians(place2.latitude - place1.latitude);
    
    double a = ( pow(sin(dlat / 2), 2) + cos(convertToRadians(place1.latitude))) * cos(convertToRadians(place2.latitude)) * pow(sin(dlon / 2), 2);
    double angle = 2 * asin(sqrt(a));
    
    return angle * RADIO;
}


@end
