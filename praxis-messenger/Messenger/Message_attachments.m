//
//  Message_attachments.m
//  Messenger
//
//  Created by Mahbub Morshed on 11/5/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import "Message_attachments.h"

@implementation Message_attachments

@synthesize  file_path;
@synthesize  file_type;
@synthesize  thumb_path;


- (id)initWithFilePath:(NSString *)init_file_Path andFileType:(NSString *)init_file_type andThumbPath:(NSString *)init_thumb_path
{
    self = [super init];
    if (self)
    {
        file_path= init_file_Path;
        file_type=  init_file_type;
        thumb_path= init_thumb_path;
        
    }
    return self;
}

@end
