//
//  TeamCell.h
//  Messenger
//
//  Created by Mahbub Morshed on 11/20/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMAppEngine.h"
#import "VcardData.h"

@interface TeamCell : UITableViewCell

@property(nonatomic, strong) IBOutlet UILabel *user_name;
@property(nonatomic, strong) IBOutlet UIButton *phone_call_button;
@property(nonatomic, strong) IBOutlet UIButton *write_msg_button;

@property(nonatomic, strong) NSString *phone_no;
@property(nonatomic, strong) NSString *userID;

-(IBAction)write_msg:(id)sender;
-(IBAction)doPhoneCall:(id)sender;
@end
