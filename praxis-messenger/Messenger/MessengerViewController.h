//
//  FlipsideViewController.h
//  Messenger
//
//  Created by Mahbub Morshed on 9/3/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

//THE WHOLE CLASS NEEDS RE-WRITTING
#import <UIKit/UIKit.h>
#import "QuartzCore/QuartzCore.h"
#import "WEPopoverController.h"
#import "UIViewController+MJPopupViewController.h"
#import "MJDetailViewController.h"
#import "MMAppEngine.h"
#import "UIBubbleTableView.h"
#import "UserGroupViewController.h"
#import "Menupopovercontent.h"
#import "KGModal.h"
#import "Attached_File.h"

#import "VcardData.h"


@class MessengerViewController;

@protocol MessengerViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(MessengerViewController *)controller;
@end

@interface MessengerViewController : UIViewController<UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, UIBubbleTableViewDataSource,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIPopoverControllerDelegate>{
    
    NSString *inboxtableRect;
    NSString *bottomBarRect;
    NSString *textBottomBarRect;
    NSString *sendButtonRect;
    NSString *textRect;
    NSString *tableHiddingButtonRect;
    
    BOOL hidden;
    
    UIViewController *popUpCard;
    
    UserGroupViewController *create_new_msg_content;
    WEPopoverController *create_new_msg_popovercontroller;
    Menupopovercontent *menu_content;
    WEPopoverController *menu_popovercontroller;
    
    MMAppEngine *engine;
    
    NSMutableArray *bubbleData;
    NSString *currentMessageId;
    MJDetailViewController *detail;
    NSString *unreadCount;
    NSString *page_no;
    BOOL firstLoad;
    UIImagePickerController *imagePicker;
    UIPopoverController* popover;
    int numberOfattachment;
    NSString *need_repositioning;
    BOOL attachmentBarHidden;
    
    UITapGestureRecognizer *tap;
}

@property (weak, nonatomic) id <MessengerViewControllerDelegate> delegate;

@property(strong) NSString *page_no;

@property(strong) IBOutlet UIButton *attachment_button;

@property(strong) IBOutlet UIActivityIndicatorView *inboxLoadingActivity;

@property(strong) IBOutlet UILabel *loadingInbox;

@property(strong) IBOutlet UITableView *inboxTableView;

@property(strong) IBOutlet UIButton *tableHidingButton;

@property(strong) IBOutlet UIView *bottomBar;

@property(strong) IBOutlet UIView *popOverPlacingView;

@property(strong) IBOutlet UIView *textBottomBar;

@property(strong) IBOutlet UITextField *text;

@property(strong) IBOutlet UIButton *sendButton;

@property(strong) IBOutlet UIButton *trashButton;

@property(strong) NSMutableArray *messageAuthorArray;

@property(strong) NSMutableArray *messageSubjectArray;

@property(strong) NSMutableArray *messageUnreadArray;

@property(strong) NSMutableArray *messageIdArray;

@property(strong) NSMutableArray *allUserName;

@property(strong) NSString *unreadCount;

@property(strong) IBOutlet UIBubbleTableView *bubbleTable;

@property(strong) IBOutlet UIPopoverController* popover;

@property(nonatomic, strong) IBOutlet UIImagePickerController *imagePicker;

@property(nonatomic, strong) IBOutlet UIView *attachmentBottomBar;

@property(nonatomic, strong) IBOutlet UIView *attachmentIndicatorBar;



// ATTACHMENT
@property(nonatomic, strong) IBOutlet UILabel *attachmentIndicator;

@property(nonatomic, strong) IBOutlet UIView *previewBoard;

@property(nonatomic, strong) IBOutlet UIImageView *attachedImage1;

@property(nonatomic, strong) IBOutlet UIImageView *attachedImage2;

@property(nonatomic, strong) IBOutlet UIImageView *attachedImage3;

@property(nonatomic, strong) IBOutlet UIImageView *attachedImage4;

@property(nonatomic, strong) IBOutlet UIImageView *attachedImage5;

@property(nonatomic, strong) IBOutlet UIButton *dummyButton1;

@property(nonatomic, strong) IBOutlet UIButton *dummyButton2;

@property(nonatomic, strong) IBOutlet UIButton *dummyButton3;

@property(nonatomic, strong) IBOutlet UIButton *dummyButton4;

@property(nonatomic, strong) IBOutlet UIButton *dummyButton5;

@property(nonatomic, strong) IBOutlet UIImageView *largeImage;

//Corner Button
@property(nonatomic,strong) IBOutlet UIButton *cornerMButton;

//Seen label
@property(nonatomic, strong) IBOutlet UILabel *seenLabel;


// All methods
-(IBAction)done:(id)sender;

//Inbox hide and show
-(IBAction)hideTableView:(id)sender;

-(IBAction)show:(id)sender;

-(IBAction)toogle:(id)sender;

-(IBAction)create_new_msg_popover:(id)sender;

-(IBAction)show_menu_popover:(id)sender;

-(void)createNewConversation:(NSNotification *)notification;

-(void)show_settings_page:(NSNotification *)notification;

-(void)show_team_list:(NSNotification *)notification;

-(void)refresh:(NSNotification *)notification;


// Online/ Offline check
- (void)reachabilityDidChange:(NSNotification *)notification;


// All network calls
- (void)get_all_groups;

- (void)getAllVcards;

- (IBAction)getMyVcard:(id)sender;

- (IBAction)fetchInbox:(id)sender;

- (IBAction)fetch_more:(id)sender;

- (void)fetchMessageDetails:(NSString *)message_id latest_reply_id:(NSString *)reply_id;

- (void)post_reply:(NSString *)message_id contents:(NSString *)contents;

- (void)fetch_membership_of_user;

- (void)logout;

// ATTACHMENT IN REPLY - NO NETWORK CALL YET
-(IBAction)toggleAttachmentBar:(id)sender;

-(IBAction)showImagePicker:(id)sender;

-(BOOL)isPad;

-(void)resetAttachMentIndicatorValue;

-(void)get_new_count;

-(void)LoadingMore;

-(void)Load_preview_of_images;

-(void)show_preview:(NSArray *)files_to_preview;

//Register Notifications
- (void)registerNotifications;



@end
