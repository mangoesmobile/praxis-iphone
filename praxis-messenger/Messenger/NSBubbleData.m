//
//  NSBubbleData.m
//
//  Created by Alex Barinov
//  StexGroup, LLC
//  http://www.stexgroup.com
//
//  Project home page: http://alexbarinov.github.com/UIBubbleTableView/
//
//  This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.
//  To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/
//

#import "NSBubbleData.h"

@implementation NSBubbleData

@synthesize date = _date;
@synthesize type = _type;
@synthesize indicator= _indicator;
@synthesize text = _text;
@synthesize attached_files= _attached_files;
@synthesize currentIndex= _currentIndex;
@synthesize sendername=__sendername;

+ (id)dataWithText:(NSString *)text andDate:(NSDate *)date andType:(NSBubbleType)type andIdicator:(NSBubbleAttachmentIndicator)indicator andAttachmentArray:(NSArray *)attachments andIndex:(NSString *)currentIndex andSenderName:(NSString *)currentSenderName
{
    return [[[NSBubbleData alloc] initWithText:text andDate:date andType:type andIdicator:indicator andAttachmentArray:attachments andIndex:currentIndex andSenderName:currentSenderName] autorelease];
}

- (id)initWithText:(NSString *)initText andDate:(NSDate *)initDate andType:(NSBubbleType)initType andIdicator:(NSBubbleAttachmentIndicator)initIndicator andAttachmentArray:(NSArray *)initAttachments andIndex:(NSString *)initCurrentIndex andSenderName:(NSString *)initSenderName
{
    self = [super init];
    if (self)
    {
        _text = [initText retain];
        if (!_text || [_text isEqualToString:@""]) _text = @" ";
        
        __sendername = [initSenderName retain];
        if (!__sendername || [__sendername isEqualToString:@""]) __sendername = @" ";
        
        _currentIndex = [initCurrentIndex retain];
        
        _date = [initDate retain];
        _type = initType;
        _indicator= initIndicator;
        
        _attached_files= [NSMutableArray arrayWithArray:initAttachments];
    }
    return self;
}

- (void)dealloc
{
    [_date release];
	_date = nil;
	[_text release];
	_text = nil;
    [super dealloc];
}

@end
