//
//  TeamViewController.h
//  Messenger
//
//  Created by Mahbub Morshed on 11/15/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeamViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>


@property(nonatomic, strong) IBOutlet UITableView *online_users_table;
@property(nonatomic, strong) IBOutlet UITableView *offline_users_table;

@end
