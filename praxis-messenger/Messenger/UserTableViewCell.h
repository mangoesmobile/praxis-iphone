//
//  UserTableViewCell.h
//  Messenger
//
//  Created by Mahbub Morshed on 9/23/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserTableViewCell : UITableViewCell

@property(strong) IBOutlet UILabel *userName;
@property(strong) IBOutlet UIImageView *selection;
@end
