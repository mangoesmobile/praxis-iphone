//
//  VcardViewController.h
//  Messenger
//
//  Created by Mahbub Morshed on 11/13/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"

#import "VcardViewControllerDelegate.h"
#import "VcardData.h"


@interface VcardViewController : UIViewController{

}

@property(nonatomic, strong) IBOutlet UIImageView *user_pic;
@property(nonatomic, strong) IBOutlet UILabel *status_label;
@property(nonatomic, strong) IBOutlet UILabel *location_label;

@property(nonatomic, strong) id <VcardViewControllerDelegate> delegate;

-(void)setupvcard:(NSNotification *)notification;
@end
