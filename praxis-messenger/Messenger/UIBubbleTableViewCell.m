//
//  UIBubbleTableViewCell.m
//
//  Created by Alex Barinov
//  StexGroup, LLC
//  http://www.stexgroup.com
//
//  Project home page: http://alexbarinov.github.com/UIBubbleTableView/
//
//  This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.
//  To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/
//


#import "UIBubbleTableViewCell.h"
#import "NSBubbleData.h"

@interface UIBubbleTableViewCell ()
- (void) setupInternalData;
@end

@implementation UIBubbleTableViewCell

@synthesize dataInternal = _dataInternal;
@synthesize attachment_array;
@synthesize usernameLabel;

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
	[self setupInternalData];
}

- (void) dealloc
{
    [_dataInternal release];
	_dataInternal = nil;
    [super dealloc];
}


- (void)setDataInternal:(NSBubbleDataInternal *)value
{
	[value retain];
	[_dataInternal release];
	_dataInternal = value;
	[self setupInternalData];
}

- (void) setupInternalData
{
    int widthToUse= self.dataInternal.labelSize.width;
    
    if (widthToUse < 100) {
        widthToUse = 100;
    }
    
    engine= [MMAppEngine sharedManager];
    
    if (self.dataInternal.header)
    {
        headerLabel.hidden = NO;  
        headerLabel.text = self.dataInternal.header;
    }
    else
    {
        headerLabel.hidden = YES;
    }
    
    NSBubbleType type = self.dataInternal.data.type;
    NSBubbleAttachmentIndicator inidicator= self.dataInternal.data.indicator;
    
    float x = (type == BubbleTypeSomeoneElse) ? 20 : self.frame.size.width - 20 - widthToUse;
    float y = 5 + (self.dataInternal.header ? 30 : 0);
    
    
    usernameLabel.font = [UIFont boldSystemFontOfSize:15.0];
    usernameLabel.frame = CGRectMake(x, y, 100,21 );
    usernameLabel.text= self.dataInternal.data.sendername;
    
    contentLabel.font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
    contentLabel.frame = CGRectMake(x, y+15, widthToUse, self.dataInternal.labelSize.height);
    contentLabel.text = self.dataInternal.data.text;
    
    if (type == BubbleTypeSomeoneElse)
    {
        bubbleImage.image = [[UIImage imageNamed:@"bubbleSomeone.png"] stretchableImageWithLeftCapWidth:21 topCapHeight:14];
        bubbleImage.frame = CGRectMake(x - 18, y - 4, widthToUse + 30, self.dataInternal.labelSize.height + 15+15);
    }
    else {
        bubbleImage.image = [[UIImage imageNamed:@"bubbleMine.png"] stretchableImageWithLeftCapWidth:15 topCapHeight:14];
        bubbleImage.frame = CGRectMake(x - 9, y - 4, widthToUse + 26, self.dataInternal.labelSize.height + 15 +15);
    }
    
    if (inidicator == AttachmentIndicatorShow) {
        attachmentButton.hidden= NO;
        attachmentButton.frame=CGRectMake(x+100, y, 22,20 );
    }
    else{
        attachmentButton.hidden= YES;
    }
    
    [self bringSubviewToFront:attachmentButton];    
}

-(IBAction)previewAttachmentArray:(id)sender{
    NSArray *__attachment_array=[engine.currentMessageAttachments objectAtIndex:[self.dataInternal.data.currentIndex intValue]];

    NSLog(@"%@", __attachment_array);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"showAttachmentFromArray" object:__attachment_array];
    
    //Notification processed at-
    //MessengerViewController Method (Load_preview_of_images:)
}
@end
