//
//  CreateNewViewController.m
//  Messenger
//
//  Created by Mahbub Morshed on 9/23/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import "CreateNewViewController.h"

@interface CreateNewViewController ()

@end

@implementation CreateNewViewController
@synthesize textBottomBar, attachmentBottomBar, subjectTextField,  attachmentIndicator, imagePicker,attachedImage1,attachedImage2, attachedImage3, attachedImage4, attachedImage5, dummyButton1, dummyButton2, dummyButton3, dummyButton4, dummyButton5,recipient_list_label, popover,messageTextView, subjects;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    numberOfattachment= 0;
    
    self.textBottomBar.layer.borderWidth= 2.0;
    self.attachmentBottomBar.layer.borderWidth= 2.0;
    
    self.messageTextView.layer.borderWidth= 2.0;
    self.messageTextView.layer.cornerRadius= 5.0;
    
    self.attachmentBottomBar.hidden =YES;
    attachmentBarHidden = YES;
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    [prefs setBool:NO forKey:kATTACHMENT_AVAILABLE];
    
    [self resetAttachMentIndicatorValue];
    self.previewBoard.hidden= YES;
    self.previewBoard.autoresizesSubviews= YES;
    
    imagePicker = [[UIImagePickerController alloc] init];
    
    // Set source to the Photo library
    imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    
    
    [self update_recipient_list];
    
    need_repositioning= @"YES";
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasHidden:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(update_recipient_list_notification:)
                                                 name:@"updaterecipientList"
                                               object:nil];
    
    NSString *subject= [prefs objectForKey:kSUBJECT_OF_MESSAGE];
    
    if ([subject length]>0) {
        [subjects setText: subject];
    }
    else{
        [subjects setText:@""];
    }
    
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"MainStoryboard"
                                                  bundle:nil];
    
    create_new_msg_content = [sb instantiateViewControllerWithIdentifier:@"UserGroupViewController"];
    
    create_new_msg_popovercontroller= [[WEPopoverController alloc] initWithContentViewController:create_new_msg_content];
    create_new_msg_popovercontroller.popoverContentSize= CGSizeMake(260, 370);
}

-(void)update_recipient_list_notification:(NSNotification *)notification{
    [self update_recipient_list];
}


-(void)update_recipient_list{
    MMAppEngine *engine=[MMAppEngine sharedManager];
    NSString *recipient_list;
    
    for (int i=0; i< [engine.selectedUserNames count]; i++) {
        if (i==0) {
            recipient_list= [NSString stringWithFormat:@"%@",[engine.selectedUserNames objectAtIndex:i]];
        }
        else
            recipient_list= [NSString stringWithFormat:@"%@, %@",recipient_list,[engine.selectedUserNames objectAtIndex:i]];
    }
    
    recipient_list_label.text = recipient_list;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.messageTextView becomeFirstResponder];
    return YES;
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    return YES;
    
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    return YES;
}

-(BOOL)isPad {
    return (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad);
}

-(IBAction)send:(id)sender{
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^{
                         
                         [self.subjectTextField resignFirstResponder];
                         self.textBottomBar.transform= CGAffineTransformTranslate(self.textBottomBar.transform, 0, 215);
                         self.attachmentBottomBar.transform= CGAffineTransformTranslate(self.attachmentBottomBar.transform, 0, 215);
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                     }];
    
    [self createNewMessage];
    
}

-(IBAction)toggleAttachmentBar:(id)sender{
    if (attachmentBarHidden == YES) {
        self.attachmentBottomBar.hidden= NO;
        attachmentBarHidden= NO;
    }
    else{
        self.attachmentBottomBar.hidden= YES;
        attachmentBarHidden= YES;
    }
}

-(void)createNewMessage{
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    
    float longitude= [prefs floatForKey:kMY_LONGITUDE];
    float latitude= [prefs floatForKey:kMY_LATITUDE];
    
    NSString *geo_longitude= [NSString stringWithFormat:@"%f", longitude];
    NSString *geo_latitude= [NSString stringWithFormat:@"%f", latitude];
    
    MMAppEngine *engine= [MMAppEngine sharedManager];
    NSString *userIDs= [[NSString alloc]init];
    
    for (int i=0; i< [engine.selectedUserIds count]; i++) {
        if (i==0) {
            userIDs= [NSString stringWithFormat:@"%@",[engine.selectedUserIds objectAtIndex:i]];
        }
        else
            userIDs= [NSString stringWithFormat:@"%@-%@",userIDs,[engine.selectedUserIds objectAtIndex:i]];
    }
    
    NSLog(@"%@", userIDs);
    
    NSString *messageText= messageTextView.text;
    NSString *subjectText= [NSString stringWithFormat:@"%@ %@",[prefs objectForKey:kSUBJECT_OF_MESSAGE ],subjectTextField.text];
    
    RKParams* params = [RKParams params];
    
    if ([prefs objectForKey:kATTACHMENT_AVAILABLE]) {
        NSLog(@"attachment found");
        RKParamsAttachment* attachment;
        
        for (int i=1; i<= numberOfattachment; i++) {
            
            NSString* imageName = [NSString stringWithFormat:@"photo%d.png",i];
            
            //Obtaining saved path
            NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString* documentsDirectory = [paths objectAtIndex:0];
            NSString* fullPathToFile = [documentsDirectory stringByAppendingPathComponent:imageName];
            
            NSLog(@"fullPathToFile: %@", fullPathToFile);
            
            UIImage *imageForUploading;
            
            //another try
            switch (numberOfattachment) {
                case 1:
                    imageForUploading = self.attachedImage1.image;
                    break;
                case 2:
                    imageForUploading = self.attachedImage2.image;
                    break;
                case 3:
                    imageForUploading = self.attachedImage3.image;
                    break;
                case 4:
                    imageForUploading = self.attachedImage4.image;
                    break;
                case 5:
                    imageForUploading = self.attachedImage5.image;
                    break;
                    
                default:
                    break;
            }
            NSData *imageData =  UIImagePNGRepresentation(imageForUploading);
            
            attachment = [params setData:imageData forParam:[NSString stringWithFormat:@"file_%d",i]];
            attachment.MIMEType = @"image/png";
            attachment.fileName = [NSString stringWithFormat:@"photo%d.png",i];
        }
    }
    
    // Set some values
    [params setValue:subjectText forParam:@"subject"];
    [params setValue:messageText forParam:@"contents"];
    [params setValue:geo_longitude forParam:@"geo_longitude"];
    [params setValue:geo_latitude forParam:@"geo_latitude"];
    [params setValue:userIDs forParam:@"notify_user_ids"];
    
    // Create an Attachment
    // Let's examine the RKRequestSerializable info...
    NSLog(@"RKParams HTTPHeaderValueForContentType = %@", [params HTTPHeaderValueForContentType]);
    NSLog(@"RKParams HTTPHeaderValueForContentLength = %d", [params HTTPHeaderValueForContentLength]);
    
    //request block
    [[RKClient sharedClient] post:@"/messages/post_new" usingBlock:^(RKRequest *request) {
        request.timeoutInterval = 500;
        [request setParams:params];
        [request setBackgroundPolicy: RKRequestBackgroundPolicyContinue];
        request.onDidLoadResponse = ^(RKResponse *response) {
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            
            //parsing the JSON response
            NSString *re= [response bodyAsString];
            NSDictionary *__response= [par objectFromString:re error:nil];
            
            NSLog(@"%@", __response);
            
            [SVProgressHUD showSuccessWithStatus:@"File(s) successfully uploaded"];
            [prefs setBool:NO forKey:kATTACHMENT_AVAILABLE];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refresh" object:nil];
            //[self dismissModalViewControllerAnimated:NO];
        };//End of onDidLoadResponse
    }];//End of block
    
    [SVProgressHUD showSuccessWithStatus:@"uploading in background"];
    
    [self dismissModalViewControllerAnimated:NO];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refresh" object:nil];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    [prefs setBool:YES forKey:kATTACHMENT_AVAILABLE];
    
    numberOfattachment++;
    [self resetAttachMentIndicatorValue];
    
    NSString *imageName= [NSString stringWithFormat:@"photo%d.png", numberOfattachment];
    
    //extracting image from the picker and saving it
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:@"public.image"]){
        
        UIImage *editedImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        NSData *webData = UIImagePNGRepresentation(editedImage);
        
        //obtaining saving path
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* documentsDirectory = [paths objectAtIndex:0];
        NSString* fullPathToFile = [documentsDirectory stringByAppendingPathComponent:imageName];
        
        [webData writeToFile:fullPathToFile atomically:NO];
        
        switch (numberOfattachment) {
            case 1:
                [self.attachedImage1 setImage:[UIImage imageWithContentsOfFile:fullPathToFile]];
                break;
            case 2:
                [self.attachedImage2 setImage:[UIImage imageWithContentsOfFile:fullPathToFile]];
                break;
            case 3:
                [self.attachedImage3 setImage:[UIImage imageWithContentsOfFile:fullPathToFile]];
                break;
            case 4:
                [self.attachedImage4 setImage:[UIImage imageWithContentsOfFile:fullPathToFile]];
                break;
            case 5:
                [self.attachedImage5 setImage:[UIImage imageWithContentsOfFile:fullPathToFile]];
                break;
            default:
                break;
        }
        
    }
    [picker dismissModalViewControllerAnimated:YES];
}



-(IBAction)showImagePicker:(id)sender{
    
    //Message text field resign first responder
    [self.subjectTextField resignFirstResponder];
    [self resignFirstResponder];
    
    
    if ([self isPad]) {
        //if iPad
        popover = [[UIPopoverController alloc]
                   initWithContentViewController:imagePicker];
        
        [popover presentPopoverFromRect:self.attachmentBottomBar.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
        
    }
    else{
        
        // Delegate is self
        imagePicker.delegate = self;
        
        // Show image picker
        [self presentModalViewController:imagePicker animated:YES];
    }
}

-(IBAction)GoBack:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissModalViewControllerAnimated:YES];
}


-(void)resetAttachMentIndicatorValue{
    self.attachmentIndicator.text = [NSString stringWithFormat:@"+%d", numberOfattachment];
}

-(IBAction)showPreview:(id)sender{
    self.previewBoard.hidden =NO;
    [[KGModal sharedInstance] showWithContentView:self.previewBoard andAnimated:YES];
}
-(IBAction)hidePreview:(id)sender{
    self.previewBoard.hidden =YES;
    [[KGModal sharedInstance] hideAnimated:YES];
}

-(IBAction)previewImage1:(id)sender{
        [self.largeImage setImage:[attachedImage1 image]];
//    UIImage *imageToSet= ((UIImageView *)[self valueForKey:[NSString stringWithFormat:@"attachedImage%d", 1]]).image;
    
//    [self.largeImage setImage:imageToSet];
}

-(IBAction)previewImage2:(id)sender{
    [self.largeImage setImage:[attachedImage2 image]];
}
-(IBAction)previewImage3:(id)sender{
    [self.largeImage setImage:[attachedImage3 image]];
}
-(IBAction)previewImage4:(id)sender{
    [self.largeImage setImage:[attachedImage4 image]];
}
-(IBAction)previewImage5:(id)sender{
    [self.largeImage setImage:[attachedImage5 image]];
}

- (void)keyboardWasShown:(NSNotification *)aNotification {
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^{
                         self.textBottomBar.transform= CGAffineTransformTranslate(self.textBottomBar.transform, 0, -215);
                         self.attachmentBottomBar.transform= CGAffineTransformTranslate(self.attachmentBottomBar.transform, 0, -215);
                     }
                     completion:^(BOOL finished){
                         need_repositioning= @"NO";
                     }];
}

- (void)keyboardWasHidden:(NSNotification *)aNotification {
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^{
                         self.textBottomBar.transform= CGAffineTransformTranslate(self.textBottomBar.transform, 0, 215);
                         self.attachmentBottomBar.transform= CGAffineTransformTranslate(self.attachmentBottomBar.transform, 0, 215);
                     }
                     completion:^(BOOL finished){
                         need_repositioning= @"YES";
                     }];
}

-(IBAction)create_new_msg_popover:(id)sender{
    UIView *v= (UIView *)sender;
    
    [create_new_msg_popovercontroller presentPopoverFromRect:v.frame inView:v permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

-(void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
}

-(BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    return YES;
}
@end
