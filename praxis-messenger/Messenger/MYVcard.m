//
//  MYVcard.m
//  Messenger
//
//  Created by Mahbub Morshed on 10/15/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import "MYVcard.h"


@implementation MYVcard

@dynamic first_name;
@dynamic last_name;
@dynamic custom_status;
@dynamic availability_status;
@dynamic base_location;
@dynamic geo_longitude;
@dynamic geo_latitude;
@dynamic is_workgroup_admin;
@dynamic org_approved;
@dynamic organization;
@dynamic phone;
@dynamic profile_pic_file_path;
@dynamic specialization;
@dynamic title;
@dynamic user_id;
@dynamic username;

@end
