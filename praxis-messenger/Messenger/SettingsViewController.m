//
//  SettingsViewController.m
//  Messenger
//
//  Created by Mahbub Morshed on 10/16/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//


#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController
@synthesize scroll, profile_pic_holder, _existing_password_field, _confirm_new_password_field, _enter_new_password_field;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    
    [SVProgressHUD dismiss];
    scroll.contentSize = CGSizeMake(320, 160);
    
    NSString *profile_pic_string= [NSString stringWithFormat:@"%@%@",kBASE_URL_FILE,[prefs objectForKey:kMY_PROFILE_PIC]];
    
    [self.profile_pic_holder setImageWithURL:[NSURL URLWithString:profile_pic_string]
                            placeholderImage:[UIImage imageNamed:@"profile_picture_thumbnail"]];
    
    NSLog(@"%@",profile_pic_string);
    
    imagePicker = [[UIImagePickerController alloc] init];
    
    // Set source to the Photo library
    imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction)back:(id)sender{
    [self dismissModalViewControllerAnimated:NO];
}

-(IBAction)change_password:(id)sender{
    //    NSString *currentPassword= [_existing_password_field text];
    NSString *newPass= [_enter_new_password_field text];
    NSString *confirmNewPass=[_confirm_new_password_field text];
    
    if ([newPass isEqualToString:confirmNewPass]) {
        int i =16-[newPass length];
        for ( ; i>0 ; i--) {
            newPass=[newPass stringByAppendingString:@" "];
        }
        
        NSData *passdata=[newPass dataUsingEncoding:NSUTF8StringEncoding];
        
        NSCharacterSet *charsToRemove = [NSCharacterSet characterSetWithCharactersInString:@"< >"];
        NSString *s = [[[passdata AES256EncryptWithKey:kAesKey] description]stringByTrimmingCharactersInSet:charsToRemove];
        NSCharacterSet *charsToRemove1 = [NSCharacterSet characterSetWithCharactersInString:@" "];
        NSString *s1= [s stringByTrimmingCharactersInSet:charsToRemove1];
        
        NSString *stringWithoutSpaces = [s1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        
        
        NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
        
        __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [prefs objectForKey:kENC_PASS], @"old_enc_password",
                                        stringWithoutSpaces, @"new_enc_password", nil];
        
        //request block
        [[RKClient sharedClient] post:@"/authorize/change_password" usingBlock:^(RKRequest *request) {
            [request setParams:params];
            request.onDidLoadResponse = ^(RKResponse *response) {
                RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
                
                //parsing the JSON response
                NSString *re= [response bodyAsString];
                NSDictionary *__response= [par objectFromString:re error:nil];
                
                NSLog(@"%@", __response);
                
                NSString *status= [__response objectForKey:@"STATUS_CODE"];
                NSString *status_msg= [__response objectForKey:@"STATUS_MESSAGE"];
                
                if ([status isEqualToString:@"200"]) {
                    [SVProgressHUD showSuccessWithStatus:status_msg];
                    [self dismissModalViewControllerAnimated:NO];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
                }
                else
                    [SVProgressHUD showErrorWithStatus:status_msg];
                
            };//End of onDidLoadResponse
            
            request.onDidFailLoadWithError= ^(NSError *error) {
                NSLog(@"error2:%@",error);
            };//End of onDidFailLoadWithError
        }];//End of block
    }
    else
        [SVProgressHUD showErrorWithStatus:@"New password doesn't match confirmation"];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


// Now Screeing! Image Picker
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    NSString *imageName= [NSString stringWithFormat:@"profile_picture.png"];
    
    //extracting image from the picker and saving it
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:@"public.image"]){
        
        UIImage *editedImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        NSData *webData = UIImagePNGRepresentation(editedImage);
        
        //obtaining saving path
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* documentsDirectory = [paths objectAtIndex:0];
        NSString* fullPathToFile = [documentsDirectory stringByAppendingPathComponent:imageName];
        
        [webData writeToFile:fullPathToFile atomically:NO];
        
        NSLog(@"fullPathToFile %@", fullPathToFile);
        
        [self.profile_pic_holder setImage:[UIImage imageWithContentsOfFile:fullPathToFile]];
        
        NSLog(@"photo picked");
        [self uploadPhoto:nil];
    }
    [picker dismissModalViewControllerAnimated:YES];
}



-(IBAction)showImagePicker:(id)sender{
    
    // Delegate is self
    imagePicker.delegate = self;
    
    if ([self isPad]) {
        //if iPad
        popover = [[UIPopoverController alloc]
                                        initWithContentViewController:imagePicker];
        
        [popover presentPopoverFromRect:self.profile_pic_holder.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
    else{
        // Show image picker
        [self presentModalViewController:imagePicker animated:YES];
    }
}

-(BOOL)isPad {
    return (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad);
}

-(IBAction)uploadPhoto:(id)sender{
    [SVProgressHUD showWithStatus:@"Sending data in background"];
    RKParams* params = [RKParams params];
    NSData *imageData =  UIImagePNGRepresentation(self.profile_pic_holder.image);
    
    RKParamsAttachment* attachment = [params setData:imageData forParam:[NSString stringWithFormat:@"file_1"]];
    attachment.MIMEType = @"image/png";
    attachment.fileName = [NSString stringWithFormat:@"photo.png"];
    
    // Create an Attachment
    NSLog(@"RKParams HTTPHeaderValueForContentType = %@", [params HTTPHeaderValueForContentType]);
    NSLog(@"RKParams HTTPHeaderValueForContentLength = %d", [params HTTPHeaderValueForContentLength]);
    
    //request block
    [[RKClient sharedClient] post:@"/user/set_profile_pic" usingBlock:^(RKRequest *request) {
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            
            //parsing the JSON response
            NSString *re= [response bodyAsString];
            NSDictionary *__response= [par objectFromString:re error:nil];
            
            NSLog(@"%@",__response);
        };//End of onDidLoadResponse
        
        request.onDidFailLoadWithError= ^(NSError *error) {
            NSLog(@"error2:%@",error);
        };//End of onDidFailLoadWithError
    }];//End of block
}
@end
