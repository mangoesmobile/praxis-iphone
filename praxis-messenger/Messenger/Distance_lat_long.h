//
//  Distance_lat_long.h
//  Messenger
//
//  Created by Mahbub Morshed on 10/21/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Distance_lat_long : NSObject

//Just use this method to calculate distance between two points
+(double)kilometresBetweenPlace1:(CLLocationCoordinate2D) place1 andPlace2:(CLLocationCoordinate2D) place2;
@end
