//
//  ForgotPasswordView.m
//  Messenger
//
//  Created by Mahbub Morshed on 10/4/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import "ForgotPasswordViewController.h"

@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController
@synthesize enterEmail, activity, label, imageCollection, praxisButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction)GoBack:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}

-(IBAction)forgotPassword:(id)sender{
    NSString *email= enterEmail.text;
    
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    email, @"email",
                                    nil];
    [SVProgressHUD show];
    
    //request block
    [[RKClient sharedClient] post:@"/authorize/forgot_password" usingBlock:^(RKRequest *request) {
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            
            //parsing the JSON response
            NSString *re= [response bodyAsString];
            NSDictionary *__response= [par objectFromString:re error:nil];
            
            NSLog(@"%@",__response);
            
            NSString *status_code= [__response objectForKey:@"STATUS_CODE"];
            NSString *message= [__response objectForKey:@"STATUS_MESSAGE"];
            
            self.label.text= message;
            
            if ([status_code intValue]==200) {
                [SVProgressHUD showSuccessWithStatus:message];
                [self GoBack:nil];
            }
            else{
                [SVProgressHUD showErrorWithStatus:message];
            }
        };//End of onDidLoadResponse
    }];//End of block
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.enterEmail resignFirstResponder];
    [self forgotPassword:nil];
    return YES;
}
@end
