//
//  AppDelegate.h
//  Messenger
//
//  Created by Mahbub Morshed on 9/3/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

/*
*** https://developer.apple.com/library/ios/#featuredarticles/iPhoneURLScheme_Reference/Articles/MapLinks.html#//apple_ref/doc/uid/TP40007894-SW1
 */
#import <UIKit/UIKit.h>
#import "MyCLController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, MyCLControllerDelegate>{
    MyCLController *locationController;
}

@property (strong, nonatomic) UIWindow *window;


- (void)reachabilityDidChange:(NSNotification *)notification;
-(BOOL)isPad;

- (void)locationUpdate:(CLLocation *)location;
- (void)locationError:(NSError *)error;
@end
