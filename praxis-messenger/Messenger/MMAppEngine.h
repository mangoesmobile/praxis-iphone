//
//  MMAppEngine.h
//  FrameWork
//
//  Created by Mahbub Morhsed on 19/6/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

/**
 * SingleTon class that works as the engine for the app 
 */

#import <Foundation/Foundation.h>

@interface MMAppEngine : NSObject {
    NSMutableArray *userVcards;
    NSMutableArray *userIds;
    NSMutableArray *userAvailability;
    
    NSMutableArray *selectedUserIds;
    NSMutableArray *selectedUserNames;
    
    NSMutableArray *userGroups;
    NSMutableArray *userGroupIds;
    
    NSMutableArray *currentMessageAttachments;
    
    NSMutableArray *currentMessageUserID;
    
    NSMutableDictionary *userVcardDictionary;
}
@property(strong) NSMutableArray *userVcards;
@property(strong) NSMutableArray *userIds;
@property(strong) NSMutableArray *selectedUserIds;
@property(strong) NSMutableArray *selectedUserNames;
@property(strong) NSMutableArray *userAvailability;
@property(strong) NSMutableArray *userGroups;
@property(strong) NSMutableArray *userGroupIds;

@property(strong) NSMutableArray *currentMessageAttachments;

@property(strong) NSMutableDictionary *userVcardDictionary;

@property(strong) NSMutableArray *currentMessageUserID;


+(id)sharedManager;
-(void)vcardUpdated;

@end
