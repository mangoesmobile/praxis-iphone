//
//  AppDelegate.m
//  Messenger
//
//  Created by Mahbub Morshed on 9/3/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//


#import "AppDelegate.h"


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    //CONFIGURE RESTKIT
    //Set base url for RKClient Singleton class
    RKClient *client= [RKClient clientWithBaseURL:[NSURL URLWithString:kBASE_URL ]];
    NSLog(@"%@", client.baseURL);
    client.requestQueue.showsNetworkActivityIndicatorWhenBusy = YES;
    client.cachePolicy=RKRequestCachePolicyLoadIfOffline|RKRequestCachePolicyTimeout;
    
    // Register for changes in network availability
    NSNotificationCenter* center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(reachabilityDidChange:) name:RKReachabilityDidChangeNotification object:nil];
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];

    
    if ([self isPad]) {
        NSLog(@"Running on iPad");
        [SVProgressHUD showImage: [UIImage imageNamed:@"message.png"] status:@"iPad"];
        [prefs setObject:@"iPad" forKey:@"device"];
    }
    else
        [prefs setObject:@"iPhone" forKey:@"device"];
    
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    
    locationController = [[MyCLController alloc] init];
	locationController.delegate = self;
	[locationController.locationManager startUpdatingLocation];

    return YES;
}


- (void)locationUpdate:(CLLocation *)location {
    float longitude=location.coordinate.longitude;
    float latitude=location.coordinate.latitude;
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    [prefs setFloat:longitude forKey:kMY_LONGITUDE];
    [prefs setFloat:latitude forKey:kMY_LATITUDE];
    
    NSLog(@"dLongitude : %f", longitude);
    NSLog(@"dLatitude : %f", latitude);
    
}

- (void)locationError:(NSError *)error {
	NSLog(@"%@", [error description]);
}

-(BOOL)isPad {
    return (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad);
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (void)reachabilityDidChange:(NSNotification *)notification {
    RKReachabilityObserver* observer = (RKReachabilityObserver *) [notification object];
    RKReachabilityNetworkStatus status = [observer networkStatus];
    if (RKReachabilityNotReachable == status) {
        RKLogInfo(@"No network access!");
        [SVProgressHUD showErrorWithStatus:@"No network access!"];
    } else if (RKReachabilityReachableViaWiFi == status) {
        RKLogInfo(@"Online via WiFi!");
    } else if (RKReachabilityReachableViaWWAN == status) {
        RKLogInfo(@"Online via Edge or 3G!");
    }
}
@end
