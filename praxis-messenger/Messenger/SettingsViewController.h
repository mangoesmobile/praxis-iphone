//
//  SettingsViewController.h
//  Messenger
//
//  Created by Mahbub Morshed on 10/16/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
#import "NSData+AES256.h"

@interface SettingsViewController : UIViewController<UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate>{

    UIImagePickerController *imagePicker;
    UIPopoverController* popover;
}

@property(strong) IBOutlet UIImageView *profile_pic_holder;
@property(strong) IBOutlet UIScrollView *scroll;

@property(strong) IBOutlet UITextField *_existing_password_field;
@property(strong) IBOutlet UITextField *_enter_new_password_field;
@property(strong) IBOutlet UITextField *_confirm_new_password_field;

@property(strong) IBOutlet UIPopoverController* popover;
-(IBAction)back:(id)sender;
-(IBAction)showImagePicker:(id)sender;
-(IBAction)uploadPhoto:(id)sender;
-(BOOL)isPad;

@end
