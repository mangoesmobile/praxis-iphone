//
//  MainViewController.m
//  Messenger
//
//  Created by Mahbub Morshed on 9/3/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//
#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController
@synthesize userNameTextField, passwordTextField, loginButton, activity, loginfailed;
@synthesize forgotPasswordButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    
    if ([prefs objectForKey:kCOOKIE]== nil) {
        [prefs setObject:@"NO" forKey:kLOGGED_IN];
    }
    
    
    if ([[prefs objectForKey:kLOGGED_IN]isEqualToString:@"YES"]) {
        
        userNameTextField.hidden= YES;
        passwordTextField.hidden= YES;
        loginButton.hidden= YES;
        forgotPasswordButton.hidden= YES;
        
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:[prefs objectForKey:kCOOKIE]];
        
        self.loginfailed.text= @"";
        
        [self performSegueWithIdentifier: @"ShowMessenger"
                                  sender: self];
        [self get_my_vcard];
    }
    else{
        userNameTextField.hidden= NO;
        passwordTextField.hidden= NO;
        loginButton.hidden= NO;
        forgotPasswordButton.hidden= NO;
        self.loginfailed.hidden =YES;
    }
    
    //    [self performSelectorInBackground:@selector(additionalSetup:) withObject:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loggedOut:) name:@"logout" object:nil];
}

-(void)loggedOut:(NSNotification *)notification{
    userNameTextField.hidden= NO;
    passwordTextField.hidden= NO;
    loginButton.hidden= NO;
    forgotPasswordButton.hidden= NO;
    self.loginfailed.hidden =YES;
}


-(void) additionalSetup:(id)sender{
    //  Push notification configuration
    //  Let the device know we want to receive push notifications
    //	[[UIApplication sharedApplication] registerForRemoteNotificationTypes:
    //  (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    //  locationController = [[MyCLController alloc] init];
    //	locationController.delegate = self;
    //	[locationController.locationManager startUpdatingLocation];
    
    //  Do any additional setup after loading the view, typically from a nib.
    
    //  if ([self isPad]) {
    //  NSLog(@"Running on iPad");
    //  [SVProgressHUD showImage: [UIImage imageNamed:@"message.png"] status:@"iPad"];
    //  }
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)isPad {
    return (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad);
}


#pragma mark - Flipside View

- (void)flipsideViewControllerDidFinish:(MessengerViewController *)controller
{
    [self dismissModalViewControllerAnimated:YES];
}

- (NSString *) LoginWIthUserName:(NSString *)username password:(NSString *)password{
    
    NSLog(@"Loggin in");
    self.loginfailed.hidden = YES;
    
    [SVProgressHUD show];
    
    self.loginButton.hidden = YES;
    
    __block NSString *returnString= nil;
    
    int i =16-[password length];
    for ( ; i>0 ; i--) {
        password=[password stringByAppendingString:@" "];
    }
    
    NSData *passdata=[password dataUsingEncoding:NSUTF8StringEncoding];
    
    NSCharacterSet *charsToRemove = [NSCharacterSet characterSetWithCharactersInString:@"< >"];
    NSString *s = [[[passdata AES256EncryptWithKey:kAesKey] description]stringByTrimmingCharactersInSet:charsToRemove];
    NSCharacterSet *charsToRemove1 = [NSCharacterSet characterSetWithCharactersInString:@" "];
    NSString *s1= [s stringByTrimmingCharactersInSet:charsToRemove1];
    
    NSString *stringWithoutSpaces = [s1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    username, @"username",
                                    stringWithoutSpaces, @"enc_password",
                                    @"TRUE",@"remember", nil];
    
    //request block
    [[RKClient sharedClient] post:@"/authorize/login" usingBlock:^(RKRequest *request) {
        [request setParams:params];
        
        NSLog(@"break 1");
        request.onDidLoadResponse = ^(RKResponse *response) {
            //            NSLog(@"Cookies at [%s],Line no [%d]", __FILE__, __LINE__);
            NSLog(@"Response after login is -[%@]", response);
            
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            
            //parsing the JSON response
            NSString *re= [response bodyAsString];
            NSDictionary *dict= [par objectFromString:re error:nil];
            NSString *status=[dict objectForKey:@"STATUS_MESSAGE"];
            NSString *statusCode=[dict objectForKey:@"STATUS_CODE"];
            NSString *error_log=[dict objectForKey:@"ERROR_LOG"];
            
            statusCode = [NSString stringWithFormat:@"%@", statusCode];
            error_log=[NSString stringWithFormat:@"%@", error_log];
            
            returnString = [NSString stringWithString:status];
            
            if([statusCode isEqualToString:@"200"]){
                
                NSLog(@"break 2");
                
                NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
                [prefs setObject:stringWithoutSpaces forKey:kENC_PASS];
                [prefs setObject:@"YES" forKey:kLOGGED_IN];
                
                NSLog(@"%@", response.cookies);
                
                NSArray *cookies= response.cookies;
                int cookiesCount= [cookies count];
                
                NSLog(@"%@", cookies);
                
                NSHTTPCookie *cookie= (NSHTTPCookie *)[cookies objectAtIndex:cookiesCount-1];
                [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
                [prefs setObject:[cookies objectAtIndex:cookiesCount-1] forKey:kCOOKIE];
                
                [self get_my_vcard];
            }//End of If
            else{
                [SVProgressHUD showErrorWithStatus:status];
                
                self.loginfailed.hidden= NO;
                self.loginButton.hidden= NO;
            }
        };//End of onDidLoadResponse
        
        request.onDidFailLoadWithError= ^(NSError *error) {
            NSLog(@"error2:%@",error);
        };//End of onDidFailLoadWithError
        
    }];//End of block
    
    return returnString;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showAlternate"]) {
        [[segue destinationViewController] setDelegate:self];
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

-(IBAction)login:(id)sender{
    NSString *userName= self.userNameTextField.text;
    NSString *password= self.passwordTextField.text;
    
    [self LoginWIthUserName:userName password:password];
}

-(void)get_my_vcard{
    // set up object manager
    RKObjectManager *objectManager =[RKObjectManager objectManagerWithBaseURL:[NSURL URLWithString:kBASE_URL]];
    // set up backing data store
    objectManager.objectStore = [RKManagedObjectStore objectStoreWithStoreFilename:@"messenger.sqlite"];
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    
    //request block
    [[RKClient sharedClient] post:@"/user/my_vcard" usingBlock:^(RKRequest *request) {
        request.onDidLoadResponse = ^(RKResponse *response) {
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            
            //parsing the JSON response
            NSString *re= [response bodyAsString];
            NSDictionary *__response= [par objectFromString:re error:nil];
            
            NSDictionary *insideResponse= [__response objectForKey:@"RESPONSE"];
            
            NSString *firstName;
            NSString *lastName;
            NSString *name;
            NSString *custom_status;
            NSString *availability_status;
            NSString *base_location;
            NSString *geo_longitude;
            NSString *geo_latitude;
            NSString *is_workgroup_admin;
            NSString *org_approved;
            NSString *organization;
            NSString *phone;
            NSString *profile_pic_file_path;
            NSString *specialization;
            NSString *title;
            NSString *user_id;
            NSString *username;
            
            if ([insideResponse objectForKey:@"first_name"] != [NSNull null]) {
                firstName= [insideResponse objectForKey:@"first_name"] ;
            }
            else
                firstName=@"";
            
            if ([insideResponse objectForKey:@"last_name"] != [NSNull null]) {
                lastName= [insideResponse objectForKey:@"last_name"];
            }
            else lastName= @"";
            
            name=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
            
            if ([insideResponse objectForKey:@"custom_status"]!=[NSNull null]) {
                custom_status= [insideResponse objectForKey:@"custom_status"];
            }
            else
                custom_status= @"";
            
            
            if ([insideResponse objectForKey:@"availability_status"]!= [NSNull null]) {
                availability_status= [insideResponse objectForKey:@"availability_status"];
            }
            else
                availability_status= @"";
            
            
            if ([insideResponse objectForKey:@"base_location"]!= [NSNull null]) {
                base_location= [insideResponse objectForKey:@"base_location"];
            }
            else
                base_location= @"";
            
            
            if ([insideResponse objectForKey:@"geo_longitude"] != [NSNull null]) {
                geo_longitude= [insideResponse objectForKey:@"geo_longitude"];
            }
            else
                geo_longitude= @"";
            
            if ([insideResponse objectForKey:@"geo_latitude"] != [NSNull null]) {
                geo_longitude= [insideResponse objectForKey:@"geo_latitude"];
            }
            else
                geo_latitude= @"";
            
            if ([insideResponse objectForKey:@"is_workgroup_admin"] != [NSNull null]) {
                is_workgroup_admin= [insideResponse objectForKey:@"is_workgroup_admin"];
            }
            else
                is_workgroup_admin= @"";
            
            if ([insideResponse objectForKey:@"org_approved"] != [NSNull null]) {
                org_approved= [insideResponse objectForKey:@"org_approved"];
            }
            else
                org_approved= @"";
            
            
            if ([insideResponse objectForKey:@"organization"] != [NSNull null]) {
                organization= [insideResponse objectForKey:@"organization"];
            }
            else{
                organization= @"not-available";
            }
            
            
            if ([insideResponse objectForKey:@"phone"] != [NSNull null]) {
                phone= [insideResponse objectForKey:@"phone"];
            }
            else{
                phone=@"0";
            }
            
            if ([insideResponse objectForKey:@"profile_pic_file_path"] != [NSNull null]) {
                profile_pic_file_path= [insideResponse objectForKey:@"profile_pic_file_path"];
            }
            else{
            profile_pic_file_path= @"";
            }
            
            
            if ([insideResponse objectForKey:@"specialization"] != [NSNull null]) {
                specialization= [insideResponse objectForKey:@"specialization"];
            }
            else{
            specialization= @"";
            }
            
            
            if ([insideResponse objectForKey:@"title"] != [NSNull null]) {
                specialization= [insideResponse objectForKey:@"title"];
            }
            else{
                title= @"";
            }
            
            
            user_id= [insideResponse objectForKey:@"user_id"];
            username= [insideResponse objectForKey:@"username"];
            
            [prefs setObject:[firstName lowercaseString]forKey:kMY_FIRST_NAME];
            [prefs setObject:name forKey:kMY_NAME];
            [prefs setObject:custom_status forKey:kMY_CUSTOM_STATUS];
            [prefs setObject:base_location forKey:kMY_BASE_LOCATION];
            [prefs setObject:profile_pic_file_path forKey:kMY_PROFILE_PIC];
            [prefs setObject:user_id forKey:kMY_USER_ID];
            
            MYVcard *vcard = [MYVcard object];
            vcard.first_name= firstName;
            vcard.last_name= lastName;
            vcard.custom_status= custom_status;
            
            if ([availability_status isEqual:[NSNull null]]) {
                vcard.availability_status= @"NULL";
            }
            else
                vcard.availability_status= availability_status;
            
            vcard.base_location= base_location;
            
            if (![geo_latitude isEqual:[NSNull null]]) {
                vcard.geo_latitude= geo_latitude;
            }
            else
                vcard.geo_latitude= @"null";
            
            if (![geo_longitude isEqual:[NSNull null]]) {
                vcard.geo_longitude= geo_longitude;
            }
            else
                vcard.geo_longitude= @"null";
            
            vcard.is_workgroup_admin= is_workgroup_admin;
            vcard.org_approved= org_approved;
            vcard.organization= organization;
            vcard.phone= phone;
            vcard.profile_pic_file_path= profile_pic_file_path;
            vcard.specialization= specialization;
            vcard.title= title;
            vcard.user_id= user_id;
            vcard.username= username;
            
            [objectManager.objectStore save:nil];
            
            //            [SVProgressHUD dismiss];
            self.loginButton.hidden = NO;
            
            [self performSegueWithIdentifier: @"ShowMessenger"
                                      sender: self];
            
            MMAppEngine *engine=[MMAppEngine sharedManager];
            //Storing VCard Data
            VcardData *vcardofuser= [[VcardData alloc]initWithProfilePicPath:profile_pic_file_path initWithCurrentStatus:custom_status initWithCurrentLocation:base_location phone_no:phone];
            
            NSData *encodedVcard = [NSKeyedArchiver archivedDataWithRootObject:vcardofuser];
            [engine.userVcardDictionary setObject:encodedVcard forKey:user_id];
        };//End of onDidLoadResponse
    }];//End of block
}


//- (void)locationUpdate:(CLLocation *)location {
//    float longitude=location.coordinate.longitude;
//    float latitude=location.coordinate.latitude;
//
//    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
//    [prefs setFloat:longitude forKey:kMY_LONGITUDE];
//    [prefs setFloat:latitude forKey:kMY_LATITUDE];
//
//    NSLog(@"dLongitude : %f", longitude);
//    NSLog(@"dLatitude : %f", latitude);
//
//}
//
//- (void)locationError:(NSError *)error {
//	NSLog(@"%@", [error description]);
//}
@end
