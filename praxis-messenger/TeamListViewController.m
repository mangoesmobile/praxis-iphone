//
//  TeamListViewController.m
//  Messenger
//
//  Created by Mahbub Morshed on 10/21/12.
//  Copyright (c) 2012 Mahbub Morshed. All rights reserved.
//

#import "TeamListViewController.h"

@interface TeamListViewController ()

@end

@implementation TeamListViewController
@synthesize slider, sliderbg, changeToMapSlider, hidden, onlineusers, offlineusers,onlineuserID,offlineuserID;
@synthesize onlineTable, offlineTable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIImage *minImage = [[UIImage imageNamed:@"slider_minimum.png"]
                         resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    UIImage *maxImage = [[UIImage imageNamed:@"slider_maximum.png"]
                         resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    
    [[UISlider appearance] setMaximumTrackImage:maxImage
                                       forState:UIControlStateNormal];
    [[UISlider appearance] setMinimumTrackImage:minImage
                                       forState:UIControlStateNormal];
    UIImage *thumbImage = [UIImage imageNamed:@"slider_dot.png"];
    
    [[UISlider appearance] setMaximumTrackImage:maxImage
                                       forState:UIControlStateNormal];
    [[UISlider appearance] setMinimumTrackImage:minImage
                                       forState:UIControlStateNormal];
    [[UISlider appearance] setThumbImage:thumbImage
                                forState:UIControlStateNormal];
    
    slider.transform = CGAffineTransformMakeScale(0.5, 0.5);
    
    sliderbg.transform = CGAffineTransformMakeScale(0.05, 00.05);
    changeToMapSlider.transform= CGAffineTransformMakeScale(0.05, 0.05);
    slider.hidden= YES;
    sliderbg.hidden= YES;
    changeToMapSlider.hidden=YES;
    
    hidden= @"yes";
    
    engine= [MMAppEngine sharedManager];
    
    [self populateOnlineandOfflineUserList];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(createNewConversation:) name:@"createNewConversationFromTeamList" object:nil];
}


-(void)populateOnlineandOfflineUserList{
    self.onlineusers=[[NSMutableArray alloc]init];
    self.offlineusers=[[NSMutableArray alloc]init];
    self.onlineuserID=[[NSMutableArray alloc]init];
    self.offlineuserID=[[NSMutableArray alloc]init];
    
    NSLog(@"Available User is %d", [engine.userAvailability count]);
    
    for (int i=0; i<[engine.userAvailability count]; i++) {
        if ([[engine.userAvailability objectAtIndex:i] isEqual:[NSNull null]]) {
            [offlineusers addObject:[engine.userVcards objectAtIndex:i]];
            NSLog(@"[%@] is offline", [engine.userVcards objectAtIndex:i]);
            [offlineuserID addObject:[engine.userIds objectAtIndex:i]];
        }
        else if ([[engine.userAvailability objectAtIndex:i] isEqualToString:@"online"]) {
            [onlineusers addObject:[engine.userVcards objectAtIndex:i]];
            NSLog(@"[%@] is online", [engine.userVcards objectAtIndex:i]);
            [onlineuserID addObject:[engine.userIds objectAtIndex:i]];
        }
        else{
            [offlineusers addObject:[engine.userVcards objectAtIndex:i]];
            NSLog(@"[%@] is offline", [engine.userVcards objectAtIndex:i]);
            [offlineuserID addObject:[engine.userIds objectAtIndex:i]];
        }
    }
    NSLog(@"online and offline users loaded [%d]", [engine.userAvailability count]);
    
    [self.onlineTable reloadData];
    [self.offlineTable reloadData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction)back:(id)sender{
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)toggleSlider:(id)sender{
    if ([hidden isEqualToString:@"yes"]) {
        sliderbg.hidden= NO;
        changeToMapSlider.hidden= NO;
        
        [UIView animateWithDuration:0.25
                              delay:0.0
                            options: UIViewAnimationCurveEaseOut
                         animations:^{
                             sliderbg.transform = CGAffineTransformMakeScale(1, 1);
                             changeToMapSlider.transform= CGAffineTransformMakeScale(1, 1);
                         }
                         completion:^(BOOL finished){
                             NSLog(@"Done!");
                         }];
        slider.hidden= NO;
        hidden= @"no";
    }
    else{
        [UIView animateWithDuration:0.25
                              delay:0.0
                            options: UIViewAnimationCurveEaseOut
                         animations:^{
                             sliderbg.transform = CGAffineTransformMakeScale(0.05, 0.05);
                             changeToMapSlider.transform= CGAffineTransformMakeScale(0.05, 0.05);
                         }
                         completion:^(BOOL finished){
                             NSLog(@"Done!");
                             slider.hidden= YES;
                             sliderbg.hidden= YES;
                             changeToMapSlider.hidden= YES;
                             hidden= @"yes";
                         }];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int total;
    if (tableView == onlineTable ) {
        total=[onlineusers count];
    }
    else
        total= [offlineusers count];
    
    return total;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TeamCell *cell = (TeamCell *)[tableView
                                      dequeueReusableCellWithIdentifier:@"OfflineList"];
    
    if (tableView == onlineTable) {
        cell= (TeamCell *)[tableView
               dequeueReusableCellWithIdentifier:@"OnlineList"];
        
        cell.user_name.text=[self.onlineusers objectAtIndex:indexPath.row] ;
        cell.userID= [self.onlineuserID objectAtIndex:indexPath.row] ;
        
        
        NSData *encodedVcardData = [engine.userVcardDictionary objectForKey:[self.onlineuserID objectAtIndex:indexPath.row]];
        VcardData *vcardcurrent = (VcardData *)[NSKeyedUnarchiver unarchiveObjectWithData:encodedVcardData];
        
        cell.phone_no = vcardcurrent.phone;
    }
    else{
        cell.user_name.text= [self.offlineusers objectAtIndex:indexPath.row];
        cell.userID= [self.offlineuserID objectAtIndex:indexPath.row] ;
        
        NSData *encodedVcardData = [engine.userVcardDictionary objectForKey:[self.offlineuserID objectAtIndex:indexPath.row]];
        VcardData *vcardcurrent = (VcardData *)[NSKeyedUnarchiver unarchiveObjectWithData:encodedVcardData];
        
        cell.phone_no = vcardcurrent.phone;
    }
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *userId;
    if (tableView ==onlineTable) {
        userId= [NSString stringWithFormat: @"Selcted user id [%d]",[[self.onlineuserID objectAtIndex:indexPath.row] intValue]];
    }
    else{
        userId= [NSString stringWithFormat: @"Selcted user id [%d]",[[self.offlineuserID objectAtIndex:indexPath.row] intValue]];
    }
    
    [SVProgressHUD showSuccessWithStatus:userId];
}

-(IBAction)ToggleOnlineTable:(id)sender{
    //Todo
    
    /*If onlineTableShowing
     Hide online table
     Translate offline table and offline label to top
     else
     Show online table
     Translate offline table and offline label to previous position
     */
    
    self.onlineTable.hidden=YES;
//    self.offlineLabel.transform= CGAffineTransformTranslate(<#CGAffineTransform t#>, <#CGFloat tx#>, <#CGFloat ty#>)
}

-(IBAction)ToggleOfflineTable:(id)sender{
    //Todo
    
    /*If offlineTableShowing
     Hide offline table
     else
     Show offline table
     */
    self.onlineTable.hidden=YES;
}

-(void)createNewConversation:(NSNotification *)notification{
    [self performSegueWithIdentifier: @"CreateNewConversationFromTeamList"
                              sender: self];
}



@end
