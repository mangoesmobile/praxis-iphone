//
//  SignupViewController.h
//  praxis-framework
//
//  Created by Mahbub Morshed on 8/1/12.
//  Copyright (c) 2012 ManGoes Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ELCTextfieldCell.h"

@interface SignupViewController : UIViewController<ELCTextFieldDelegate, UITableViewDelegate, UITableViewDataSource>{
    UITableView *tableView;
    
    NSArray *labels;
	NSArray *placeholders;
}

@property (strong) IBOutlet UITableView *tableView;
@property (strong) NSArray *labels;
@property (strong) NSArray *placeholders;

@end