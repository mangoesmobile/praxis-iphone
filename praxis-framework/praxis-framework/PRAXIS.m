//
//  Authorize.m
//  FrameWork
//
//  Created by Mahbub Morshed on 7/17/12.
//  Copyright (c) 2012 Mangoes Mobile. All rights reserved.
//

#define kAesKey @"jc0gvDzyg3lpwmQyTbHhBSFlD4wkiLy8"

#import "PRAXIS.h"

@implementation PRAXIS
/*************************************************************************************************/
//General methods for everyone
//call this method for encrytion
-(NSString*) sha1:(NSString*)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
    
}


/*************************************************************************************************/
//Authorize 
//todo: add encryption 
- (NSString *) LoginWIthUserName:(NSString *)username password:(NSString *)password{
    
    __block NSString *returnString= nil;
    
    int i =16-[password length];
    for ( ; i>0 ; i--) {
        NSLog(@"%@%@", password,@"/");
        password=[password stringByAppendingString:@" "];
    }
    
    NSData *passdata=[password dataUsingEncoding:NSUTF8StringEncoding];    
    
    NSCharacterSet *charsToRemove = [NSCharacterSet characterSetWithCharactersInString:@"< >"];
    NSString *s = [[[passdata AES256EncryptWithKey:kAesKey] description]stringByTrimmingCharactersInSet:charsToRemove];
    NSCharacterSet *charsToRemove1 = [NSCharacterSet characterSetWithCharactersInString:@" "];
    NSString *s1= [s stringByTrimmingCharactersInSet:charsToRemove1];
    
    NSString *stringWithoutSpaces = [s1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"returned String %@",stringWithoutSpaces);
    
    NSLog(@"returned String %@",s);
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    username, @"username", 
                                    stringWithoutSpaces, @"enc_password", nil];
    
    //request block
    [[RKClient sharedClient] post:@"/authorize/login" usingBlock:^(RKRequest *request) {
        //        [request sendSynchronously];
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            NSLog(@"Inside didLoadResponse");
            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
            
            NSLog(@"%@", [response parsedBody:nil]);
            
            //parsing the JSON response
            NSString *re= [response bodyAsString];
            NSDictionary *dict= [par objectFromString:re error:nil];
            NSString *status=[dict objectForKey:@"STATUS_MESSAGE"];
            NSString *statusCode= [dict objectForKey:@"STATUS_CODE"];
            
            returnString = [NSString stringWithString:status];
            
            if([status isEqualToString:@"Logged In Successfully"]){
                
                NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
                [prefs setObject:@"200" forKey:@"logged_in"];
                
                NSLog(@"Current status is - %@ %@", statusCode, status);
            }//End of If
        };//End of onDidLoadResponse
    }];//End of block
    
    return returnString;
}

//todo: Parse the JSON
- (NSString *)logOut {
    __block NSString *returnString= nil;
    
    [[RKClient sharedClient] post:@"/authorize/logout" usingBlock:^(RKRequest *request) {
        
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            NSString *re= [response bodyAsString];
            NSLog(@"Logout response is %@", re);
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
}

//todo: Parse the JSON
- (NSString *)changePasswordOld:(NSString *)oldPassword new:(NSString *)newPassword {
    
    __block NSString *returnString= nil;
    
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    oldPassword, @"old_enc_password", 
                                    newPassword, @"new_enc_password", nil];
    
    //request block
    [[RKClient sharedClient] post:@"/authorize/change_password" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
        };//End of onDidLoadResponse
    }];//End of block
    
    return returnString;
}

//todo: Parse the JSON
//Send password reset request to email
-(NSString *)forgotPassword:(NSString *)email{
    
    __block NSString *returnString;
    
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    email, @"email", nil];
    
    //request block
    [[RKClient sharedClient] post:@"/authorize/forgot_password" usingBlock:^(RKRequest *request) {            
        
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    
    return returnString;
}

//todo: Parse the JSON
-(NSString *) signupUsingEmail:(NSString *)email username:(NSString *)username password:(NSString *)password workgroup_id:(NSString *)org{
    
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    email, @"email", 
                                    username, @"username",
                                    password, @"enc_password",
                                    org, @"workgroup_id",
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/authorize/signup" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
}

/*************************************************************************************************/
//shareables
//todo: Parse the JSON
-(NSString *)fetch_followed_page:(NSString *)pageNo{
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    pageNo, @"page_no", 
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/shareables/fetch_followed" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
}

//todo: Parse the JSON
-(NSString *)fetch_owned:(NSString *)access_level page:(NSString *)pageNo{
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    access_level, @"access_level", 
                                    pageNo, @"page_no", 
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/shareables/fetch_owned" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
}

//todo: Parse the JSON
-(NSString *)get_item:(NSString *)shareable_id{
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    shareable_id, @"shareable_id", 
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/shareables/get_item" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
}

//todo: Parse the JSON
-(NSString *)publish : (NSString *)name 
                type : (NSString *)type 
            contents : (NSString *)contents 
       geo_longitude : (NSString *)geo_longitude 
        geo_latitude : (NSString *)geo_latitude 
    tag_category_ids : (NSString *)tag_category_ids 
     notify_user_ids : (NSString *)notify_user_ids 
        access_level : (NSString *)access_level
{
    
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    name,@"name",
                                    type,"type",
                                    contents,@"contents",
                                    geo_longitude,@"geo_longitude", 
                                    geo_latitude,@"geo_latitude" ,
                                    tag_category_ids,@"tag_category_ids" ,
                                    notify_user_ids,@"notify_user_ids", 
                                    access_level,@"access_level" ,
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/shareables/publish" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
}


//todo: Parse the JSON
-(NSString *)update  : (NSString *)shareable_id
                name : (NSString *)name 
                type : (NSString *)type 
            contents : (NSString *)contents 
       geo_longitude : (NSString *)geo_longitude 
        geo_latitude : (NSString *)geo_latitude 
    tag_category_ids : (NSString *)tag_category_ids 
     notify_user_ids : (NSString *)notify_user_ids 
        access_level : (NSString *)access_level
{
    
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    shareable_id,@"shareable_id",
                                    name,@"name",
                                    type,"type",
                                    contents,@"contents",
                                    geo_longitude,@"geo_longitude", 
                                    geo_latitude,@"geo_latitude", 
                                    tag_category_ids,@"tag_category_ids" ,
                                    notify_user_ids,@"notify_user_ids", 
                                    access_level,@"access_level" ,
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/shareables/update" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
}


//todo: Parse the JSON
-(NSString *)start_following:(NSString *)shareable_id{
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    shareable_id, @"shareable_id", 
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/shareables/start_following" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
}

//todo: Parse the JSON
-(NSString *)stop_following:(NSString *)shareable_id{
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    shareable_id, @"shareable_id", 
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/shareables/start_following" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
}

/*************************************************************************************************/
//Class: Messages

//todo: Parse the JSON
-(NSString *)fetch_inbox:(NSString *)page_no{
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    page_no, @"page_no", 
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/messages/fetch_inbox" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
}

//todo: Parse the JSON
-(NSString *)fetch_sentbox:(NSString *)page_no{
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    page_no, @"page_no", 
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/messages/fetch_sentbox" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
}

//todo: Parse the JSON
-(NSString *)message_details_id: (NSString *)message_id latest_reply_id:(NSString *)latest_reply_id{
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    message_id, @"message_id",
                                    latest_reply_id,@"latest_reply_id",
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/messages/message_details" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
}

//todo: Parse the JSON
-(NSString *)post_new_subject:(NSString *) subject 
                     contents:(NSString *)contents 
                 geo_logitude:(NSString *)geo_longitude 
                 geo_latitude:(NSString *)geo_latitude  
              notify_user_ids:(NSString *)notify_user_ids 
             tag_category_ids:(NSString *)tag_category_ids{
    
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    subject, @"subject",
                                    contents,@"contents",
                                    geo_longitude,@"geo_longitude",
                                    geo_latitude,@"geo_latitude",
                                    notify_user_ids,@"notify_user_ids",
                                    tag_category_ids,@"tag_category_ids",
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/messages/post_new" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
    
}

//todo: Parse the JSON
-(NSString *)post_reply:(NSString *) message_id
               contents:(NSString *)contents 
           geo_logitude:(NSString *)geo_longitude 
           geo_latitude:(NSString *)geo_latitude  {
    
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    message_id, @"message_id",
                                    contents,@"contents",
                                    geo_longitude,@"geo_longitude",
                                    geo_latitude,@"geo_latitude",
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/messages/post_reply" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
    
}
/*************************************************************************************************/
// Cases

//todo: Parse the JSON
-(NSString *)change_review_date_case_id:(NSString *) case_id
                       next_review_date:(NSString *)next_review_date {
    
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    case_id,@"case_id",
                                    next_review_date,@"next_review_date",
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/cases/change_review_date" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
}

//todo: Parse the JSON
-(NSString *)close_case:(NSString *) case_id{
    
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    case_id,@"case_id",
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/cases/close_case" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
}

//todo: Parse the JSON
-(NSString *)fetch_comments_of_proposition:(NSString *) case_prop_id{
    
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    case_prop_id,@"case_prop_id",
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/cases/fetch_comments_of_proposition" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
}


// string fetch_complete_case( string $_POST['$case_id']  )
//todo: Parse the JSON
-(NSString *)fetch_complete_case:(NSString *) case_id{
    
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    case_id,@"case_id",
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/cases/fetch_complete_case" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
}


// string fetch_followed( string $_POST['user_id'], string $_POST['page_no']  )
//todo: Parse the JSON
-(NSString *)fetch_followed:(NSString *) user_id
                    page_no:(NSString *)page_no{
    
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    user_id,@"user_id",
                                    page_no,@"page_no",
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/cases/fetch_followed" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
}

// string fetch_propositions_of_review( string $_POST['case_rev_id'], string $_POST['latest_proposition_id']  )
// todo: Parse the JSON
-(NSString *)fetch_propositions_of_review:(NSString *) case_rev_id
                    latest_proposition_id:(NSString *)latest_proposition_id{
    
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    case_rev_id,@"case_rev_id",
                                    latest_proposition_id,@"latest_proposition_id",
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/cases/fetch_propositions_of_review" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
}

// string fetch_reviews_of_case( string $_POST['case_id'], type $_POST['latest_review_id']  )
// todo: Parse the JSON
-(NSString *)fetch_reviews_of_case:(NSString *) case_id
                  latest_review_id:(NSString *)latest_review_id{
    
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    case_id,@"case_id",
                                    latest_review_id,@"latest_review_id",
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/cases/fetch_reviews_of_case" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
}

// string fetch_unfollowed( string $_POST['page_no']  )
// todo: Parse the JSON
-(NSString *)fetch_unfollowed:(NSString *) page_no{
    
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    page_no,@"page_no",
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/cases/fetch_unfollowed" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
}

// string my_cases( string $_POST['page_no']  )
// todo: Parse the JSON
-(NSString *)my_cases:(NSString *) page_no{
    
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    page_no,@"page_no",
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/cases/my_cases" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
}


// string post_comment( string $_POST['case_prop_id'], string $_POST['contents'], string $_POST['geo_longitude'], string $_POST['geo_latitude'], string $_POST['consent'], string $_POST['notify_user_ids']  )
-(NSString *)post_comment: (NSString *) case_prop_id contents:(NSString *)contents geo_longitude:(NSString *)geo_longitude geo_latitude:(NSString *)geo_latitude consent:(NSString *)consent notify_user_ids:(NSString *)notify_user_ids{
    
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    case_prop_id,@"case_prop_id",
                                    contents,@"contents",
                                    geo_longitude,@"geo_longitude",
                                    geo_latitude,@"geo_latitude",
                                    consent,@"consent",
                                    notify_user_ids,@"notify_user_ids",
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/cases/post_comment" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
    
}

// string post_new( string $_POST['subject'], string $_POST['contents'], string $_POST['tag_category_ids'], string $_POST['geo_longitude'], string $_POST['geo_latitude'], string $_POST['notify_user_ids'], string $_POST['next_review_date']  )

-(NSString *)post_new:(NSString *)subject contents:(NSString *)contents tag_category_ids:(NSString *)tag_category_ids geo_logitude:(NSString *)geo_logitude geo_latitude:(NSString *)geo_latitude notify_user_ids:(NSString *)notify_user_ids next_review_date:(NSString *)next_review_date{
    
    __block NSString *returnString= nil;
    __block NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    subject, @"subject",
                                    contents,@"contents",
                                    tag_category_ids,@"tag_category_ids",
                                    geo_logitude,@"geo_logitude",
                                    geo_latitude,@"geo_latitude",
                                    notify_user_ids,@"notify_user_ids",
                                    next_review_date,@"next_review_date",
                                    nil];
    
    //request block
    [[RKClient sharedClient] post:@"/cases/post_comment" usingBlock:^(RKRequest *request) {            
        [request setParams:params];
        request.onDidLoadResponse = ^(RKResponse *response) {
            
            
            NSString *re= [response bodyAsString];
            //Parse and put the response inside the return string
            NSLog(@"re");
            returnString= re;
            
        };//End of onDidLoadResponse
    }];//End of block
    return returnString;
    
}
// string post_proposition( string $_POST['case_id'], string $_POST['contents'], string $_POST['geo_longitude'], string $_POST['geo_latitude'], string $_POST['notify_user_ids']  )
// string post_review( string $_POST['case_id'], string $_POST['contents'], string $_POST['geo_longitude'], string $_POST['geo_latitude'], string $_POST['notify_user_ids'], string $_POST['next_review_date']  )
// string publish_coauthor( string $_POST['case_id'], string $_POST['coauthor_user_ids']  )
// string reopen_case( string $_POST['case_id']  )
// string start_following( string $_POST['case_id']  )
// string stop_following( string $_POST['case_id']  )


/*************************************************************************************************/
@end
