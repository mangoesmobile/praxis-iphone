//
//  ViewController.m
//  praxis-framework
//
//  Created by Mahbub Morshed on 8/1/12.
//  Copyright (c) 2012 ManGoes Mobile. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize userNameInput, passWordInput, warningLabel,loginButton;

- (void)viewDidLoad
{
    [super viewDidLoad];

    warningLabel.hidden =YES;
    
    [userNameInput becomeFirstResponder];
    
    //Grid Dimension
    xInput = [[UIAlertView alloc]initWithTitle:@"Enter X value" message:@"X value is" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles: @"Load JSON",nil];
    xInput.alertViewStyle= UIAlertViewStylePlainTextInput;
    xInput.delegate= self;
    [[xInput textFieldAtIndex:0]setKeyboardType:UIKeyboardTypeNumberPad];
    
    //Grid dimension
    yInput= [[UIAlertView alloc]initWithTitle:@"Enter Y value" message:@"Y value is" delegate:self cancelButtonTitle:@"okay" otherButtonTitles: nil];
    yInput.alertViewStyle= UIAlertViewStylePlainTextInput;
    yInput.delegate= self;
    [[yInput textFieldAtIndex:0]setKeyboardType:UIKeyboardTypeNumberPad];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];

    if(alertView== xInput){
        if (buttonIndex == 0) {
            NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
            [prefs setInteger:0 forKey:@"loadjson"];
            
            NSString *entered=[xInput textFieldAtIndex:0].text;
            
            if( [entered length] >= 1 )
            {
                [yInput show];
                
                int x= [entered intValue];
                [prefs setInteger:x forKey:@"xval"];
            }
            else
            {
                [xInput show];
            }
        }
        else {

            [prefs setInteger:1 forKey:@"loadjson"];
            
            //JSON Loading
            NSString *myText;
            
            NSString *filePath = [[NSBundle mainBundle] pathForResource:@"json_value" ofType:@"txt"];
            if (filePath) {
                myText = [NSString stringWithContentsOfFile:filePath usedEncoding:nil error:nil];
                if (myText) {
                    NSLog(@"JSON_value is- %@",myText);
                }
            }
            
//            //parse json and save the values
//            RKJSONParserJSONKit *par= [[RKJSONParserJSONKit alloc]init];
//            NSDictionary *dict= [par objectFromString:myText error:nil];
//            
//            NSMutableArray *arrayOfFrame= [[NSMutableArray alloc]initWithCapacity:1];
//            NSMutableArray *arrayOfContentType= [[NSMutableArray alloc]initWithCapacity:1];
//            NSMutableArray *arrayOfContent= [[NSMutableArray alloc]initWithCapacity:1];
//            
//            NSMutableArray *currentJSONFrames= [dict objectForKey:@"frames"];
//            
//            int x= [[dict objectForKey:@"screen_width"] intValue];
//            int y= [[dict objectForKey:@"screen_height"] intValue];
//            
//            for(int i=0; i< [currentJSONFrames count]; i++){
//                NSDictionary *d= [currentJSONFrames objectAtIndex:i];
//                NSString *start_x= [d objectForKey:@"start_x"];
//                NSString *start_y= [d objectForKey:@"start_y"];
//                NSString *width= [d objectForKey:@"width"];
//                NSString *height= [d objectForKey:@"height"];
//                NSString *content= [d objectForKey:@"content"];
//                NSString *content_type= [d objectForKey:@"content_type"];
//                
//                
//                int minW= 320/ x;
//                int minH= 400/ y;
//                
//                CGRect r= CGRectMake([start_x intValue]*minW, [start_y intValue]*minH, [width intValue]*minW, [height intValue]*minH);
//                NSValue *v= [NSValue valueWithCGRect:r];
//                
//                //                NSLog(@"%@", v);
//                
//                [arrayOfFrame addObject:v];
//                [arrayOfContent addObject:content];
//                [arrayOfContentType addObject:content_type];
//                
//            }
//            
//            //Load a JSON
//            [prefs setInteger:x forKey:@"xval"];
//            [prefs setInteger:y forKey:@"yval"];
            
//            MMAppEngine *engine= [MMAppEngine sharedManager];
//            engine.currentStates = [NSMutableArray arrayWithArray:arrayOfFrame];
//            engine.content = [NSMutableArray arrayWithArray:arrayOfContent];
//            engine.contentType = [NSMutableArray arrayWithArray:arrayOfContentType];
        }
    }
    
    if (alertView==yInput) {
        NSString *entered=[yInput textFieldAtIndex:0].text;
        if( [entered length] >= 1 )
        {
            int y= [entered intValue];
            
            [prefs setInteger:y forKey:@"yval"];
            
            [self performSegueWithIdentifier: @"loggedinsegue" sender: self];
        }
        else
        {
            [yInput show];
        }
    }
}

-(IBAction)LoginButton:(id)sender{
    NSString *userName= userNameInput.text;
    NSString *passWord= passWordInput.text;
    
    NSLog(@"username is- %@", userName);
    NSLog(@"password is- %@", passWord);
    
    if ([userName length] > 0 && [passWord length] > 0) 
     {
        warningLabel.hidden =YES;
        [xInput show];
    }
    else {
        warningLabel.hidden =NO;
    }
}

-(IBAction)ShowSignUpForm:(id)sender{
    [self performSegueWithIdentifier: @"signupsegue" sender: self];
}

@end
