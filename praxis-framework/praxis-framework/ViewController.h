//
//  ViewController.h
//  praxis-framework
//
//  Created by Mahbub Morshed on 8/1/12.
//  Copyright (c) 2012 ManGoes Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ViewController : UIViewController<UIAlertViewDelegate>{
    
    UITextField *userNameInput;
    UITextField *passWordInput;
    UIButton *loginButton;
    
    UILabel *warningLabel;
    
    UIAlertView *xInput;
    UIAlertView* yInput;
}

@property(strong) IBOutlet UITextField *userNameInput;
@property(strong) IBOutlet UITextField *passWordInput;
@property(strong) IBOutlet UIButton *loginButton;

@property(strong) IBOutlet UILabel *warningLabel;

-(IBAction)LoginButton:(id)sender;
-(IBAction)ShowSignUpForm:(id)sender;

@end
