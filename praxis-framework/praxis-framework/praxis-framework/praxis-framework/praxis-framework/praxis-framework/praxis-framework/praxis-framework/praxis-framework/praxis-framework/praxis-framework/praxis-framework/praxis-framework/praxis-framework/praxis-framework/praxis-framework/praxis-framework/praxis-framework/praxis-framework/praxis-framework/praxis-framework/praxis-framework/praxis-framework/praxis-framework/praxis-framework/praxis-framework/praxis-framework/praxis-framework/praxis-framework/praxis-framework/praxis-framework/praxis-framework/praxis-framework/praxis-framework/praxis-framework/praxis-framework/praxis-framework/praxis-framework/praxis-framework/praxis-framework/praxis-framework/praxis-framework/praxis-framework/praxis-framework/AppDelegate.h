//
//  AppDelegate.h
//  praxis-framework
//
//  Created by Mahbub Morshed on 8/1/12.
//  Copyright (c) 2012 ManGoes Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
